<?php

if(has_post_thumbnail()):
    $background = get_the_post_thumbnail_url();
else:
    $background = get_field('default_background','option');
endif;

$classes = 'post';

if(is_archive()) {
    $classes = ' one-fourth';
    $i = $wp_query->current_post;
    if($i % 4 == 0 || $i == 0) $classes .= ' first';
}

?>

<div class="<?php echo $classes;?>">

    <a href="<?php echo get_permalink();?>">

        <div style="background-image:url('<?php echo $background;?>')" class="background"></div>

        <div class="post_content">

            <span class="post_title"><?php echo get_the_title(); ?></span>

        </div>

    </a>

</div>
