<?php

$i = $wp_query->current_post;
$classes = 'post one-fourth';

if ($i % 4 == 0 || $i == 0) $classes .= ' first';

if (has_post_thumbnail()) :

    $background = get_the_post_thumbnail_url();

else :

    $background = get_field('default_background', 'option');

endif;

?>

<div class="<?php echo $classes; ?>">

    <div style="background-image:url('<?php echo $background; ?>')" class="background"></div>

    <div class="post_content">

        <span class="first_name">
            <?php the_field('first_name'); ?>
        </span>
        <span class="last_name"><?php the_field('last_name'); ?></span>
        <span class="job_position"><?php the_field('job_position'); ?></span>
        <span class="pratices_since"><?php the_field('pratices_since_label', 'option'); ?>
            <?php the_field('pratices_since'); ?>
        </span>
        <span class="rpps"><?php the_field('rpps_label', 'option'); ?> <?php the_field('rpps'); ?></span>

    </div>

</div>