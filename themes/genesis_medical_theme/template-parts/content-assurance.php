<?php

$i = $wp_query->current_post;
$classes .= 'post one-sixth';

if($i % 6 == 0 || $i == 0) $classes .= ' first';

if(has_post_thumbnail()):

    $background = get_the_post_thumbnail_url();

else:

    $background = get_field('default_background','option');

endif;

?>

<div class="<?php echo $classes;?>">

    <div style="background-image:url('<?php echo $background;?>')" class="background"></div>

</div>
