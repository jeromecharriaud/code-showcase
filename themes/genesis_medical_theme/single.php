<?php

//Style & Scripts
add_action( 'wp_enqueue_scripts', 'enqueue_style_template_single' );
function enqueue_style_template_single () {

    wp_enqueue_script( 'popup-scripts', get_stylesheet_directory_uri() . '/js/popup.js', array());

    $scroll_height = '50';
    if(!empty(get_field('popup_know_more_scroll_height','option'))) {
        $scroll_height = get_field('popup_know_more_scroll_height','option');
    }
    wp_localize_script('popup-scripts', 'know_more_scroll_height', $scroll_height);

    if(!empty($_COOKIE["number_sessions"])) {
        wp_localize_script('popup-scripts', 'number_sessions', $_COOKIE["number_sessions"]);
    }

}

//Scripts : Actualité
add_action('wp_enqueue_scripts', 'single_scripts');
function single_scripts () {
    if(is_single('actualite')) {
        wp_localize_script('theme-scripts', 'change_title', 'true');
    }
}

//Sidebar
add_action( 'genesis_sidebar', 'genesis_do_sidebar' );

//Breadcrumb
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs');

//Popup
add_action('genesis_before_header', 'show_popup_know_more', 10, 1);
add_action('genesis_before_header', 'show_popup_newsletter', 20, 1);

//Single Post Header
add_action('genesis_after_header', 'show_single_header', 50);
function show_single_header () {

    if(has_post_thumbnail()):
        $background = get_the_post_thumbnail_url();
    else:
        $background = get_field('default_background','option');
    endif;

    ?>

    <div style="background-image:url('<?php echo $background;?>')" class="background post_header"></div>

    <?php

}

//Single Post Content
remove_action('genesis_entry_content', 'genesis_do_post_content');
add_action('genesis_loop', 'show_single_post');
function show_single_post () {

    $post_type = get_post_type();
    $post_type_object = get_post_type_object($post_type);
    $labels = $post_type_object->labels;

    ?>

    <section id="single" class="box single_<?php echo $post_type;?>">

        <?php genesis_do_breadcrumbs(); ?>

        <h1 class="post_title"><?php echo get_the_title();?></h1>

        <div class="post_meta">
            <span class="date">Article publié le <?php echo get_the_date('j M Y'); ?></span>
            <?php if(!empty(get_the_author())) { ?>
                <span class="author">par <?php echo get_the_author(); ?></span>
            <?php } ?>
        </div>

        <?php echo get_the_content(); ?>

        <div class="post_meta_footer">
            <div class="author"><b><?php echo get_the_author(); ?></b></div>
            <div class="date">Article paru le <?php echo get_the_date('j M Y'); ?> et re-publié le <?php the_modified_date('j M Y'); ?></div>
        </div>

        <p class="<?php echo $post_type; ?>_footer post_footer"><?php the_field($post_type.'_footer','option');?>

    </section>

    <?php

}

genesis();
