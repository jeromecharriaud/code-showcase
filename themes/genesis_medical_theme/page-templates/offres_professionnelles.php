<?php
/* Template Name: Offres professionnels */

// Forces full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//Scripts
add_action('wp_enqueue_scripts', 'offres_pro_scripts');
function offres_pro_scripts () {
    wp_localize_script('theme-scripts', 'change_title', 'true');

    wp_enqueue_script( 'popup-scripts', get_stylesheet_directory_uri() . '/js/popup.js', array());

    $scroll_height = '50';
    if(!empty(get_field('popup_contact_scroll_height','option'))) {
        $scroll_height = get_field('popup_contact_scroll_height','option');
    }
    wp_localize_script('popup-scripts', 'contact_scroll_height', $scroll_height);
}

////Popup
add_action('genesis_before_header', 'show_popup_contact', 10, 1);

//Formulaire
add_action('genesis_entry_content', 'show_form', get_field('form_prioriy'), 1);

//Services
add_action('genesis_entry_content', 'show_services', get_field('services_prioriy'), 1);

//Motifs
add_action('genesis_entry_content', 'show_offers', get_field('offers_prioriy'), 1);

//Blocs
add_action('genesis_entry_content', 'show_blocks', get_field('blocks_prioriy'), 1);

//Certification
add_action('genesis_entry_content', 'show_certification', get_field('certification_prioriy'), 1);

//Identité
add_action('genesis_entry_content', 'show_identite', get_field('identite_prioriy'), 1);

// Runs the Genesis loop.
genesis();
