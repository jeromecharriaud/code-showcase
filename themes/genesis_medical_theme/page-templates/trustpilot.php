<?php
/* Template Name: Trustpilot */

// Forces full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//Style & Scripts
add_action( 'wp_enqueue_scripts', 'enqueue_style_template_trustpilot' );
function enqueue_style_template_trustpilot () {

    wp_enqueue_script( 'trustbox-script', '//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js' , array());

}

//Services
add_action('genesis_entry_content', 'show_trustpilot_grid', 10, 1);

// Runs the Genesis loop.
genesis();
