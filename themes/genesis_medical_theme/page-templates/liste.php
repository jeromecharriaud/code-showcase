<?php

/* Template Name: Liste */

//Tabs
add_action('wp_enqueue_scripts','enqueue_tabs_scripts', 10);
add_action('wp_enqueue_scripts','enqueue_tabs_style', 10);

add_action('genesis_entry_content', 'show_template_liste', 10, 1);
function show_template_liste () {

    ?>

    <section id="liste" class="box">

        <div id="tabs">

            <?php

            if( have_rows('list') ):

                ?>

                <ul>

                    <?php

                    //Titles
                    $i = 1;

                    while ( have_rows('list') ) : the_row();

                        ?>

                        <li><a href="#tabs-<?php echo $i;?>"><i class="fas fa-long-arrow-alt-right"></i><?php the_sub_field('title');?></a></li>

                        <?php

                        $i++;

                    endwhile;

                    ?>

                </ul>

                <h1><?php the_title(); ?></h1>

                <?php

                //Content
                $i = 1;

                while ( have_rows('list') ) : the_row();

                    ?>


                    <div id="tabs-<?php echo $i;?>"><?php the_sub_field('content');?></div>

                    <?php

                    $i++;

                endwhile;

            endif;

            ?>

        </div>

    </section>

    <?php

}

genesis();
