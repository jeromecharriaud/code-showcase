<?php
/* Template Name: Formulaire */

// Forces full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//After Header
add_action('genesis_after_header', 'show_template_form_background', 10, 1);
function show_template_form_background () {

    ?>

    <div class="background" style="background-image:url('<?php echo get_field('background');?>');"></div>

    <?php

}

//Content
add_action('genesis_entry_content', 'show_template_form_content', 10, 1);
function show_template_form_content () {

    ?>

    <section id="gform">

        <div id="form-content"><?php the_content();?></div>

    </section>

    <?php

}

// Runs the Genesis loop.
genesis();
