<?php
/* Template Name: Service Grand Public */

// Forces full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//Style & Scripts
add_action( 'wp_enqueue_scripts', 'enqueue_style_template_homepage' );
function enqueue_style_template_homepage () {

    wp_enqueue_style( 'template-slider', get_stylesheet_directory_uri() . '/css/style-slider.css', array());

}

//Formulaire
add_action('genesis_entry_content', 'show_form', get_field('form_prioriy'), 1);

//Médecins
add_action('genesis_entry_content', 'show_medecins', get_field('medecins_prioriy'), 1);

//Blocs
add_action('genesis_entry_content', 'show_blocks', get_field('blocks_prioriy'), 1);

//Certification
add_action('genesis_entry_content', 'show_certification', get_field('certification_prioriy'), 1);

//Identité
add_action('genesis_entry_content', 'show_identite', get_field('identity_prioriy'), 1);

// Runs the Genesis loop.
genesis();
