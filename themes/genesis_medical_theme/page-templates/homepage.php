<?php
/* Template Name: Homepage */

// Forces full width content layout.
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//Style & Scripts
add_action( 'wp_enqueue_scripts', 'enqueue_style_template_homepage' );
function enqueue_style_template_homepage () {

    wp_enqueue_style( 'template-slider', get_stylesheet_directory_uri() . '/css/style-slider.css', array());

	wp_enqueue_script( 'template-homepage-scripts', get_stylesheet_directory_uri() . '/js/template_homepage.js', array());

    wp_enqueue_script( 'popup-scripts', get_stylesheet_directory_uri() . '/js/popup.js', array());

    wp_enqueue_script( 'trustbox-script', '//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js' , array());

}

//Consulter un médecin
add_action('genesis_entry_content', 'show_consult', get_field('consult_priority'), 1);
add_action('genesis_before_header', 'show_popup_assurance', 10, 1);

//Services
add_action('genesis_entry_content', 'show_services', get_field('services_priority'), 1);

//Motifs
add_action('genesis_entry_content', 'show_motifs', get_field('motifs_priority'), 1);

//Applications
add_action('genesis_entry_content', 'show_apps', get_field('apps_priority'), 1);

//Statistiques
add_action('genesis_entry_content', 'show_statistics', get_field('statistics_priority'), 1);

//Slider
add_action('genesis_entry_content', 'show_slider', get_field('slider_priority'), 1);

//Certification
add_action('genesis_entry_content', 'show_certification', get_field('certification_priority'), 1);

//Qui me répond ?
add_action('genesis_entry_content', 'show_qui_me_repond', get_field('qui_me_repond_priority'), 1);

//Arguments
add_action('genesis_entry_content', 'show_arguments', get_field('arguments_priority'), 1);

//Rechercher assurances
add_action('genesis_entry_content', 'show_search_assurances', get_field('form_priority'), 1);

//Bannière
add_action('genesis_entry_content', 'show_banniere', get_field('banniere_priority'), 1);

//Identité
add_action('genesis_entry_content', 'show_identite', get_field('identite_priority'), 1);

//Last news
add_action('genesis_entry_content', 'show_last_posts', get_field('last_posts_priority'), 1);

// Runs the Genesis loop.
genesis();