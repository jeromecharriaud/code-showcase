<?php

remove_action('genesis_loop','genesis_do_loop');
remove_action( 'genesis_after_endwhile', 'genesis_posts_nav' );

//Header
add_action('genesis_before_loop', 'show_assurance_archive_header');

//Loop
add_action('genesis_loop', 'show_archive_loop', 10, 1);

//Footer
add_action('genesis_after_loop', 'show_archive_assurance_footer', 10, 1);

//Identité
add_action('genesis_after_loop', 'show_identite', 20, 1);

genesis();
