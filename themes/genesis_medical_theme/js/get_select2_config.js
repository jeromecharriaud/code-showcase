jQuery(function($){

    var select2 = $(".select2").select2({
         width: '33.33%',
//        dropdownAutoWidth : true,
    });

    $('#filter-category').on('change', function () {

        var url = $('#page-link').val()+'?categorie='+$(this).val();

        if (url) { // require a URL
            window.location = url; // redirect
        }
        else return false;

    })

} );
