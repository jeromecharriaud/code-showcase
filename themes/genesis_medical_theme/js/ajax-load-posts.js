(function ($) {

    //When the user changes the value of the input
    $('input.target').on('input', function () {
        if ($(this).val().length >= 3) {
            ajax_load_posts();
        }
    });

    //Hide the content when clicking outside the div
    $(document).mouseup(function (e) {

        var container = $(".form_content");

        if (!container.is(e.target) && container.has(e.target).length === 0) {
            $('.ajax_content').hide();
        }

    });

    function ajax_load_posts() {

        var form = $('.search_posts');
        $('.target').addClass('loading');


        event.preventDefault();

        $.ajax({

            // URL to which request is sent.
            url: ajax_object.ajaxurl,

            // specifies how contents of data option are sent to the server.
            // `post` indicates that we are submitting the data.
            type: 'post',

            // data to be sent to the server.
            data: {
                // a function defined in functions.php hooked to this action (with `wp_ajax_nopriv_` and/or `wp_ajax_` prefixed) will run.
                action: 'load_posts',

                // stores the value of `data-id` attribute of the clicked link in a variable.
                post_type: form.find('input[name="post_type"]').val(),
                title: form.find('#title').val(),
            },

            // pre-reqeust callback function.
            beforeSend: function () {

            },

            // function to be called if the request succeeds.
            // `response` is data returned from the server.
            success: function (response) {

                $('.ajax_content').children().remove();
                $('.ajax_content').append(response);
                $('.target').removeClass('loading');
                $('.ajax_content').show();

            }
        })

    };

})(jQuery);