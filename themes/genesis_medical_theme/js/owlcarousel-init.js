( function ( document, $, undefined ) {

    $(document).ready(function(){

        //Mobile Carousel
        var checkWidth = $(document).width();

        //Default Slider
        var slider = $('.slider');

        slider.owlCarousel({
            items: 1,
            loop: true,
            nav: false,
            video:false,
            dots: true,
            autoplay: true,
            autoplayTimeout:7000,
            autoHeight:true,
            merge:true,
            margin:10,
            center:true,
        });

        //If the width is under a given resolution, we activate the carousel
        if(checkWidth < 1280 ){

            $(".mobile-carousel").owlCarousel({
                items: 1,
                margin: 30,
                video:true,
                autoplay: true,
                nav: false,
                dots: false,
                autoHeight:false,
                center: true,
            });

        }

        //Else we remove the class
        else {

            $("div").removeClass("mobile-carousel");

            //Last news
            var last_posts = $('.last_posts');

            last_posts.owlCarousel({
                items: 4,
                loop: true,
                nav: false,
                video:false,
                dots: false,
                autoplay: true,
                autoplayTimeout:7000,
                autoHeight:false,
                merge:true,
                margin: 75,
                center:false,
            });

            //Last news
            var medecins = $('.medecins');

            medecins.owlCarousel({
                items: 2,
                loop: true,
                nav: true,
                video:false,
                dots: false,
                autoplay: false,
                autoplayTimeout:7000,
                autoHeight:false,
                merge:true,
                margin: 30,
                center:false,
            });

        }

    });

})( document, jQuery );
