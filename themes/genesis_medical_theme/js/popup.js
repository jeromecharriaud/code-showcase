jQuery(document).ready(function($) {
	//Location : Formulaire Assurance
	//Trigger : When user clicks on a partner
	var assurance_popup = $(".assurance_popup .title");
	var assurance_popup_content = $(".assurance_popup .assurance-content");

	$(document).on("click", ".assurance_found", function() {
		$(".assurance_popup").hide();
		$("body").addClass("popup-displayed");

		var pris_en_charge = this.dataset.value;

		var title = $(this)
			.find(".assurance_title")
			.html();
		var content = $(this)
			.find(".assurance_content")
			.html();

		assurance_popup.text(title);
		assurance_popup_content.text(content);

		if (pris_en_charge == true) {
			$(".assurance_popup[data-value=true]").show();
		} else {
			$(".assurance_popup[data-value=false]").show();
		}
	});

	$(document).on("click", ".close_popup", function() {
		$(".popup").hide();
		$("body").removeClass("popup-displayed");
	});

	//Location : Single Post
	//Trigger : After scrolling
	var popup_know_more_displayed = false;

	$(window).scroll(function() {
		if (popup_know_more_displayed != true && $(".know_more")[0]) {
			var scrollPercent =
				(100 * $(window).scrollTop()) /
				($(document).height() - $(window).height());

			if (scrollPercent >= know_more_scroll_height) {
				$(".know_more").show();
				popup_know_more_displayed = true;
				return;
			}
		}
	});

	//Location : Single Post
	//Trigger : After 3 visits
	var popup_newsletter_displayed = false;

	if (popup_newsletter_displayed != true && $(".newsletter")[0]) {
		if (number_sessions == 3) {
			$(".newsletter").show();
			popup_newsletter_displayed = true;
			return;
		}
	}

	//Location : Offres professionnels
	//Trigger : After scrolling
	var popup_contact_displayed = false;

	$(window).scroll(function() {
		if (popup_contact_displayed != true && $(".contact")[0]) {
			var scrollPercent =
				(100 * $(window).scrollTop()) /
				($(document).height() - $(window).height());

			if (scrollPercent >= contact_scroll_height) {
				$(".contact").show();
				popup_contact_displayed = true;
				return;
			}
		}
	});
});
