jQuery(document).ready(function($) {

	//FIrst word
	$(".firstword").html(function(){
	  var text= $(this).text().trim().split(" ");
	  var first = text.shift();
	  return (text.length > 0 ? "<b>"+ first + "</b> " : first) + text.join(" ");
	});

	var checkWidth = $(document).width();

	if(checkWidth < 960 ){
		$('#header-menu .sub-menu').hide();
	}

	//Responsive Menu
	$('.responsive-menu').click(function() {

		$('.responsive-menu .sub-menu').slideToggle("slow");

	});

	//Filter for News
	if ($('.filter_category a').length > 0) {
		$(".filter_category a").css("display", "none");
	}

	$( ".filter_button" ).toggle(function() {
		$(this).removeClass("down");
		$(this).addClass("up");
		$(".filter_category a").toggle();
	}, function() {
		$(this).removeClass("up");
		$(this).addClass("down");
		$(".filter_category a").toggle();
	});

	//Change menu title

});
