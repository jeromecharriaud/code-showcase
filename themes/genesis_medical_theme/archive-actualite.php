<?php

remove_action('genesis_loop','genesis_do_loop');
remove_action( 'genesis_after_endwhile', 'genesis_posts_nav' );

//Script : Select2
add_action('wp_enqueue_scripts','get_lib_select2', 20);

//Before Loop
add_action('genesis_before_loop', 'show_actualite_before_loop');

//Show Filter
add_action('genesis_before_loop', 'show_archive_filter');

//Archive Loop
add_action('genesis_loop','show_archive_loop');

genesis();
