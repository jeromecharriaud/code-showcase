<?php

function show_medecin_before_loop () {

	?>

	<section id="archive-header" class="box">

		<div class="medecins_header"><?php the_field('medecins_header', 'option');?></div>

		<h1>
			<div><b><?php the_field('medecins_title_1', 'option');?></b></div>
			<div><span><?php the_field('medecins_title_2', 'option');?></span></div>
		</h1>

	</section>

	<?php

}
