<?php

// Register Custom Post medecin
add_action('init', 'post_type_medecin', 0);
function post_type_medecin()
{

    $labels = array(
        'name'                  => 'Médecins',
        'singular_name'         => 'Médecin',
        'menu_name'             => 'Médecins',
        'name_admin_bar'        => 'Médecin',
    );
    $rewrite = array(
        'slug'                  => 'medecins',
        'with_front'            => true,
        'pages'                 => true,
        'feeds'                 => true,
    );
    $capabilities = array(
        'edit_post'             => 'edit_medecin',
        'read_post'             => 'read_medecin',
        'delete_post'           => 'delete_medecin',
        'edit_posts'            => 'edit_medecins',
        'edit_others_posts'     => 'edit_others_medecins',
        'publish_posts'         => 'publish_medecins',
        'read_private_posts'    => 'read_private_medecins',
    );
    $args = array(
        'label'                 => __('Médecin', 'child-theme'),
        'description'           => __('Description', 'child-theme'),
        'labels'                => $labels,
        'supports'                 => array('title', 'editor', 'thumbnail'),
        'taxonomies'            => array('category-medecin'),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'menu_icon'                => 'dashicons-businessperson',
        'menu_position'         => 8,
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'rewrite'                => $rewrite,
        'capability_type'       => 'medecin',
        'map_meta_cap'             => true,
        'capabilities'          => $capabilities,

    );

    register_post_type('medecin', $args);
}
