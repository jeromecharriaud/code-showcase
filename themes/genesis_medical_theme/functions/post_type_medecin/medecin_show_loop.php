<?php

function show_medecin_loop () {

	global $post;
	global $wp_query;

	$post_type = get_post_type();
	$post_type_object = get_post_type_object($post_type);

	$taxonomy = get_object_taxonomies($post_type)[0];
	$labels = $post_type_object->labels;

	//Let's build the arguments for the query
	$args2 = array();

	//Tax Query : Parent term
	if( isset($_GET['categorie']) && !empty($_GET['categorie']) && $_GET['categorie'] != 'toutes' ):

		$args2['tax_query'][] = array(
			'taxonomy' => $taxonomy,
			'field'    => 'slug',
			'terms'    =>  $_GET['categorie'],
		);

	endif;

	$args = array_merge(
		array(
		'post_type'         => $post_type,
		'orderby'           => '',
		'order'             => 'DESC',
		'post_status'       => 'publish',
		'posts_per_page '   => 16,  //Must be changed in WordPress Admin
		'paged'             => get_query_var( 'paged' )
	), $args2);

	$wp_query = new WP_Query( $args );

	if( $wp_query->have_posts() ):

        $current_post = $wp_query->current_post;
        $count = $wp_query->post_count;

        ?>

        <section id="posts">

            <?php

            while( $wp_query->have_posts() ): $wp_query->the_post();

                if($current_post%4 == 0) echo '<div class="line">';

                get_template_part('template-parts/content','');

                if($current_post%4 == 0 || $current_post == ($count)) echo '</div>';

            endwhile;

            echo '<div class="clearfix"></div>';

            genesis_posts_nav();

            ?>

        </section>

    <?php

	endif;

	wp_reset_query();

}
