<?php


if (!function_exists('add_taxonomy_cat_medecin')) {

    // Register Custom Taxonomy
    add_action('init', 'add_taxonomy_cat_medecin', 0);
    function add_taxonomy_cat_medecin()
    {

        $labels = array(
            'name'                       => __('Catégories', 'la-nebuleuse'),
            'singular_name'              => __('Catégorie', 'la-nebuleuse'),
            'menu_name'                  => __('Catégories', 'la-nebuleuse'),
        );
        $rewrite = array(
            'slug'                       => 'cat-medecin',
            'with_front'                 => false,
            'hierarchical'               => false,
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
            'rewrite'                    => $rewrite,
        );
        register_taxonomy('category-medecin', array('medecin'), $args);
    }
}
