<?php

add_action('wp_enqueue_scripts', 'enqueue_theme_scripts');
function enqueue_theme_scripts () {

	wp_enqueue_script(
		'theme-scripts',
		get_stylesheet_directory_uri() . '/js/theme-scripts.js',
		array( 'jquery' ),
		filemtime(plugin_dir_path( __FILE__ )),
		true
	);


}
