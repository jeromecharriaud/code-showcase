<?php

function show_archive_loop () {

    $post_type = get_post_type();
    $post_type_object = get_post_type_object($post_type);

    // PARAMETERS
    $posts_per_line = 4;
    if($post_type == 'assurance') $posts_per_line = 6;

    ?>

    <section id="archive-loop">

        <?php

        global $post;
        global $wp_query;

        $taxonomy = get_object_taxonomies($post_type)[0];
        $labels = $post_type_object->labels;

        //Let's build the arguments for the query
        $args2 = array();

        //Tax Query : Parent term
        if( isset($_GET['categorie']) && !empty($_GET['categorie']) && $_GET['categorie'] != 'toutes' ):

            $args2['tax_query'][] = array(
                'taxonomy' => $taxonomy,
                'field'    => 'slug',
                'terms'    =>  $_GET['categorie'],
            );

        endif;

        $args = array_merge(
            array(
            'post_type'         => $post_type,
            'orderby'           => '',
            'order'             => 'DESC',
            'post_status'       => 'publish',
            'posts_per_page '   => 16,  //Must be changed in WordPress Admin
            'paged'             => get_query_var( 'paged' )
        ), $args2);

        //Assurance : Prise en charge
        if($post_type == 'assurance') {
            $args['meta_query'] = array(
        		'relation'		=> 'AND',
        		array(
        			'key'	 	=> 'pris_en_charge',
        			'value'	  	=> true,
        			'compare' 	=> '=',
        		)
        	);
        }

    	$wp_query = new WP_Query( $args );

    	if( $wp_query->have_posts() ):

            $count = $wp_query->post_count;

            ?>

            <div id="posts" class="lines">

                <?php

                while( $wp_query->have_posts() ): $wp_query->the_post();

                    $current_post = $wp_query->current_post;

                    if($current_post%$posts_per_line == 0) echo '<div class="line">';

                    get_template_part('template-parts/content', $post_type);

                    if( ($current_post+1)%$posts_per_line == 0) echo '</div>';

                endwhile;

                if( ($current_post+1)%$posts_per_line != 0) echo '</div>';

                genesis_posts_nav();

                ?>

            </div>

        <?php

    	endif;

    	wp_reset_query();

    ?>

    </section>

    <?php

}
