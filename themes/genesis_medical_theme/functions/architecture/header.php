<?php

remove_action('genesis_site_title', 'genesis_seo_site_title');
//remove_action('genesis_site_description', 'genesis_seo_site_description');

//Logo
remove_action ( 'genesis_header', 'sixteen_nine_site_gravatar', 5 );
add_action( 'genesis_header', 'header_add_logo', 5 );
function header_add_logo () {

    echo '<a class="site-logo" href="'.get_site_url().'">';

        get_svg_logo();

    echo '</a>';

    ?>

    <a href="" class="button mobile my_account">Mon compte</a>

    <?php

}

add_action ('genesis_header_right', 'show_header_button', 30 );
function show_header_button () {

    ?>

    <a href="" class="button my_account">Mon compte</a>

    <?php

}

// Repositions primary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_nav' );
add_action( 'genesis_header_right', 'genesis_do_nav' );
