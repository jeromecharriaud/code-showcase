<?php

function show_archive_filter () {

    $post_type = get_post_type();
    $taxonomy = get_object_taxonomies($post_type)[0];

    $terms = get_terms( array(
        'taxonomy'      => $taxonomy,
        'hide_empty'    => true,
        'parent'        => 0,
    ) );

    if($post_type == 'medecin') {
        $placeholder = 'Filtrer par spécialités médicales';
    }
    else {
        $placeholder = 'Toutes les catégories';
    }

    if(!empty($terms)):

        ?>

        <input id="page-link" hidden="hidden" value="<?php echo get_post_type_archive_link($post_type); ?>"/>

        <select class="select2" id="filter-category" name="filter-category">

            <option value="toutes"><?php echo $placeholder; ?></option>

            <?php

            foreach($terms as $term):

                $selected = '';

                if(!empty($_GET['categorie']) && $_GET['categorie'] == $term->slug) {
                    $selected = 'selected="selected"';
                }

                echo '<option '.$selected.' value="'.$term->slug.'">'.$term->name.'</option>';

            endforeach;

            ?>

        </select>

    <?php

    endif;

}
