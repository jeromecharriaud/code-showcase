<?php

remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
add_action('genesis_entry_content', 'show_page', 200);
function show_page () {

    if(!is_page()) return;

    if(basename(get_page_template()) === 'page.php'){

        ?>

        <div class="box">

            <h1 class="title"><?php the_title(); ?></h1>

            <?php the_content(); ?>

        </div>

        <?php

    }

}
