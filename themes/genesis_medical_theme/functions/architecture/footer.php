<?php

//Logo
remove_action( 'genesis_footer', 'genesis_do_footer' );
add_action( 'genesis_footer', 'show_custom_footer', 10 );
function show_custom_footer() {

    ?>

    <!--Sitemap-->
    <div class="logo">

        <?php get_svg_logo_footer(); ?>

    </div>

    <?php

}

//Footer Widgets
remove_action( 'genesis_before_footer', 'genesis_footer_widget_areas' );
add_action( 'genesis_footer', 'genesis_footer_widget_areas', 10 );

//Avertissement
add_action( 'genesis_after_footer', 'show_warning', 10 );
function show_warning() {

    ?>

    <!--Avertissement-->
    <div class="warning">

        <?php the_field('warning','option'); ?>

    </div>

    <?php

}

//Copyright
add_action( 'genesis_after_footer', 'show_copyright', 10 );
function show_copyright() {

    echo '<div class="copyright">';
        the_field('copyright','option');
    echo '</div>';

}

//Menu
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_after_footer', 'genesis_do_subnav', 10 );
