<?php

//Password protect
add_filter('acf/update_value/type=password', 'eudonet_encrypt_passwords', 10, 3);
function eudonet_encrypt_passwords($value, $post_id, $field) {

    $value = wp_hash_password($value);

    return $value;
}