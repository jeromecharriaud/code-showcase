<?php

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
        'page_title' 	=> 'Paramètres',
        'menu_title'	=> 'Paramètres',
        'menu_slug' 	=> 'options',
        'capability'	=> 'administrator',
        'redirect'		=> false
    ));


	acf_add_options_sub_page(array(
		'page_title' 	=> 'Composants',
		'menu_title'	=> 'Composants',
		'parent_slug'	=> 'options',
		'capability'	=> 'administrator',
	));

	acf_add_options_sub_page(array(
		'page_title' 	=> 'Pop-ups',
		'menu_title'	=> 'Pop-ups',
		'parent_slug'	=> 'options',
		'capability'	=> 'administrator',
	));

}