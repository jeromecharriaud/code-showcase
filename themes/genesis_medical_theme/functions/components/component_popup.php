<?php

function show_popup ($array) {

    if(empty($array)) return;

    ?>

	<div style="display:none;" class="popup <?php echo $array['class'];?>" data-value="<?php echo $array['value'];?>">

		<div class="content">

			<span class="close_popup"><i class="fas fa-2x fa-times"></i></span>

			<div class="one-half first">

                <div class="title"></div>

                <div class="title-2"></div>

                <?php if(!empty($array['description'])) { ?>
                    <div class="description"><?php echo $array['description'];?></div>
                <?php } ?>

			</div>

			<div class="one-half">

                <span class="assurance-content"></span>

                <?php if(!empty($array['content-header'])) { ?>
	                <span class="content-header"><?php echo $array['content-header'];?></span>
                <?php } ?>

			</div>

		</div>
	</div>

	<?php

}
