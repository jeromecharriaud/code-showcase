<?php

add_shortcode('trustpilot-micro', 'show_trustpilot_micro');
function show_trustpilot_micro() {

?>

    <!-- TrustBox widget - Micro TrustScore -->
    <div class="trustpilot-widget" data-locale="fr-FR" data-template-id="5419b637fa0340045cd0c936" data-businessunit-id="5beacd456920b3000195dc58" data-style-height="20px" data-style-width="100%" data-theme="light">
        <a href="https://fr.trustpilot.com/review/www.jcorp.fr" target="_blank" rel="noopener">Trustpilot</a>
    </div>
    <!-- End TrustBox widget -->

<?php

}
