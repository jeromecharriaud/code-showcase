<?php

function show_popup_newsletter () {

	$array = array(
		'value'				=> 'newsletter',
		'class'				=> 'newsletter',
		'title'				=> get_field('popup_newsletter_title_1', 'option'),
		'title-2'			=> get_field('popup_newsletter_title_2', 'option'),
		'description'		=> get_field('popup_newsletter_description', 'option'),
		'content' 			=> get_field('popup_newsletter_content','option'),
		'field_description' => '',
	);

	show_popup($array);

}
