<?php

add_shortcode('slider', 'show_slider');
function show_slider () {

    if(get_field('slider_display') == FALSE) return;

    if( have_rows('slides') ):

        ?>

        <section id="slider">

            <div class="owl-carousel owl-theme slider">

                <?php

                while ( have_rows('slides') ) : the_row();

                    if(!empty(get_sub_field('button_label')))
                        $button_label = get_sub_field('button_label');
                    else
                        $button_label = 'En savoir plus';

                    //Video
                    if(!empty(get_sub_field('video'))):

                        ?>

                        <a class="owl-video" href="<?php echo get_sub_field('video');?>"><span class="slider_title"><?php echo get_sub_field('title');?></span></a>

                        <?php

                    //Normal Content
                    else:

                        ?>

                        <div class="slide">

                            <div style="background-image:url('<?php echo get_sub_field('background');?>')" class="background"></div>

                            <div class="content">

                                <h2 class="title"><?php echo get_sub_field('title');?></h2>

                                <?php if(!empty(get_sub_field('description'))): ?>

                                    <span class="description"><?php echo get_sub_field('description', false, false); ?></span>

                                <?php endif; ?>

                                <?php if(!empty(get_sub_field('button_link'))): ?>

                                    <div><a class="button" style="" href="<?php echo get_sub_field('button_link');?>"><?php echo $button_label;?></a></div>

                                <?php endif; ?>

                            </div>

                        </div>

                    <?php

                    endif;

                endwhile;

                ?>

            </div>

        </section>

        <?php

    endif;

}
