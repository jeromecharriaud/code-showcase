<?php

//Ajax : Load posts
add_action( 'wp_enqueue_scripts', 'load_posts_script' );
add_shortcode('consult', 'show_consult');
function show_consult () {

	if(get_field('consult_display') == FALSE) return;

	$link = get_field('consult_cta');

	?>

	<section id="consult" class="box" style="background-image:url('<?php the_field('consult_background');?>')">

		<div class="consult_header">

			<h1 class="firstword consult_title"><?php the_field('consult_title_action');?> <?php the_field('consult_title_description');?></h1>

		</div>

		<div class="consult_description"><?php the_field('consult_description');?></div>

		<a class="button" href="<?php echo esc_url($link['url']); ?>" target="<?php echo esc_attr($link['target']); ?>"><?php echo esc_html($link['title']); ?></a>

		<div class="responsive_background"></div>

		<div class="trustpilot trustpilot-micro">
			<?php echo do_shortcode('[trustpilot-micro]'); ?>
		</div>

	</section>

	<?php

}
