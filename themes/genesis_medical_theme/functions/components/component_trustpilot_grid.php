<?php

add_shortcode('trustpilot-grid', 'show_trustpilot_grid');
function show_trustpilot_grid() {

?>

    <!-- TrustBox widget - Grid -->
    <div class="trustpilot-widget" data-locale="fr-FR" data-template-id="539adbd6dec7e10e686debee" data-businessunit-id="5beacd456920b3000195dc58" data-style-width="100%" data-theme="light" data-stars="4,5">
        <a href="https://fr.trustpilot.com/review/www.jcorp.fr" target="_blank" rel="noopener">Trustpilot</a>
    </div>
    <!-- End TrustBox widget -->

<?php

}
