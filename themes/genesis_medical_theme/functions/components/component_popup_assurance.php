<?php

function show_popup_assurance ($post_ID = '') {

	$array = array();

	$class = 'assurance_popup assurance_prise_en_charge';

	$array['true'] = array(
		'value'	=>	'true',
		'class'	=>	$class,
		'description'	=>	'offre le service.',
		'content' => get_the_content($post_ID),
		'content-header' => get_field('assurance_popup_true_description','option'),
	);

	$array['false'] = array(
		'value'	=>	'false',
		'class'	=>	$class,
		'description'	=>	"n'offre pas encore le service.",
		'content' => get_the_content($post_ID),
		'content-header' => get_field('assurance_popup_false_description','option'),
    );
    
	show_popup($array['true']);
	show_popup($array['false']);

}
