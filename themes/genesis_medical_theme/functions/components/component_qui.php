<?php

add_shortcode('qui_me_repond', 'show_qui_me_repond');
function show_qui_me_repond () {

    if(get_field('qui_me_repond_display') == FALSE) return;

    $link = get_field('qui_me_repond_cta');

    ?>

    <section id="qui_me_repond" class="box">

        <h2 class="title qui_me_repond_title"><?php the_field('qui_me_repond_title');?></h2>

        <div class="description qui_me_repond_description"><?php the_field('qui_me_repond_description');?></div>

        <div class="qui_me_repond_list">

            <?php

            if( have_rows('reponses') ):

                $i = 0;

                while ( have_rows('reponses') ) : the_row();

                    $classes = 'one-half reponse';

                    if($i % 2 == 0 || $i == 0) $classes .= ' first';

                    ?>

                    <div class="<?php echo $classes;?>">

                        <div class="image-content">

                            <img alt="image-qui-me-repond" src="<?php echo get_sub_field('image');?>"/>

                        </div>

                        <div class="content">

                            <h3 class="title"><?php echo get_sub_field('title');?></h3>

                            <div class="description"><?php echo get_sub_field('description');?></div>

                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <?php

                    $i++;

    				if ($i% 2 == 0) echo '<div class="clearfix"></div>';

                endwhile;

            endif;

            ?>

        </div>

        <a class="button" href="<?php echo esc_url($link['url']); ?>" target="<?php echo esc_attr($link['target']); ?>"><?php echo esc_html($link['title']); ?></a>

    </section>

    <?php

}
