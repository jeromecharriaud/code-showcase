<?php

add_shortcode('last-posts', 'show_last_posts');
function show_last_posts () {

    if(get_field('last_posts_display') == FALSE) return;

    ?>

    <section id="last_posts">

        <h2>Dernières Actualités</h2>

        <div id="posts" class="owl-carousel owl-theme mobile-carousel last_posts">

            <?php

            $args = array(
                'post_type'             => 'actualite',
                'posts_per_page'        => 8,
                'order'                 => 'DESC',
                'orderby'               => 'date',
            );

            $query = new WP_Query( $args );

            // The Loop
            if ( $query->have_posts() ) {

                while ( $query->have_posts() ) {

                    $query->the_post();

                    if(has_post_thumbnail()):
                        $background = get_the_post_thumbnail_url();
                    else:
                        $background = get_stylesheet_directory_uri().'/images/logo.png';
                    endif;

                    ?>

                    <div class="slide">

                        <a class="post" href="<?php echo get_the_permalink();?>">

                            <div style="background-image:url('<?php echo $background;?>')" class="one-third first background"></div>

                            <div class="title"><?php the_title();?></div>

                        </a>

                    </div>

                    <?php

                }

            }

            // Restore original Post Data
            wp_reset_postdata();

            ?>

        </div>

    </section>

    <?php

}
