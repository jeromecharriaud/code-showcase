<?php

add_shortcode('arguments', 'show_arguments');
function show_arguments () {

    if(get_field('arguments_display') == FALSE) return;

    ?>

    <section id="arguments" class="box">

        <div class="arguments_list">

            <?php

            if( have_rows('arguments_list') ):

                $i = 0;

                while ( have_rows('arguments_list') ) : the_row();

                    $classes = 'one-third argument';

                    if($i % 3 == 0 || $i == 0) $classes .= ' first';

                    ?>

                    <div class="<?php echo $classes;?>">

                        <div class="image-content">

                            <img alt="image-argument" src="<?php echo get_sub_field('image');?>"/>

                        </div>

                        <div class="content">

                            <h3 class="title"><?php echo get_sub_field('title');?></h3>

                            <div class="description"><?php echo get_sub_field('description');?></div>

                        </div>

                        <div class="clearfix"></div>

                    </div>

                    <?php

                    $i++;

    				if ($i% 3 == 0) echo '<div class="clearfix"></div>';

                endwhile;

            endif;

            ?>

        </div>

    </section>

    <?php

}
