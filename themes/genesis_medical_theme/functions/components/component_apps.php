<?php

add_shortcode('apps', 'show_apps');
function show_apps () {

    if(get_field('apps_display') == FALSE) return;

    $link = get_stylesheet_directory_uri().'/images/';

    ?>

	<section id="apps" class="box">

		<a target="_blank" class="android-app" href="<?php echo get_field('android-app');?>">
            <img width="180" height="54" alt="android-app" src="<?php echo $link ?>/android-app.png"/>
        </a>

		<a target="_blank" class="apple-app" href="<?php echo get_field('apple-app');?>">
            <img width="160" height="54" alt="apple-app" src="<?php echo $link ?>/apple-app.png"/>
        </a>

	</section>

	<?php


}
