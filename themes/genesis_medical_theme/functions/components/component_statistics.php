<?php

add_shortcode('statistics', 'show_statistics');
function show_statistics () {

	if(get_field('statistics_display') == FALSE) return;

	?>

	<section id="statistics" class="box">

		<div class="title"><?php the_field('statistics_title'); ?></div>

		<div class="one-half first">

			<?php

			if( have_rows('statistics') ):

				// loop through the rows of data
				while ( have_rows('statistics') ) : the_row();

					?>

					<div class="statistic">
						<span class="title"><?php the_sub_field('statistics_number');?></span>
						<span class="description"><?php the_sub_field('statistics_description');?></span>
					</div>

					<?php

				endwhile;

			endif;

			?>

		</div>

		<div class="one-half trustpilot">

			<?php echo do_shortcode('[trustpilot-mini]'); ?>

		</div>

		<div class="clearfix"></div>

	</section>

	<?php

}
