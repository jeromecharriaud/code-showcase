<?php

add_shortcode('certification', 'show_certification');
function show_certification () {

	if(get_field('certification_display') == FALSE) return;

	?>

	<section id="certification" class="box">

		<img alt="certification" src="<?php echo get_field('certification_image');?>"/>

		<div><?php the_field('certification_texte');?></div>

	</section>

	<?php

}
