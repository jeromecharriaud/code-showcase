<?php

add_shortcode('motifs', 'show_motifs');
function show_motifs () {

    if(get_field('motifs_display') == FALSE) return;

    //Sentences
    if( have_rows('motifs_sentences') ):

        $sentences = '';

     	// loop through the rows of data
        while ( have_rows('motifs_sentences') ) : the_row();

            $sentences .= '"'.get_sub_field('sentence').'",';

        endwhile;

        $sentences = rtrim($sentences, ",");
        $sentences = str_replace("'", '&apos;', $sentences);

    endif;

    ?>

    <section id="motifs" class="box">

		<h2><?php the_field('motifs_title');?></h2>

        <div class="motifs_descriptions">

            <div class="motifs_description motifs_description_1" data-period="2000">
                <span><?php the_field('motifs_description_1');?></span>
            </div>

            <div class="motifs_description motifs_description_2">
                <span class="typewrite" data-period="2000" data-type='[<?php echo $sentences;?>]'>
                    <span class="wrap"></span>
                </span>
            </div>

        </div>

        <div class="motifs">

            <?php

    		//Colonnes
    		for($i = 1; $i <= 3; $i++) {

    			$classes = 'one-third motif';

    			if( ($i-1) % 3 == 0 || $i == 1) $classes .= ' first';

    			?>

    			<div class="<?php echo $classes;?>">

    				<?php

    				if( have_rows('motifs_'.$i) ):

    					while ( have_rows('motifs_'.$i) ) : the_row();

    						echo '<div class="motif_bloc">';

    							echo '<span class="title">'.get_sub_field('title').'</span>';

    							if( have_rows('motifs') ):

    								while ( have_rows('motifs') ) : the_row();

    									echo '<span class="link">'.get_sub_field('link').'</span>';

    								endwhile;

    							endif;

    						echo '</div>';

    					endwhile;

    				endif;

    				?>

    			</div>

    			<?php

    			if ($i% 3 == 0) echo '<div class="clearfix"></div>';

    		}

    		?>

        </div>

    </section>

    <?php

}
