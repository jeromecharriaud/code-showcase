<?php

add_shortcode('banniere', 'show_banniere');
function show_banniere () {

	if(get_field('banniere_display') == FALSE) return;

	$link = get_stylesheet_directory_uri().'/images/';

	?>

	<section id="banniere" class="box">

		<div class="one-half image first">
			<img alt="banniere" src="<?php echo get_field('banniere_image');?>"/>
		</div>

		<div class="one-half text">

			<h2><?php the_field('banniere_title');?></h2>

			<div><?php the_field('banniere_description');?></div>

			<div class="apps">

				<a target="_blank" class="android-app" href="<?php echo get_field('android-app');?>">
					<img width="180" height="54" alt="android-app" src="<?php echo $link ?>/android-app.png"/>
				</a>

				<a target="_blank" class="apple-app" href="<?php echo get_field('apple-app');?>">
					<img width="180" height="54" alt="apple-app" src="<?php echo $link ?>/apple-app.svg"/>
				</a>

			</div>

		</div>

		<div class="clearfix"></div>



	</section>

	<?php

}
