<?php

function show_popup_contact () {

	$array = array(
		'value'				=> 'contact',
		'class'				=> 'contact',
		'title'				=> get_field('popup_contact_title_1', 'option'),
		'title-2'			=> get_field('popup_contact_title_2', 'option'),
		'description'		=> get_field('popup_contact_description', 'option'),
		'content' 			=> get_field('popup_contact_content','option'),
		'field_description' => '',
	);

	show_popup($array);

}
