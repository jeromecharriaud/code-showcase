<?php

//Ajax : Load posts
add_action( 'wp_enqueue_scripts', 'load_posts_script' );

add_shortcode('search-assurances', 'show_search_assurances');
function show_search_assurances () {

	if(get_field('form_display') == FALSE) return;

	$form_select = get_field('form_select');
	$form_shortcode = get_field('form_select');

	?>

	<section id="form" class="box">

		<div class="form_header">

			<div class="form_before_title"><?php echo get_field('form_before_title');?></div>

			<h2 class="before_form">
				<div class="form_title_action"><?php echo get_field('form_title_action');?></div>
				<div class="form_title_description"><?php echo get_field('form_title_description');?></div>
			</h2>

		</div>

		<div class="form_content">

			<?php if ($form_select == 'assurances') { ; ?>

				<form id="search_form" class="search_posts search_assurances">

					<input type="hidden" class="post_type" name="post_type" value="assurance">

					<input class="target" autocomplete="off" id="title" name="title" placeholder="Recherchez votre assureur" />

				</form>

				<div class="form_footer"><?php the_field('form_footer');?></div>

				<div class="ajax_content"></div>

			<?php }

			else do_shortcode($form_shortcode);

			?>

		</div>

	</section>

	<?php

}
