<?php

add_shortcode('services', 'show_services');
function show_services () {

    if(get_field('services_display') == FALSE) return;

    ?>

<section id="services" class="box">

    <h2 class="title services_title firstword"><?php the_field('services_title');?></h2>

    <div class="description services_description"><?php the_field('services_description');?></div>

    <div class="services_list">

        <?php

            if( have_rows('services') ):

                $i = 0;

                while ( have_rows('services') ) : the_row();

                    $classes = 'one-third service';

                    if($i % 3 == 0 || $i == 0) $classes .= ' first';

                    ?>

        <div class="<?php echo $classes;?>">

            <img alt="icone-service" src="<?php echo get_sub_field('icon');?>" />

            <div class="content">

                <h3 class="title"><?php echo get_sub_field('title');?></h3>

                <div class="description"><?php echo get_sub_field('description');?></div>

            </div>

            <div class="clearfix"></div>

        </div>

        <?php

                    $i++;

    				if ($i% 3 == 0) echo '<div class="clearfix"></div>';

                endwhile;

            endif;

            ?>

    </div>

</section>

<?php

}