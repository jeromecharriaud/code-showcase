<?php

add_shortcode('medecins', 'show_medecins');
function show_medecins() {

    if (get_field('medecins_display') == FALSE) return;

?>

    <section id="medecins" class="box">

        <h2 class="title"><?php the_field('medecins_title'); ?></h2>

        <p class="description"><?php the_field('medecins_description'); ?></p>

        <div id="slider" class="owl-carousel owl-theme mobile-carousel medecins">

            <?php

            $args = array(
                'post_type'             => 'medecin',
                'posts_per_page'        => 1000,
                'order'                 => 'DESC',
                'orderby'               => 'title',
            );

            $query = new WP_Query($args);

            // The Loop
            if ($query->have_posts()) {

                while ($query->have_posts()) {

                    $query->the_post();

                    if (has_post_thumbnail()) :
                        $background = get_the_post_thumbnail_url();
                    else :
                        $background = '';
                    endif;

            ?>

                    <div class="slide post">

                        <div style="background-image:url('<?php echo $background; ?>')" class="one-half first background"></div>

                        <div class="one-half medecin-content">
                            <div>
                                <div class="first_name"><?php the_field('first_name'); ?></div>
                                <div class="last_name"><?php the_field('last_name'); ?></div>
                                <div class="job_position"><?php the_field('job_position'); ?></div>
                            </div>
                        </div>

                    </div>

            <?php

                }
            }

            // Restore original Post Data
            wp_reset_postdata();

            ?>

        </div>

        <div style="text-align:center;"><a class="button" href="/medecins">Voir tous les médecins</a></div>

    </section>

<?php

}
