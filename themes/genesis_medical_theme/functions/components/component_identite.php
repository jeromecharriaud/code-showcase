<?php

add_shortcode('identite', 'show_identite');
function show_identite () {

	if(get_field('identite_display') == FALSE) return;

	//Défault
	$title = get_field('identite_title', 'option');
	$description = get_field('identite_description', 'option');
	$links = get_field('identite_links', 'option');

	if(get_field('identite_title', get_the_ID())) $title = get_field('identite_title', get_the_ID());
	if(get_field('identite_description', get_the_ID())) $description = get_field('identite_description', get_the_ID());
	if(get_field('identite_links', get_the_ID())) $links = get_field('identite_links', get_the_ID());

	?>

    <section id="identite" class="box">

        <h2><?php echo $title;?></h2>
        <div><?php echo $description;?></div>

        <div class="links" style="">

            <?php

                if(!empty($links)):

                    foreach($links as $key => $link):

                        ?>
                        <a class="button" href="<?php echo esc_url($link['link']['url']); ?>"
                            target="<?php echo esc_attr($link['link']['target']); ?>">
                            <span><?php echo esc_html($link['link']['title']); ?></span>
                        </a>
                        <?php

                    endforeach;

                endif;

                ?>

        </div>

    </section>

    <?php

}