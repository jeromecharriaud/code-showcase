<?php

function show_popup_know_more () {

	$array = array(
		'value'				=> 'know_more',
		'class'				=> 'know_more',
		'title'				=> get_field('popup_know_more_title_1', 'option'),
		'title-2'			=> get_field('popup_know_more_title_2', 'option'),
		'description'		=> get_field('popup_know_more_description', 'option'),
		'content' 			=> get_field('popup_know_more_content','option'),
		'field_description' => '',
	);

	show_popup($array);

}
