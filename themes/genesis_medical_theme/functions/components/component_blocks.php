<?php

add_shortcode('blocs', 'show_blocks');
function show_blocks () {

    if(get_field('blocks_display') == FALSE) return;

    if( have_rows('blocks') ):

        $i = 0;

        ?>

        <section id="blocks" class="box">

            <div class="owl-carousel owl-theme mobile-carousel mobile-only">

            <?php

                $i = 0;

                while ( have_rows('blocks') ) : the_row();

                    $classes = 'block one-half';

                    ?>

                    <div class="line slide">

                        <?php if($i%2 == 0 || wp_is_mobile()) : ?>

                            <div class="one-half first background" style="background-image:url('<?php echo get_sub_field('background');?>')"></div>

                            <div class="one-half content">
                                <div class="title"><?php the_sub_field('title');?></div>
                                <div class="description"><?php the_sub_field('description');?></div>
                            </div>

                        <?php else: ?>

                            <div class="one-half first content">
                                <div class="title"><?php the_sub_field('title');?></div>
                                <div class="description"><?php the_sub_field('description');?></div>
                            </div>

                            <div class="one-half background" style="background-image:url('<?php echo get_sub_field('background');?>')"></div>

                        <?php endif; ?>

                    </div>

                    <?php

                    $i++;

                endwhile;

            ?>

            </div>

        </section>

        <?php

    endif;

}
