<?php

add_shortcode('trustpilot-mini', 'show_trustpilot_mini');
function show_trustpilot_mini() {

?>

    <!-- TrustBox widget - Mini -->
    <div class="trustpilot-widget" data-locale="fr-FR" data-template-id="53aa8807dec7e10d38f59f32" data-businessunit-id="5beacd456920b3000195dc58" data-style-height="150px" data-style-width="100%" data-theme="light">
        <a href="https://fr.trustpilot.com/review/www.jcorp.fr" target="_blank" rel="noopener">Trustpilot</a>
    </div>
    <!-- End TrustBox widget -->

<?php

}
