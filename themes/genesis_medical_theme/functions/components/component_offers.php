<?php

add_shortcode('offers', 'show_offers');
function show_offers () {

    if(get_field('offers_display') == FALSE) return;

    ?>

    <section id="offers" class="box">

		<h2>Nos offres</h2>

        <div class="lines">

            <?php
    		//Colonnes
            if( have_rows('offers') ):

                $i = 0;

                while ( have_rows('offers') ) : the_row();

    		         $classes = 'one-half column';
                     if($i == 0 || $i%2 == 0) $classes .= ' first';
                     if($i%2 == 0) echo '<div class="line">';

                     ?>

                     <div class="<?php echo $classes;?>">

                        <h3 class="title"><?php the_sub_field('title');?></h3>

                        <?php
                        if( have_rows('offer') ):

                            while ( have_rows('offer') ) : the_row();

                                ?>
                                <div class="offer">

                                    <div class="background" style="background-image:url('<?php the_sub_field('image');?>');"></div>
                                    <h4 class="title"><?php the_sub_field('title');?></h4>
                                    <div class="description"><?php the_sub_field('description');?></div>

                                </div>

                                <?php

                            endwhile;

                        endif;
                        ?>

                        <div class="price">
                            <div class="price_amount"><b><?php the_sub_field('price');?></b> €/<?php the_sub_field('period');?></div>
                            <div class="price_description"><?php the_sub_field('price_description');?></div>
                        </div>

                     </div>

                     <?php

                     $i++;

                     if($i%2 == 0) echo '</div>';

                endwhile;

            endif;

    		?>

        </div>

    </section>

    <?php

}
