<?php

function enqueue_tabs_scripts () {

    wp_enqueue_script('jquery-ui-tabs');

    wp_enqueue_script(
		'tabs',
		get_stylesheet_directory_uri() . '/js/tabs.js',
		array( 'jquery' ),
		CHILD_THEME_VERSION,
		true
	);

}

function enqueue_tabs_style () {

	wp_enqueue_style( 'tabs-css', get_stylesheet_directory_uri() . '/css/tabs.css', array());

}
