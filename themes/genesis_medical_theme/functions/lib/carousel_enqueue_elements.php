<?php

add_action( 'wp_enqueue_scripts', 'carousel_enqueue_scripts' );
function carousel_enqueue_scripts() {

    wp_enqueue_style( 'owlcarousel', get_stylesheet_directory_uri() . '/lib/owlcarousel/dist/assets/owl.carousel.min.css', array());

    wp_enqueue_style( 'owlcarousel-theme', get_stylesheet_directory_uri() . '/lib/owlcarousel/dist/assets/owl.theme.default.min.css', array());

    wp_enqueue_script('owlcarousel-scripts', get_stylesheet_directory_uri() . '/lib/owlcarousel/dist/owl.carousel.min.js', array( 'jquery' ), filemtime(plugin_dir_path( __FILE__ )), true);

    wp_enqueue_script('owlcarousel-init', get_stylesheet_directory_uri() . '/js/owlcarousel-init.js', array( 'jquery' ), filemtime(plugin_dir_path( __FILE__ )), true);

}
