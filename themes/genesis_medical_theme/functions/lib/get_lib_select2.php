<?php

//Select 2
function get_lib_select2 () {

    wp_enqueue_style( 'select2-css', get_stylesheet_directory_uri() .'/lib/select2-4.0.5/dist/css/select2.min.css', __FILE__ );

    wp_enqueue_script('select2', get_stylesheet_directory_uri() .'/lib/select2-4.0.5/dist/js/select2.min.js',__FILE__ , array( 'jquery' ), '4.0.5', true);

    //Config
    wp_enqueue_script('select2-config', get_stylesheet_directory_uri().'/js/get_select2_config.js', array( 'jquery' ), '1.0.0', true);

}

?>
