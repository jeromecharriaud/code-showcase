<?php

add_action( 'wp_body_open', 'add_google_tag_manager' );
function add_google_tag_manager() {
	?>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo get_field('google_tag_manager','option');?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
	<?php
}
