<?php

add_action( 'wp_ajax_nopriv_load_posts', 'load_posts' );
add_action( 'wp_ajax_load_posts', 'load_posts' );
function load_posts() {

    $post_type = $_POST['post_type'];

    $args = array(
        'post_type'         => $_POST['post_type'],
        'orderby'           => 'title',
        'posts_per_page'    => 5,
        'order'             => 'ASC',
    );

    //Name
    if(isset($_POST['title']) && !empty($_POST['title'])):

        $args['s'] = $_POST['title'];

    endif;

    $query = new WP_Query( $args );

    echo '<div class="liste_assurances">';

        if ( $query->have_posts() ) {

            while ( $query->have_posts() ) {

                $query->the_post();

                $pris_en_charge = get_field('pris_en_charge');

                if($pris_en_charge == TRUE) {
                    $pris_en_charge_message = '<div class="prise_en_charge" data-value="true">100% pris en charge</div>';
                    $message = '<a class="connection" data-value="true" href="">Se connecter</a>';
                }
                else {
                    $pris_en_charge_message = '<div class="prise_en_charge" data-value="false">non pris en charge</div>';
                    $message = '<a class="connection" data-value="false" href="">Je veux être prévenu quand le service sera pris en charge</a>';
                }

                ?>

    			<div class="assurance_found" data-value="<?php echo $pris_en_charge; ?>">
                    <div class="assurance_title"><?php echo get_the_title(); ?></div>
                    <div style="display:none;" class="assurance_content"><?php echo get_the_content(); ?></div>
                    <?php echo $pris_en_charge_message; ?>
                    <?php echo $message; ?>
                </div>

                <?php

            }

            wp_reset_postdata();

            ?>

            <a class="assurance_found" href="/partenaires">Voir la liste complète des partenaires de MédecinDirect</a>

            <?php

        }

        else {

            ?>

            <div class="assurance_found">Aucune assurance trouvée.</div>

            <?php

        }

    echo '</div>';

    die();

}
