<?php

function load_posts_script() {

    wp_enqueue_script(
        'load_post',
        get_stylesheet_directory_uri() . '/js/ajax-load-posts.js',
        array( 'jquery' ),
        filemtime(plugin_dir_path( __FILE__ )),
        true
    );

    wp_localize_script(
        'load_post',
        'ajax_object',
        array(
            'ajaxurl'       => admin_url( 'admin-ajax.php' ),
            'loadingimage'  => get_stylesheet_directory_uri() . '/images/loading.gif',
        )
    );

}
