<?php

class Related_Posts_Widget extends WP_Widget {

    /*
	 * Register widget with WordPress.
	*/
    function __construct() {
        parent::__construct(
            'related_posts_widget', // Base ID
            __('Articles similaires', 'text_domain'), // Name
            array('description' => __('Affiche les articles similaires', 'text_domain'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     */
    public function widget($args, $instance) {

        // Catch results
        $post_id = $GLOBALS['post']->ID;

        $categories = wp_get_post_categories($post_id, array('fields' => 'ids'));

        if ($categories) {

            $related_search = array(
                'post_type'    =>    get_post_type(),
                'category__in' => $categories,
                'posts_per_page' => 3,
                'orderby' => 'rand'
            );

            $my_query = new WP_Query($related_search);

            if ($my_query->have_posts()) {

                echo $args['before_widget'];

                if (!empty($instance['title'])) {
                    echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
                }


                echo '<div id="posts" class="article-related">';
                while ($my_query->have_posts()) {
                    $my_query->the_post();
                    $permalink = get_permalink();
                    $title_attribute = the_title_attribute(array('echo' => false));
                    $title = the_title('', '', false);

                    get_template_part('template-parts/content', '');
                }
                echo '</div>';
                wp_reset_query();
                echo $args['after_widget'];
            }
        }
    }
    /**
     * Back-end widget form.
     */
    public function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('Articles similaires', 'text_domain');
?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
<?php
    }
    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */

    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
}

/***********************************
 ****** Register the widget *******
 **********************************/
function register_related_posts_widget() {
    register_widget('Related_Posts_Widget');
}
add_action('widgets_init', 'register_related_posts_widget');
