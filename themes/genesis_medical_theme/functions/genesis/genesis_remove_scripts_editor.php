<?php

add_action( 'admin_menu' , 'remove_genesis_page_post_scripts_box' );
function remove_genesis_page_post_scripts_box() {

    $types = array( 'post','page' );

    remove_meta_box( 'genesis_inpost_scripts_box', $types, 'normal' ); 
    
}