<?php

/** Customise the post info info function */
add_filter( 'genesis_post_info', 'genesischild_post_info' );
function genesischild_post_info($post_info) {
     
    if (!is_page()) {
         
        $post_info = 'Posted on [[post_date]] [[post_comments]] [[post_edit]]';
        return $post_info;
         
     }
}