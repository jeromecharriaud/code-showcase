<?php

//Custom Style
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 9000);
function child_enqueue_styles() {

    //Child Theme
    wp_enqueue_style( 'style-child-theme', get_stylesheet_directory_uri() . '/css/style-child-theme.css', array(), filemtime(plugin_dir_path( __FILE__ )) );

    //Font awesome
    wp_enqueue_style( 'font-awesome', get_stylesheet_directory_uri() . '/css/fontawesome.min.css', array(), filemtime(plugin_dir_path( __FILE__ )) );

    //Page
    if(is_page()) {

        wp_enqueue_style( 'style-page', get_stylesheet_directory_uri().'/css/style-page.css', array(), filemtime(plugin_dir_path( __FILE__ )) );

    }

    //Archive
    if(is_archive()) {
        wp_enqueue_style( 'style-archive', get_stylesheet_directory_uri() . '/css/style-archive.css', array(), filemtime(plugin_dir_path( __FILE__ )) );
    }

    //Single
    if(is_single()) {
        wp_enqueue_style( 'style-single', get_stylesheet_directory_uri() . '/css/style-single.css', array(), filemtime(plugin_dir_path( __FILE__ )) );
    }

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_responsive', 9999);
function child_enqueue_responsive() {

    // if(wp_is_mobile()) {

        wp_enqueue_style( 'style-responsive', get_stylesheet_directory_uri() . '/css/style-responsive.css', array(), filemtime(plugin_dir_path( __FILE__ )) );

    // }

}
