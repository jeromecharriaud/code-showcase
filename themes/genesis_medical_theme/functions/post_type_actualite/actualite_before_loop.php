<?php

function show_actualite_before_loop () {

    remove_action ('genesis_loop', 'genesis_do_loop');

    $post_type = get_post_type();
    $post_type_object = get_post_type_object($post_type);
    $labels = $post_type_object->labels;

    ?>

    <div style="background-image:url('<?php echo get_field($post_type.'_background', 'option');?>')" class="post_type_background background"></div>
        <h1><?php echo $labels->name;?></h1>
    </div>

    <?php

}
