<?php

// Register Custom Post "actualites"
add_action( 'init', 'post_type_actualite', 0 );
function post_type_actualite() {

	$labels = array(
		'name'                  => __( 'Actualités', 'presanse' ),
		'singular_name'         => __( 'Actualité', 'presanse' ),
		'menu_name'             => __( 'Actualités', 'presanse' ),
		'name_admin_bar'        => __( 'Actualité', 'presanse' ),
	);
	$rewrite = array(
		'slug'                  => 'actualites',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$capabilities = array(
		'edit_post'             => 'edit_actualites',
		'read_post'             => 'read_actualites',
		'delete_post'           => 'delete_actualites',
		'edit_posts'            => 'edit_several_actualites',
		'edit_others_posts'     => 'edit_others_actualites',
		'publish_posts'         => 'publish_actualites',
		'read_private_posts'    => 'read_private_actualites',
	);
	$args = array(
		'label'                 => __( 'Actualités', 'presanse' ),
		'description'           => __( 'Description', 'presanse' ),
		'labels'                => $labels,
		'supports' 				=> array('title', 'editor', 'thumbnail', 'excerpt'),
		'taxonomies'            => array('category', 'post_tag'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'menu_icon'				=> 'dashicons-admin-post',
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'				=> $rewrite,
		'capability_type'       => 'actualites',
		'map_meta_cap' 			=> true,
		'capabilities'          => $capabilities,

	);

	register_post_type( 'actualite', $args );

}
