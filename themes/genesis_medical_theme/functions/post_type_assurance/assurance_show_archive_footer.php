<?php

function show_archive_assurance_footer () {

	$link = get_field('assurances_call_to_action', 'option');

	?>

	<section id="archive-footer" class="box">

		<h2 ><?php the_field('assurances_footer_title', 'option');?></h2>

		<div><?php the_field('assurances_footer_description', 'option');?></div>

		<?php

		if($link):

			$link_url = $link['url'];
			$link_title = $link['title'];
			$link_target = $link['target'] ? $link['target'] : '_self';

			?>

			<a class="button button-2" href="<?php echo esc_url($link_url); ?>" target="<?php echo esc_attr($link_target); ?>"><?php echo esc_html($link_title); ?></a>

		<?php

		endif;

		?>

	</section>

	<?php

}
