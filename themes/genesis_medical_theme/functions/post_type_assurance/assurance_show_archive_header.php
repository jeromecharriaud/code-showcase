<?php

function show_assurance_archive_header () {

	?>

	<section id="archive-header" class="box">

		<div><?php the_field('assurances_header', 'option');?></div>

		<h1>
			<div><b><?php the_field('assurances_title_1', 'option');?></b></div>
			<div><span><?php the_field('assurances_title_2', 'option');?></span></div>
		</h1>

	</section>

	<?php

}
