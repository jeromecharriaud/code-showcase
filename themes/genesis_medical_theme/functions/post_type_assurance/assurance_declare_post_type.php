<?php

// Register Custom Post assurance
add_action( 'init', 'post_type_assurance', 0 );
function post_type_assurance() {

	$labels = array(
		'name'                  => 'Assurances',
		'singular_name'         => 'Assurance',
		'menu_name'             => 'Assurances',
		'name_admin_bar'        => 'Assurance',
	);
	$rewrite = array(
		'slug'                  => 'assurances',
		'with_front'            => true,
		'pages'                 => true,
		'feeds'                 => true,
	);
	$capabilities = array(
		'edit_post'             => 'edit_assurance',
		'read_post'             => 'read_assurance',
		'delete_post'           => 'delete_assurance',
		'edit_posts'            => 'edit_assurances',
		'edit_others_posts'     => 'edit_others_assurances',
		'publish_posts'         => 'publish_assurances',
		'read_private_posts'    => 'read_private_assurances',
	);
	$args = array(
		'label'                 => __( 'Assurance', 'child-theme' ),
		'description'           => __( 'Description', 'child-theme' ),
		'labels'                => $labels,
		'supports' 				=> array('title', 'editor', 'thumbnail'),
		'taxonomies'            => array(''),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'menu_icon'				=> 'dashicons-building',
		'menu_position'         => 8,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'				=> $rewrite,
		'capability_type'       => 'assurance',
		'map_meta_cap' 			=> true,
		'capabilities'          => $capabilities,

	);

	register_post_type( 'assurance', $args );

}
