<?php

add_action('rest_api_init', function () {
    register_rest_route('jcorp/v1', '/assurances/', array(
        'methods' => 'GET',
        'callback' => 'endpoint_post_type_assurance'
    ));
});

function  endpoint_post_type_assurance($request_data) {

    $args = array(
        'post_type'         => 'assurance',
        'posts_per_page'    =>  1000,
        'numberposts'       =>  500
    );

    $posts = get_posts($args);

    if (!empty($posts)) {

        foreach ($posts as $key => $post) {

            $posts[$key]->acf = get_fields($post->ID);
            $posts[$key]->link = get_permalink($post->ID);

            if (has_post_thumbnail($post->ID)) {
                $posts[$key]->image = get_the_post_thumbnail_url($post->ID);
            }
        }
    }

    return  $posts;
}
