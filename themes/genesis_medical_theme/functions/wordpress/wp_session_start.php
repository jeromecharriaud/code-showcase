<?php
add_action( 'init', '_session_start', 1 );
function _session_start() {

   if ( ! session_id() ) {

      @session_start();

   }

}
