<?php

//Redirection After Login
add_filter( 'login_redirect', 'wp_redirect_after_login', 10, 3 );
function wp_redirect_after_login ( $redirect_to, $request, $user ) {

	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		return home_url();
	}
	else {
		return $redirect_to;
	}
    
}
