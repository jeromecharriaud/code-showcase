<?php

/**
 * Plugin Name: JCorp - Documents Inbox
 * Plugin URI:  https://www.inovagora.net/
 * Description: Plugin for sending documents to users
 * Author:      Inovagora
 * Author URI:  https://www.inovagora.net/
 * Version:     1.0.0
 * Text Domain: jcorp-docs-inbox
 * Domain Path: /languages/
 * License:     GPLv3 or later (license.txt)
 */

/**
 * This file should always remain compatible with the minimum version of
 * PHP supported by WordPress.
 */

// Exit if accessed directly
defined('ABSPATH') || exit;

//Plugin Activation
register_activation_hook(__FILE__, array('JCorp_Docs_Inbox_BB_Platform_Addon', 'activation_hook'));

if (!class_exists('JCorp_Docs_Inbox_BB_Platform_Addon')) {

    /**
     * Main JCorp Docs Inbox Class
     *
     * @class JCorp_Docs_Inbox_BB_Platform_Addon
     * @version    1.0.0
     */
    final class JCorp_Docs_Inbox_BB_Platform_Addon {

        /**
         * @var JCorp_Docs_Inbox_BB_Platform_Addon The single instance of the class
         * @since 1.0.0
         */
        protected static $_instance = null;

        /**
         * Main JCorp_Docs_Inbox_BB_Platform_Addon Instance
         *
         * Ensures only one instance of JCorp_Docs_Inbox_BB_Platform_Addon is loaded or can be loaded.
         *
         * @since 1.0.0
         * @static
         * @see JCorp_Docs_Inbox_BB_Platform_Addon()
         * @return JCorp_Docs_Inbox_BB_Platform_Addon - Main instance
         */
        public static function instance() {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * Cloning is forbidden.
         * @since 1.0.0
         */
        public function __clone() {
            _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'jcorp-docs-inbox'), '1.0.0');
        }
        /**
         * Unserializing instances of this class is forbidden.
         * @since 1.0.0
         */
        public function __wakeup() {
            _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'jcorp-docs-inbox'), '1.0.0');
        }

        /**
         * JCorp_Docs_Inbox_BB_Platform_Addon Constructor.
         */
        public function __construct() {
            $this->define_constants();
            $this->includes();
            // Set up localisation.

            $this->load_plugin_textdomain();

            add_action('bp_core_install_emails', array($this, 'nw_docs_inbox_new_document_received'));
            add_action('bp_core_install_emails', array($this, 'nw_docs_inbox_document_modified'));
        }

        /**
         * Define WCE Constants
         */
        private function define_constants() {
            $this->define('JCORP_DOCS_INBOX_BB_ADDON_PLUGIN_FILE', __FILE__);
            $this->define('JCORP_DOCS_INBOX_BB_ADDON_PLUGIN_BASENAME', plugin_basename(__FILE__));
            $this->define('JCORP_DOCS_INBOX_BB_ADDON_PLUGIN_PATH', plugin_dir_path(__FILE__));
            $this->define('JCORP_DOCS_INBOX_BB_ADDON_PLUGIN_URL', plugin_dir_url(__FILE__));
            $this->define('JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG', __('my-documents', 'jcorp-docs-inbox'));
        }

        /**
         * Define constant if not already set
         * @param  string $name
         * @param  string|bool $value
         */
        private function define($name, $value) {
            if (!defined($name)) {
                define($name, $value);
            }
        }

        /**
         * Include required core files used in admin and on the frontend.
         */
        public function includes() {
            $includes = glob(JCORP_DOCS_INBOX_BB_ADDON_PLUGIN_PATH . 'includes/*/*.php');

            //Include All Files
            foreach ($includes as $include) {

                require_once $include;
            }
        }

        /**
         * Load Localisation files.
         *
         * Note: the first-loaded translation file overrides any following ones if the same translation is present.
         */
        public function load_plugin_textdomain() {
            $locale = is_admin() && function_exists('get_user_locale') ? get_user_locale() : get_locale();
            $locale = apply_filters('plugin_locale', $locale, 'jcorp-docs-inbox');

            unload_textdomain('jcorp-docs-inbox');
            load_textdomain('jcorp-docs-inbox', WP_LANG_DIR . '/' . plugin_basename(dirname(__FILE__)) . '/' . plugin_basename(dirname(__FILE__)) . '-' . $locale . '.mo');
            load_plugin_textdomain('jcorp-docs-inbox', false, plugin_basename(dirname(__FILE__)) . '/languages');
        }

        public static function activation_hook() {
            self::nw_docs_inbox_new_document_received();
            self::nw_docs_inbox_document_modified();
            self::nw_docs_inbox_insert_wpai_template();
        }

        public static function nw_docs_inbox_new_document_received() {

            $mail_subject = __('[{{{site.name}}}] You have received a new document', 'jcorp-docs-inbox');

            if (post_exists($mail_subject)) {
                return;
            }

            // Create new document
            $my_post = array(
                'post_title'   => $mail_subject,
                'post_content' => __('You have received a new document : {{{profile.url}}}', 'jcorp-docs-inbox'), // HTML email content.
                'post_excerpt' => __('A new document has been received', 'jcorp-docs-inbox'), // Plain text email content.
                'post_status'  => 'publish',
                'post_type'    => bp_get_email_post_type(), // this is the post type for emails
            );

            // Insert the email post into the database
            $post_id = wp_insert_post($my_post);

            if ($post_id) {
                $tt_ids = wp_set_object_terms($post_id, 'document_new', bp_get_email_tax_type());
                foreach ($tt_ids as $tt_id) {
                    $term = get_term_by('term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type());
                    wp_update_term((int) $term->term_id, bp_get_email_tax_type(), array(
                        'description' => __('A document has been sent', 'jcorp-docs-inbox'),
                    ));
                }
            }
        }

        public static function nw_docs_inbox_document_modified() {

            $mail_subject = __('[{{{site.name}}}] One or several documents have been changed', 'jcorp-docs-inbox');

            if (post_exists($mail_subject)) {
                return;
            }

            // Create new document
            $my_post = array(
                'post_title'   => $mail_subject,
                'post_content' => __('One or several documents have been changed : {{{profile.url}}}', 'jcorp-docs-inbox'), // HTML email content.
                'post_excerpt' => __('One or several documents have been changed', 'jcorp-docs-inbox'), // Plain text email content.
                'post_status'  => 'publish',
                'post_type'    => bp_get_email_post_type(), // this is the post type for emails
            );

            // Insert the email post into the database
            $post_id = wp_insert_post($my_post);

            if ($post_id) {
                $tt_ids = wp_set_object_terms($post_id, 'document_modified', bp_get_email_tax_type());
                foreach ($tt_ids as $tt_id) {
                    $term = get_term_by('term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type());
                    wp_update_term((int) $term->term_id, bp_get_email_tax_type(), array(
                        'description' => __('One or several documents have been changed', 'jcorp-docs-inbox'),
                    ));
                }
            }
        }

        public static function nw_docs_inbox_insert_wpai_template() {

            global $wpdb;

            $table = $wpdb->prefix . 'pmxi_templates';
            $name  = __('Shared Docs', 'jcorp-docs-inbox');

            //Let's see if the template doesn't exist already
            $template_exists = $wpdb->get_var($wpdb->prepare("SELECT COUNT(*) FROM {$table} WHERE name = %s", $name));

            if ($template_exists > 0) {
                return;
            }

            $content = file_get_contents(__DIR__ . '/assets/wpai_templates/wpai_shared_docs_template.txt');

            $wpdb->insert($table, array(
                'options'            => $content,
                'scheduled'          => '',
                'name'               => $name,
                'title'              => '',
                'content'            => '',
                'is_keep_linebreaks' => '',
                'is_leave_html'      => '',
                'fix_characters'     => '',
                'meta'               => '',
            ));
        }
    }

    /**
     * Returns the main instance of JCorp_Docs_Inbox_BB_Platform_Addon to prevent the need to use globals.
     *
     * @since  1.0.0
     * @return JCorp_Docs_Inbox_BB_Platform_Addon
     */
    function JCorp_Docs_Inbox_BB_Platform_Addon() {
        return JCorp_Docs_Inbox_BB_Platform_Addon::instance();
    }

    function JCorp_Docs_Inbox_BB_Platform_install_bb_platform_notice() {
        echo '<div class="error fade"><p>';
        _e('<strong>JCorp Docs Inbox</strong></a> requires the BuddyBoss Platform plugin to work. Please <a href="https://buddyboss.com/platform/" target="_blank">install BuddyBoss Platform</a> first.', 'jcorp-docs-inbox');
        echo '</p></div>';
    }

    function JCorp_Docs_Inbox_BB_Platform_update_bb_platform_notice() {
        echo '<div class="error fade"><p>';
        _e('<strong>JCorp Docs Inbox</strong></a> requires BuddyBoss Platform plugin version 1.2.6 or higher to work. Please update BuddyBoss Platform.', 'jcorp-docs-inbox');
        echo '</p></div>';
    }

    function JCorp_Docs_Inbox_BB_Platform_is_active() {
        if (defined('BP_PLATFORM_VERSION') && version_compare(BP_PLATFORM_VERSION, '1.2.6', '>=')) {
            return true;
        }
        return false;
    }

    function JCorp_Docs_Inbox_BB_Platform_init() {
        if (!defined('BP_PLATFORM_VERSION')) {
            add_action('admin_notices', 'JCorp_Docs_Inbox_BB_Platform_install_bb_platform_notice');
            add_action('network_admin_notices', 'JCorp_Docs_Inbox_BB_Platform_install_bb_platform_notice');
            return;
        }

        if (version_compare(BP_PLATFORM_VERSION, '1.2.6', '<')) {
            add_action('admin_notices', 'JCorp_Docs_Inbox_BB_Platform_update_bb_platform_notice');
            add_action('network_admin_notices', 'JCorp_Docs_Inbox_BB_Platform_update_bb_platform_notice');
            return;
        }

        JCorp_Docs_Inbox_BB_Platform_Addon();
    }

    add_action('plugins_loaded', 'JCorp_Docs_Inbox_BB_Platform_init', 9);
}
