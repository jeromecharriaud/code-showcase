<?php

add_action('acf/save_post', 'nw_docs_inbox_notification_users_added', 2);
function nw_docs_inbox_notification_users_added($post_id) {

    if (get_post_type($post_id) != 'shared_doc') {
        return;
    }

    $users_new = $_POST['acf']['field_5ed9e83a593d6'];
    $users_old = get_field('users', $post_id);

    if (!empty($users_old) && !empty($users_new)) {

        foreach ($users_new as $user_content) {

            $list_users_new[] = $user_content['field_5f16f0cc3bde3'];
        }

        foreach ($users_old as $user_content) {

            $list_users_old[] = $user_content['user'];
        }

        //Array Differences
        $array_diff = array_diff($list_users_new, $list_users_old);

        if (!empty($array_diff)) {

            foreach ($array_diff as $key => $user_id) {

                $notification = get_user_meta($user_id, 'notification_docs_inbox_new_notification', true);

                //Let's send a notification depending on the user's settings
                if (!isset($notification) || $notification != 'no') {

                    //Send Email
                    $args = array(
                        'tokens' => array(
                            'site.name'   => get_bloginfo('name'),
                            'profile.url' => bp_core_get_user_domain($user_id) . JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG,
                        ),
                    );

                    bp_send_email('document_new', (int) $user_id, $args);
                }

                //Send BuddyPress Notification
                bp_notifications_add_notification(
                    array(
                        'user_id'           => $user_id,
                        'item_id'           => $post_id,
                        'secondary_item_id' => get_current_user_id(),
                        'component_name'    => 'docs_inbox',
                        'component_action'  => 'document_new',
                        'date_notified'     => bp_core_current_time(),
                        'is_new'            => 1,
                    )
                );
            }
        }
    }
}
