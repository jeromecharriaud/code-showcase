<?php

add_action('acf/save_post', 'nw_docs_inbox_notification_file_change', 1);
function nw_docs_inbox_notification_file_change($post_id) {

    if (get_post_type($post_id) !== 'shared_doc') {
        return;
    }

    $field_new = $_POST['acf']['field_5f16efcd5ec7e'];
    $field_old = get_field('documents', $post_id);

    $users = $_POST['acf']['field_5ed9e83a593d6'];

    if (!empty($field_old) && !empty($field_new)) {

        foreach ($field_new as $documents_new) {

            $list_documents_new[] = $documents_new['field_5ed9e854593d7'];
        }

        foreach ($field_old as $documents_old) {

            $list_documents_old[] = $documents_old['file']['ID'];
        }

        //Array Differences
        $array_diff = array_diff($list_documents_new, $list_documents_old);

        //If there is any difference, it means one or several files have been changed
        if (!empty($array_diff)) {

            if (!empty($users)) {

                foreach ($users as $user_content) {

                    $user_id      = $user_content['field_5f16f0cc3bde3'];
                    $notification = get_user_meta($user_id, 'notification_docs_inbox_modified_notification', true);

                    //Let's send a user notification depending on the user's settings
                    if (!isset($notification) || $notification != 'no') {

                        //Send Email
                        $args = array(
                            'tokens' => array(
                                'site.name'   => get_bloginfo('name'),
                                'profile.url' => bp_core_get_user_domain($user_id) . JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG,
                            ),
                        );
                        bp_send_email('document_modified', (int) $user_id, $args);
                    }

                    //Send BuddyPress Notification
                    bp_notifications_add_notification(
                        array(
                            'user_id'           => $user_id,
                            'item_id'           => $post_id,
                            'secondary_item_id' => '',
                            'component_name'    => 'docs_inbox',
                            'component_action'  => 'document_modified',
                            'date_notified'     => bp_core_current_time(),
                            'is_new'            => 1,
                        )
                    );
                }
            }
        }
    }
}
