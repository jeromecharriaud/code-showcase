<?php

add_action('new_to_publish', 'nw_docs_inbox_notification_new_document');
add_action('auto-draft_to_publish', 'nw_docs_inbox_notification_new_document');
add_action('draft_to_publish', 'nw_docs_inbox_notification_new_document');
add_action('pending_to_publish', 'nw_docs_inbox_notification_new_document');
function nw_docs_inbox_notification_new_document($post) {

    $post_id = $post->ID;

    if (get_post_type($post_id) !== 'shared_doc') {
        return;
    }

    if (!isset($_POST['acf']['field_5ed9e83a593d6'])) {
        return;
    }

    $users     = $_POST['acf']['field_5ed9e83a593d6'];
    $documents = $_POST['acf']['field_5f16efcd5ec7e'];

    if (empty($users)) {
        return;
    }

    if (empty($documents)) {
        return;
    }

    foreach ($users as $user_content) {
        $user_id = $user_content['field_5f16f0cc3bde3'];

        $notification = get_user_meta($user_id, 'notification_docs_inbox_new_notification', true);

        //Let's send a notification depending on the user's settings
        if (!isset($notification) || $notification != 'no') {

            //Send Email
            $args = array(
                'tokens' => array(
                    'site.name'   => get_bloginfo('name'),
                    'profile.url' => bp_core_get_user_domain($user_id) . JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG,
                ),
            );

            bp_send_email('document_new', (int) $user_id, $args);
        }

        //Send BuddyPress Notification
        bp_notifications_add_notification(
            array(
                'user_id'           => $user_id,
                'item_id'           => $post_id,
                'secondary_item_id' => get_current_user_id(),
                'component_name'    => 'docs_inbox',
                'component_action'  => 'document_new',
                'date_notified'     => bp_core_current_time(),
                'is_new'            => 1,
            )
        );
    }
}

add_filter('pmxi_acf_custom_field', 'nw_docs_inbox_new_document_notification_after_import', 10, 3);
function nw_docs_inbox_new_document_notification_after_import($value, $pid, $name) {

    if (substr($name, 0, 5) === 'users' && substr($name, -4, 4) === 'user') {

        $args = array(
            'tokens' => array(
                'site.name'   => get_bloginfo('name'),
                'profile.url' => bp_core_get_user_domain($value) . JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG,
            ),
        );

        bp_send_email('document_new', (int) $value, $args);

        //Send BuddyPress Notification
        bp_notifications_add_notification(
            array(
                'user_id'           => $value,
                'item_id'           => $pid,
                'secondary_item_id' => '',
                'component_name'    => 'docs_inbox',
                'component_action'  => 'document_new',
                'date_notified'     => bp_core_current_time(),
                'is_new'            => 1,
            )
        );
    }

    return $value;
}
