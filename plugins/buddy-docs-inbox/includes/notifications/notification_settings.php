<?php

add_action('bp_notification_settings', 'nw_docs_inbox_screen_notification_settings', 1);
function nw_docs_inbox_screen_notification_settings() {

    //New document
    $notification_doc_new = 'no';

    if (!$notification_doc_new = bp_get_user_meta(bp_displayed_user_id(), 'notification_docs_inbox_new_notification', true)) {
        $notification_doc_new = 'yes';
    }

    //Modified document
    $notification_doc_modified = 'no';

    if (!$notification_doc_modified = bp_get_user_meta(bp_displayed_user_id(), 'notification_docs_inbox_modified_notification', true)) {
        $notification_doc_modified = 'yes';
    }

?>

    <table class="notification-settings" id="docs-inbox-notification-settings">
        <thead>
            <tr>
                <th class="icon">&nbsp;</th>
                <th class="title"><?php _e('Documents Inbox', 'jcorp-docs-inbox'); ?></th>
                <th class="yes"><?php _e('Yes', 'jcorp-docs-inbox'); ?></th>
                <th class="no"><?php _e('No', 'jcorp-docs-inbox'); ?></th>
            </tr>
        </thead>

        <tbody>
            <?php $current_user = wp_get_current_user(); ?>
            <tr id="docs-inbox-notification-settings-notifications">
                <td>&nbsp;</td>
                <td><?php _e('You have received a new document', 'jcorp-docs-inbox'); ?>
                </td>
                <td class="yes">
                    <div class="bp-radio-wrap">
                        <input type="radio" name="notifications[notification_docs_inbox_new_notification]" id="notification-docs-inbox-new-notification-yes" class="bs-styled-radio" value="yes" <?php checked($notification_doc_new, 'yes', true); ?> />
                        <label for="notification-docs-inbox-new-notification-yes"><span class="bp-screen-reader-text"><?php _e('Yes, send email', 'jcorp-docs-inbox'); ?></span></label>
                    </div>
                </td>
                <td class="no">
                    <div class="bp-radio-wrap">
                        <input type="radio" name="notifications[notification_docs_inbox_new_notification]" id="notification-docs-inbox-new-notification-no" class="bs-styled-radio" value="no" <?php checked($notification_doc_new, 'no', true); ?> />
                        <label for="notification-docs-inbox-new-notification-no"><span class="bp-screen-reader-text"><?php _e('No, do not send email', 'jcorp-docs-inbox'); ?></span></label>
                    </div>
                </td>
            </tr>
            <tr id="docs-inbox-notification-settings-notifications">
                <td>&nbsp;</td>
                <td><?php _e('One or several documents have been changed', 'jcorp-docs-inbox'); ?>
                </td>
                <td class="yes">
                    <div class="bp-radio-wrap">
                        <input type="radio" name="notifications[notification_docs_inbox_modified_notification]" id="notification-docs-inbox-modified-notification-yes" class="bs-styled-radio" value="yes" <?php checked($notification_doc_modified, 'yes', true); ?> />
                        <label for="notification-docs-inbox-modified-notification-yes"><span class="bp-screen-reader-text"><?php _e('Yes, send email', 'jcorp-docs-inbox'); ?></span></label>
                    </div>
                </td>
                <td class="no">
                    <div class="bp-radio-wrap">
                        <input type="radio" name="notifications[notification_docs_inbox_modified_notification]" id="notification-docs-inbox-modified-notification-no" class="bs-styled-radio" value="no" <?php checked($notification_doc_modified, 'no', true); ?> />
                        <label for="notification-docs-inbox-modified-notification-no"><span class="bp-screen-reader-text"><?php _e('No, do not send email', 'jcorp-docs-inbox'); ?></span></label>
                    </div>
                </td>
            </tr>

            <?php

            /**
             * Fires inside the closing </tbody> tag for activity screen notification settings.
             *
             * @since BuddyPress 1.2.0
             */
            do_action('bp_docs_inbox_screen_notification_settings')
            ?>
        </tbody>
    </table>

<?php
}
