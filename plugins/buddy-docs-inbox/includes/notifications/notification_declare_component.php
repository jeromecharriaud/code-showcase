<?php

add_filter('bp_notifications_get_registered_components', 'nw_docs_inbox_component');
function nw_docs_inbox_component($component_names = array()) {
    if (!is_array($component_names)) {
        $component_names = array();
    }
    array_push($component_names, 'docs_inbox');
    return $component_names;
}

// this gets the saved item id, compiles some data and then displays the notification
add_filter('bp_notifications_get_notifications_for_user', 'nw_docs_inbox_notification', 10, 15);
function nw_docs_inbox_notification($action, $item_id, $secondary_item_id, $total_items, $format = 'string') {

    // New document
    if ($action === 'document_new') {
        $custom_title = $custom_text = __('You have received a new document', 'jcorp-docs-inbox');

        $custom_link = bp_core_get_user_domain(get_current_user_id()) . JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG . '/';

        //WordPress Toolbar
        if ('string' === $format) {
            $return = apply_filters('custom_filter', '<a href="' . esc_url($custom_link) . '" title="' . esc_attr($custom_title) . '">' . esc_html($custom_text) . '</a>', $custom_text, $custom_link);

            echo $return;
        }
    }

    // New document
    elseif ($action === 'document_modified') {
        $custom_title = $custom_text = __('One or several documents have been changed or added to your space', 'jcorp-docs-inbox');

        $custom_link = bp_core_get_user_domain(get_current_user_id()) . JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG . '/';

        //WordPress Toolbar
        if ('string' === $format) {
            $return = apply_filters('custom_filter', '<a href="' . esc_url($custom_link) . '" title="' . esc_attr($custom_title) . '">' . esc_html($custom_text) . '</a>', $custom_text, $custom_link);

            echo $return;
        }
    }
}
