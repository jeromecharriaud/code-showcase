<?php

add_filter( 'acf/prepare_field/name=inbox_documents', 'nw_docs_inbox_disable_fields' );

function nw_docs_inbox_disable_fields( $field ) {

    $key = array_search( 'downloaded', array_column( $field['sub_fields'], 'name' ) );

    unset( $field['sub_fields'][ $key ] );

    return $field;

}
