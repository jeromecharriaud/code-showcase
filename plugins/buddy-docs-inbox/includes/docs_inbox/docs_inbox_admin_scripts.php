<?php

add_action('admin_enqueue_scripts', 'nw_docs_inbox_add_admin_scripts', 10, 1);
function nw_docs_inbox_add_admin_scripts($hook) {

    global $post;

    if ($hook == 'post-new.php' || $hook == 'post.php') {
        if ('shared_doc' === $post->post_type) {
            wp_register_script('nw-docs-inbox-admin', JCORP_DOCS_INBOX_BB_ADDON_PLUGIN_URL . 'assets/js/nw-docs-inbox-admin.js', array('jquery'));
            wp_enqueue_script('nw-docs-inbox-admin');
        }
    }
}
