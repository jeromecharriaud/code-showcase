<?php

// Register Custom Post Shared Docs
add_action('init', 'nw_docs_inbox_declare_post_type_shared_doc', 0);
function nw_docs_inbox_declare_post_type_shared_doc() {

    $labels = array(
        'name'                     => __('Documents', 'jcorp-docs-inbox'),
        'singular_name'            => __('Document', 'jcorp-docs-inbox'),
        'add_new_item'             => __('Add New Document'),
        'edit_item'                => __('Edit Document'),
        'new_item'                 => __('New Document'),
        'view_item'                => __('View Document'),
        'view_items'               => __('View Documents'),
        'search_items'             => __('Search Documents'),
        'not_found'                => __('No documents found.'),
        'not_found_in_trash'       => __('No documents found in Trash.'),
        'all_items'                => __('All Documents'),
        'archives'                 => __('Document Archives'),
        'attributes'               => __('Document Attributes'),
        'insert_into_item'         => __('Insert into document'),
        'uploaded_to_this_item'    => __('Uploaded to this document'),
        'filter_items_list'        => __('Filter documents list'),
        'items_list_navigation'    => __('Documents list navigation'),
        'items_list'               => __('Documents list'),
        'item_published'           => __('Document published.'),
        'item_published_privately' => __('Document published privately.'),
        'item_reverted_to_draft'   => __('Document reverted to draft.'),
        'item_scheduled'           => __('Document scheduled.'),
        'item_updated'             => __('Document updated.'),
    );
    $rewrite = array(
        'slug'       => '',
        'with_front' => false,
        'pages'      => false,
        'feeds'      => false,
    );
    $args = array(
        'label'               => __('Documents', 'jcorp-docs-inbox'),
        'description'         => __('Description', 'jcorp-docs-inbox'),
        'labels'              => $labels,
        'supports'            => array('title'),
        'taxonomies'          => array(),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'menu_icon'           => 'dashicons-media-document',
        'menu_position'       => 8,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite'             => $rewrite,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'publicly_queryable'  => false,
    );

    register_post_type('shared_doc', $args);
}
