<?php

add_action( 'init', 'nw_docs_inbox_declare_tags', 10 );
function nw_docs_inbox_declare_tags() {
    
    $args = array(
        'hierarchical'       => false,
        'public'             => true,
        'show_admin_column'  => false,
        'show_in_nav_menus'  => true,
        'show_tagcloud'      => true,
        'show_ui'            => true,
        'show_in_quick_edit' => false,
        'meta_box_cb'        => false,
    );
    register_taxonomy( 'tag-docs', array( 'shared_doc' ), $args );

}
