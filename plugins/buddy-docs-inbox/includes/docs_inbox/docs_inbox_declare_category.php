<?php

add_action( 'init', 'nw_docs_inbox_declare_category', 10 );
function nw_docs_inbox_declare_category() {
    
    $args = array(
        'hierarchical'       => true,
        'public'             => true,
        'show_admin_column'  => false,
        'show_in_nav_menus'  => true,
        'show_tagcloud'      => true,
        'show_ui'            => true,
        'show_in_quick_edit' => false,
        'meta_box_cb'        => false,
    );
    register_taxonomy( 'category-docs', array( 'shared_doc' ), $args );

}
