<?php

add_action('bp_setup_nav', 'nw_docs_inbox_setup_profile_nav');
function nw_docs_inbox_setup_profile_nav() {

    global $bp;
    if (false == bp_is_user()) {
        return;
    }

    //Private : the displayed user must be the current user
    if (!current_user_can('administrator') && bp_displayed_user_id() != get_current_user_id()) {
        return;
    }

    bp_core_new_nav_item(array(
        'name'                => __('Received documents', 'jcorp-docs-inbox'),
        'slug'                => JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG,
        'screen_function'     => 'nw_docs_inbox_bp_profile',
        'position'            => 40,
        'parent_url'          => bp_loggedin_user_domain() . '/' . JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG . '/',
        'parent_slug'         => $bp->profile->slug,
        'default_subnav_slug' => 'view',
    ));

    bp_core_new_subnav_item(array(
        'name'            => __('Inbox', 'jcorp-docs-inbox'),
        'slug'            => 'view',
        'parent_slug'     => JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG,
        'parent_url'      => bp_loggedin_user_domain() . '/' . JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG . '/',
        'item_css_id'     => '',

        'position'        => 0,
        'screen_function' => 'nw_docs_inbox_bp_profile',
    ));
}

//Inbox
function nw_docs_inbox_bp_profile() {

    add_action('bp_template_content', 'nw_docs_inbox_show_profile_inbox');
    bp_core_load_template('buddypress/members/single/plugins');
}

function nw_docs_inbox_show_profile_inbox() {
?>

    <div class="bp-profile-wrapper">
        <div class="bp-profile-content">
            <div class="profile public">
                <header class="entry-header profile-loop-header profile-header flex align-items-center">
                    <h1 class="entry-title bb-profile-title"><?php _e('Inbox', 'jcorp-docs-inbox'); ?></h1>
                </header>
                <div class="bp-widget details">
                    <?php nw_docs_inbox_show_inbox(bp_displayed_user_id()); ?>
                </div>
            </div>
        </div>
    </div>
<?php
}

add_action('buddyboss_theme_after_bb_profile_menu', 'nw_docs_inbox_add_link');
function nw_docs_inbox_add_link() {
    $profile_link = trailingslashit(bp_loggedin_user_domain());

?>

    <li id="wp-admin-bar-my-account-documents_inbox" class="menupop parent">
        <a class="ab-item" aria-haspopup="true" href="<?php echo $profile_link . JCORP_DOCS_INBOX_BB_ADDON_BP_NAV_SLUG; ?>">
            <span class="wp-admin-bar-arrow" aria-hidden="true"></span><?php _e('Documents', 'jcorp-docs-inbox'); ?>
        </a>
    </li>
<?php
}

/*
//Send Documents
function nw_docs_inbox_send_bp_profile() {

    //ACF Form
    acf_form_head();

    add_action( 'bp_template_content', 'nw_docs_inbox_show_send_documents_page' );
    bp_core_load_template( 'buddypress/members/single/plugins' );
}

function nw_docs_inbox_show_send_documents_page() {
    ?>

<div class="bp-profile-wrapper">
    <div class="bp-profile-content">
        <div class="profile public">
            <header class="entry-header profile-loop-header profile-header flex align-items-center">
                <h1 class="entry-title bb-profile-title"><?php _e( 'Send Document', 'jcorp-docs-inbox' );?></h1>
            </header>
            <div class="bp-widget details">
                <?php nw_docs_inbox_show_form_send_documents(); ?>
            </div>
        </div>
    </div>
</div>
<?php
}

*/ ?>