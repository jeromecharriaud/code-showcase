<?php

function nw_docs_inbox_get_inbox_documents( $user_id ) {

    if ( empty( $user_id ) ) {
        return;
    }

    $args = array(
        'post_type'   => array( 'shared_doc' ),
        'post_status' => 'publish',
        'numberposts' => -1,
        'meta_query'  => array(
            array(
                'key'     => 'users_%_user',
                'value'   => $user_id,
                'compare' => '=>',
            ),
        ),
    );

    $query = new WP_Query( $args );

    if ( $query->have_posts() ) {

        $documents = array();
        $i         = 0;

        while ( $query->have_posts() ): $query->the_post();

            $post_id = get_the_ID();

            //Find User Row
            $user_row = array_search( $user_id, array_column( get_field( 'users' ), 'user' ) );

            $list_documents = get_field( 'documents', $post_id );

            if ( ! empty( $list_documents ) ) {

                foreach ( $list_documents as $single_document ) {

                    $documents[ $i ] = array(
                        'post_id'     => $post_id,
                        'user_row'    => $user_row,
                        'file'        => $single_document['file'],
                        'title'       => $single_document['title'],
                        'description' => $single_document['description'],
                        'categories'  => $single_document['categories'],
                        'tags'        => $single_document['tags'],
                    );

                    $i++;

                }

            }

        endwhile;

        return $documents;

    }

}
