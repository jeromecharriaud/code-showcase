<?php

//Load Field - Read Only
add_filter( 'acf/load_field/name=download_counter', 'nw_docs_inbox_disable_acf_load_field' );
add_filter( 'acf/load_field/name=downloaded', 'nw_docs_inbox_disable_acf_load_field' );
function nw_docs_inbox_disable_acf_load_field( $field ) {

    $field['disabled'] = true;

    return $field;

}

//Load Value - Counter
add_filter( 'acf/load_value/name=download_counter', 'nw_docs_inbox_docs_load_counter', 10, 3 );
function nw_docs_inbox_docs_load_counter( $value, $post_id, $field ) {

    $row = explode( '_', $field['name'] );
    $row = $row[1];

    $key        = 'users_' . $row . '_downloaded';
    $downloaded = get_post_meta( $post_id, $key, true );

    if ( is_array( $downloaded ) ) {
        $value = count( $downloaded );
    }

    return $value;

}

//Load Value - Dates
add_filter( 'acf/load_value/name=downloaded', 'nw_docs_inbox_docs_load_dates', 10, 3 );
function nw_docs_inbox_docs_load_dates( $value, $post_id, $field ) {

    if ( ! empty( $value ) && is_array( $value ) ) {
        foreach ( $value as $key => $date ) {
            $value[ $key ] = date( 'd/m/Y H:i', $date );
        }
        $value = implode( $value, ' | ' );

    }

    return $value;

}
