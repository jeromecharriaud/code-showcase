<?php

add_action('wp_ajax_nw_docs_inbox_download_file', 'nw_docs_inbox_download_file');
function nw_docs_inbox_download_file() {

    //Let's check the nonce
    if (!wp_verify_nonce($_REQUEST['nonce'], 'nw_docs_inbox_download_file')) {
        exit(__('Wrong nonce', 'jcorp-docs-inbox'));
    }

    $post_id     = $_REQUEST['post_id'];
    $document_id = $_REQUEST['document_id'];
    $row         = $_REQUEST['row'];
    $user_id     = get_current_user_id();

    $users = get_field('users', $post_id);
    $auth  = false;

    if (!empty($users)) {
        foreach ($users as $user) {
            if ($user['user'] == $user_id) {
                $auth = true;
            }
        }
    }

    //The user is authorized to download the file
    if ($auth === true) {

        //Increment dates
        $key_dates = 'users_' . $row . '_downloaded';

        $dates = get_post_meta($post_id, $key_dates, true);
        if (empty($dates)) {
            $dates = array();
        }
        $dates[] = time();

        //Update Sub Field
        update_post_meta($post_id, $key_dates, $dates);

        $result['type'] = 'success';
        $result['url']  = get_the_guid($document_id);
        $result['file'] = $_REQUEST['file'];
    } else {
        $result['type'] = 'error';
    }

    // Check if action was fired via Ajax call. If yes, JS code will be triggered, else the user is redirected to the post page
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    die();
}

// define the function to be fired for logged out users
add_action('wp_ajax_nopriv_nw_docs_inbox_download_file', 'nw_docs_inbox_download_file_nopriv');
function nw_docs_inbox_download_file_nopriv() {
    echo __('You must log in', 'jcorp-docs-inbox');
    die();
}
