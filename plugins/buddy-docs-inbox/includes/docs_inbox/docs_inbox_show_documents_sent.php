<?php

function nw_docs_inbox_show_inbox($user_id) {

    $documents = nw_docs_inbox_get_inbox_documents($user_id);
    if (empty($documents)) {
        echo __('You currently have no documents', 'jcorp-docs-inbox');
        return;
    }

    //Docs Inbox Scripts
    wp_register_style('nw-docs-inbox', JCORP_DOCS_INBOX_BB_ADDON_PLUGIN_URL . 'assets/css/nw-docs-inbox.css', '', '');
    wp_enqueue_style('nw-docs-inbox');
    wp_register_script('nw-docs-inbox', JCORP_DOCS_INBOX_BB_ADDON_PLUGIN_URL . 'assets/js/nw-docs-inbox.js', array('jquery'));
    wp_localize_script('nw-docs-inbox', 'my_ajax', array('ajaxurl' => admin_url('admin-ajax.php')));
    wp_enqueue_script('nw-docs-inbox');

    //Datatables Style & Scripts
    nw_datatables_call_files();

    $nonce = wp_create_nonce('nw_docs_inbox_download_file');

    $html_output = '';

    if (!empty($documents)) {

        foreach ($documents as $document_key => $document) {

            $details     = '';
            $file        = $document['file'];
            $post_id     = $document['post_id'];
            $document_id = $file['ID'];

            //Description
            if (!empty($document['description'])) {
                $details .= '<b>' . __('Description', 'jcorp-docs-inbox') . '</b>';
                $details .= '<div>' . $document['description'] . '</div>';
            }

            //Counter
            $key        = 'users_' . $document['user_row'] . '_downloaded';
            $downloaded = get_post_meta($post_id, $key, true);

            if (!empty($downloaded) && is_array($downloaded)) {

                $counter       = count($downloaded);
                $last_download = date('d/m/Y', max($downloaded));

                //Change Date format
                $downloaded = array_map(function ($date) {
                    return date('d/m/Y - H:i', $date);
                }, $downloaded);

                $details .= '<div><b>' . _n('Last download', 'Last downloads', count($downloaded), 'jcorp-docs-inbox') . '</b></div>';
                $details .= implode('<br/>', $downloaded);
            } else {
                $counter       = 0;
                $last_download = '';
            }

            $filename    = pathinfo($file['filename'], PATHINFO_EXTENSION);
            $hidden_date = get_the_date('Y/m/d H:i', $document['post_id']);
            $date        = get_the_date('', $document['post_id']);

            $icon = 'bb-icon-file-' . $filename;

            //Categories
            $categories = '';
            if (!empty($document['categories'])) {
                foreach ($document['categories'] as $category) {
                    $categories .= '<span class="badge">' . $category->name . '</span>';
                }
            }

            //Tags
            $tags = '';
            if (!empty($document['tags'])) {
                foreach ($document['tags'] as $tag) {
                    $tags .= '<span class="badge">' . $tag->name . '</span>';
                }
            }

            //Details Icon
            $icon_details = 'details-control';
            if (empty($details)) {
                $icon_details = '';
            }

            if (!empty($document_id)) {

                $download_link = admin_url('admin-ajax.php?action=nw_docs_inbox_download_file');

                $html_output .= '
                <tr data-child-value="' . $details . '">

                    <td alt="' . __('More Details', 'jcorp-docs-inbox') . '" class="' . $icon_details . '"></td>
                    <td class="data">' . $hidden_date . '</td>
                    <td><i class="' . $icon . '"></i> ' . nw_mb_ucfirst($document['title']) . '</td>
                    <td class="data">' . $date . '</td>
                    <td class="counter">' . $counter . '</td>
                    <td class="categories">' . $categories . '</td>
                    <td class="tags">' . $tags . '</td>
                    <td><a alt="' . __('Download', 'jcorp-docs-inbox') . '" data-nonce="' . $nonce . '" data-row="' . $document['user_row'] . '" data-file="' . $document['file']['filename'] . '"  data-document_id="' . $document_id . '" data-post_id="' . $post_id . '" class="download-file" href="' . $download_link . '" download><i   class="bb-icon-download"></i></a></td>
                </tr>
                ';
            }
        }
    }

    echo '
    <table class="datatables display responsive cell-border compact stripe row-border dt[-head|-body]-center" style="width:100%">

        <thead>
            <tr>
                <td></td>
                <td>' . __('Hidden date', 'jcorp-docs-inbox') . '</td>
                <td>' . __('Name', 'jcorp-docs-inbox') . '</td>
                <td>' . __('Shared date', 'jcorp-docs-inbox') . '</td>
                <td>' . __('Download counter', 'jcorp-docs-inbox') . '</td>
                <td>' . __('Categories', 'jcorp-docs-inbox') . '</td>
                <td>' . __('Tags', 'jcorp-docs-inbox') . '</td>
                <td>' . __('Download', 'jcorp-docs-inbox') . '</td>
            </tr>
        </thead>

        <tbody>' . $html_output . '</tbody>

    </table>
    ';
}
