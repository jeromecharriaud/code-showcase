<?php

// ACF upload prefilter
add_filter('acf/upload_prefilter/key=field_5ed9e854593d7', 'nw_docs_inbox_upload_dir_prefilter', 10, 3);
function nw_docs_inbox_upload_dir_prefilter($errors, $file, $field) {

    // Only allow editors and admins, change capability as you see fit
    if (!current_user_can('edit_posts')) {
        $errors[] = __('Only Editors and Administrators may upload attachments', 'jcorp-docs-inbox');
    }

    // This filter changes directory just for item being uploaded
    add_filter('upload_dir', 'nw_docs_inbox_upload_dir');
}

// Custom upload directory
function nw_docs_inbox_upload_dir($param) {

    // Set to whatever directory you want the ACF file field to upload to
    $custom_dir    = '/uploads/shared-docs';
    $param['path'] = WP_CONTENT_DIR . $custom_dir;
    $param['url']  = WP_CONTENT_URL . $custom_dir;

    return $param;
}

//Hide the folder from the wordpress library
add_filter('ajax_query_attachments_args', 'filter_query_attachments_args');

function filter_query_attachments_args($query) {

    $shared_docs_ids = get_posts(array(
        'fields'         => 'ids', // Only get post IDs
        'posts_per_page' => 1000,
        'post_type'      => 'shared_doc',
    ));
    $query['post_parent__not_in'] = $shared_docs_ids;
    return $query;
}
