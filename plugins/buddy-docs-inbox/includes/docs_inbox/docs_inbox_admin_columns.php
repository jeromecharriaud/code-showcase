<?php

add_filter('manage_shared_doc_posts_columns', 'nw_docs_inbox_shared_doc_add_admin_columns');
function nw_docs_inbox_shared_doc_add_admin_columns($columns) {
    $new = array();
    foreach ($columns as $key => $title) {

        if ($key == 'date') // Put the Thumbnail column before the Author column
        {
            $new['documents'] = __('Documents', 'jcorp-docs-inbox');
            $new['users']     = __('Shared with', 'jcorp-docs-inbox');
        }

        $new[$key] = $title;
    }
    return $new;
}

add_action('manage_shared_doc_posts_custom_column', 'nw_docs_inbox_shared_doc_add_admin_columns_info', 10, 2);
function nw_docs_inbox_shared_doc_add_admin_columns_info($column, $post_id) {

    $limit = 10;

    switch ($column) {

        case 'users':
            $array_users = '';
            $users       = get_field('users', $post_id);
            if (!empty($users)) {
                $array_users = array();
                foreach ($users as $user_content) {
                    $userdata = get_userdata($user_content['user']);
                    if ($userdata !== false) {

                        $array_users[] = $userdata->display_name;
                    }
                }

                if (count($array_users) > $limit) {
                    for ($i = 0; $i < $limit; $i++) {
                        echo $array_users[$i];
                        if ($i < $limit - 1) {
                            echo ', ';
                        }
                    }
                    echo '<a href="" class="show_more"> ' . __('show more', 'jcorp-docs-inbox') . '</a>';
                    echo '<span class="hidden hidden_users">';
                    echo ', ';
                    for ($i = $limit; $i < count($array_users); $i++) {
                        echo $array_users[$i];
                        if ($i < count($array_users) - 1) {
                            echo ', ';
                        }
                    }
                    echo '</span>';
                } else {
                    echo implode($array_users, ', ');
                }
            }

            break;

        case 'documents':
            $array_documents = '';
            $documents       = get_field('documents', $post_id);
            if (!empty($documents)) {
                $array_documents = array();
                foreach ($documents as $document_content) {
                    $array_documents[] = $document_content['title'];
                }

                $array_documents = implode($array_documents, ', ');
            }
            echo $array_documents;
            break;
    }
}
