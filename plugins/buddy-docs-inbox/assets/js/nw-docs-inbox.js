jQuery(document).ready(function ($) {
	jQuery('.download-file').click(function (e) {
		e.preventDefault()

		var tr = this.closest('tr')

		jQuery.ajax({
			type: 'post',
			dataType: 'json',
			url: my_ajax.ajaxurl,
			data: {
				action: 'nw_docs_inbox_download_file',
				nonce: jQuery(this).attr('data-nonce'),
				post_id: jQuery(this).attr('data-post_id'),
				file: jQuery(this).attr('data-file'),
				document_id: jQuery(this).attr('data-document_id'),
				row: jQuery(this).attr('data-row'),
			},
			success: function (response) {
				if (response.type == 'success') {
					//Increment the counter
					var counter = $(tr).find('.counter')
					var counter_val = counter.html()
					var counter_new = ++counter_val
					counter.html(counter_new)

					//Downloading file
					const link = document.createElement('a')
					link.href = response.url
					link.setAttribute('download', response.file)
					document.body.appendChild(link)
					link.click()
					document.body.removeChild(link)
				} else {
				}
			},
		})
	})
})
