jQuery(document).ready(function ($) {
	$('.acf-field-5ed9e83a593d6').on(
		'click',
		'a[data-event="add-row"]',
		function () {
			if ($('[data-name="download_counter"] input').length) {
				setTimeout(function () {
					$('[data-name="download_counter"] input').prop(
						'disabled',
						true
					)
					$('[data-name="downloaded"] input').prop('disabled', true)
				}, 1)
			}
		}
	)
})
