<?php 

class JCORP_JOKE_Regenerate_Token {

    private $expiration = 48*60*60;

    public function __construct() {

        //Update the transient joke token when the token is updated from the admin
        add_action( "update_option_nw_joke_token", array($this, 'set_meta_token_transient'), 10, 3 );

        //Schedule a new token every 24 hours
        add_action('nw_cron_regenerate_token', array($this, 'nw_regenerate_token'));
        
        if (! wp_next_scheduled ( 'nw_cron_regenerate_token' )) {
            
            wp_schedule_event(time(), 'daily', 'nw_cron_regenerate_token');
            
        }

    }

    //Generate Token
    public function nw_regenerate_token () {

        $token = get_option( 'nw_joke_token');
        $api_url = get_option('nw_joke_api_url');

        if(empty($token) || empty($api_url)) return null;

        $url = $api_url.'token';

        $args = array( 
            'headers' => array( 
                'Content-Type' => 'application/json', 
                "Authorization" => 'Bearer ' . $token
            ) 
        ); 

        $response = wp_remote_post($url, $args);

        if ( ! is_wp_error( $response ) && isset( $response['body'] ) ) {

            $result = json_decode( $response['body'], true );

            if ( $result !== NULL && $result['token'] !== NULL ) {

                set_transient( 'nw_joke_token', $result['token'], $this->expiration );

            }

        }

    }

    //Set transient for token
    public function set_meta_token_transient($old_value, $value, $option) {

        set_transient( 'nw_joke_token', $value, $this->expiration );

    }

}

new JCORP_JOKE_Regenerate_Token();
