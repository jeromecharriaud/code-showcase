<?php
// Exit if accessed directly
defined('ABSPATH') || exit;

if (!function_exists('JCORP_JOKE_admin_enqueue_script')) {
    function JCORP_JOKE_admin_enqueue_script() {
        wp_enqueue_style('buddyboss-addon-admin-css', plugin_dir_url(__FILE__) . 'style.css');
    }

    add_action('admin_enqueue_scripts', 'JCORP_JOKE_admin_enqueue_script');
}

if (!function_exists('JCORP_JOKE_get_settings_sections')) {
    function JCORP_JOKE_get_settings_sections() {

        $settings = array(
            'JCORP_JOKE_settings_section' => array(
                'page'  => 'addon',
                'title' => __('Joke of the day Settings', 'jcorp'),
            ),
        );

        return (array) apply_filters('JCORP_JOKE_get_settings_sections', $settings);
    }
}

if (!function_exists('JCORP_JOKE_get_settings_fields_for_section')) {
    function JCORP_JOKE_get_settings_fields_for_section($section_id = '') {

        // Bail if section is empty
        if (empty($section_id)) {
            return false;
        }

        $fields = JCORP_JOKE_get_settings_fields();
        $retval = isset($fields[$section_id]) ? $fields[$section_id] : false;

        return (array) apply_filters('JCORP_JOKE_get_settings_fields_for_section', $retval, $section_id);
    }
}

if (!function_exists('JCORP_JOKE_get_settings_fields')) {
    function JCORP_JOKE_get_settings_fields() {

        $fields = array();

        $fields['JCORP_JOKE_settings_section'] = array(


            'nw_joke_api_url' => array(
                'title'             => __('API URL', 'jcorp'),
                'callback'          => 'JCORP_JOKE_settings_callback_api_url',
                'sanitize_callback' => '',
                'args'              => array(),
            ),


            'nw_joke_token' => array(
                'title'             => __('Token', 'jcorp'),
                'callback'          => 'JCORP_JOKE_settings_callback_token',
                'sanitize_callback' => '',
                'args'              => array(),
            ),

        );

        return (array) apply_filters('JCORP_JOKE_get_settings_fields', $fields);
    }
}

//API URL
if (!function_exists('JCORP_JOKE_settings_callback_api_url')) {

    function JCORP_JOKE_settings_callback_api_url() { ?>

        <input name="nw_joke_api_url" id="nw_joke_api_url" type="text" value="<?php echo get_option('nw_joke_api_url'); ?>" />


    <?php

    }
}

//Token Field
if (!function_exists('JCORP_JOKE_settings_callback_token')) {

    function JCORP_JOKE_settings_callback_token() { ?>

        <input name="nw_joke_token" id="nw_joke_token" type="text" value="<?php echo get_option('nw_joke_token'); ?>" />

        <p><a href="https://api-light.com/token" target="_blank">Obtenir un token</a></p>


<?php

    }
}

/**************************************** MY PLUGIN INTEGRATION ************************************/

/**
 * Set up the my plugin integration.
 */
function JCORP_JOKE_register_integration() {
    require_once dirname(__FILE__) . '/integration/buddyboss-integration.php';
    buddypress()->integrations['addon'] = new JCORP_JOKE_BuddyBoss_Integration();
}
add_action('bp_setup_integrations', 'JCORP_JOKE_register_integration');
