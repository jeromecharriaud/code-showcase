<?php

class Joke_Widget extends WP_Widget {

    public function __construct() {

        parent::__construct(
            'joke_widget', // Base ID
            '(NW) Blague', // Name
            array('description' => __('Un jour, une blague.', 'jcorp'),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */

    public function widget($args, $instance) {

        $api_url = get_option('nw_joke_api_url');
        $token = get_transient('nw_joke_token');

        if (current_user_can('administrator') && empty($token)) {
            echo '<pre>Veuillez regénérer votre token.</pre>';
        }

        if (empty($token) || empty($api_url)) return null;

        extract($args);
        $title = apply_filters('widget_title', $instance['title']);

        echo $before_widget;

        if (!empty($title)) {
            echo $before_title . $title . $after_title;
        }

        $api_url .= 'get/random?limit=1';

        $args = array(
            'headers' => array(
                'Content-Type' => 'application/json',
                "Authorization" => 'Bearer ' . $token
            )
        );

        $response = wp_remote_get($api_url, $args);

        if (!is_wp_error($response) && isset($response['body'])) {

            $result = json_decode($response['body'], true);

            if ($result !== NULL) {

                foreach ($result as $element) {
                    echo "<em>" . $element['category'] . "</em><br />";
                    echo "<strong>" . $element['content'] . "</strong><br />";
                }
            }
        }

        echo $after_widget;
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Joke of the day', 'jcorp');
        }
?>
        <p>
            <label for="<?php echo $this->get_field_name('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>
<?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';

        return $instance;
    }
} // class Foo_Widget

// Register Foo_Widget widget
add_action('widgets_init', 'register_joke');

function register_joke() {
    register_widget('Joke_Widget');
}
