<?php

add_action('the_content', 'nw_show_star', 999, 1);
function nw_show_star($content) {

    if (empty(get_post_type())) {
        return $content;
    }

    if (empty(get_the_ID())) {
        return $content;
    }

    //Scripts & Styles
    wp_enqueue_style('nw-favorites');
    wp_enqueue_script('nw-favorites');

    $post_id = get_the_ID();

    $css       = 'add_favorite';
    $icon      = 'bb-icon-star';
    $label     = 'Ajouter à mes favoris';
    $post_type = get_post_type($post_id);

    //Let's check if the post is in the favorites
    $favorites = get_user_meta(get_current_user_id(), 'favorites', true);

    if (!empty($favorites) && !empty($favorites) && is_array($favorites)) {
        if (in_array($post_id, $favorites)) {
            $css .= ' favorited';
            $icon  = 'bb-icon-star-fill';
            $label = 'Retirer de mes favoris';
        }
    }

    $nonce = wp_create_nonce("user_add_favorite_nonce");

    $link = admin_url('admin-ajax.php?action=user_add_favorite&post_id=' . $post_id . '&nonce=' . $nonce);

    $content .= '
    <div id="add-favorite" class="favorite">
        <a title="Ajour cette page à mes favoris" class="' . $css . '" data-nonce="' . $nonce . '" data-post_id="' . $post_id . '" href="' . $link . '">
            <button><i class="' . $icon . '"></i> <span>' . $label . '</span></button>
        </a>
    </div>';

    return $content;

    ?>

<?php

}