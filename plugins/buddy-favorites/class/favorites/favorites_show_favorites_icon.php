<?php

add_action(
    'nw_do_show_header_right_icons',
    'nw_favorites_show_favorites_icon',
    1,
    5
);
function nw_favorites_show_favorites_icon() {

?>

    <div id="header-applications-dropdown-elem" class="dropdown-passive dropdown-right notification-wrap applications-wrap menu-item-has-children">

        <a href="" ref="notification_applications" class="notification-link">
            <span data-balloon-pos="down" data-balloon="Favoris">
                <i class="bb-icon-star"></i>
            </span>
        </a>

        <section class="notification-dropdown favorites-dropdown">

            <header class="notification-header">
                <h2 class="title"><?php _e('Favoris', 'jcorp'); ?></h2>
            </header>

            <ul class="notification-list favorites-list">

                <?php

                $favorites = get_user_meta(get_current_user_id(), 'favorites', true);

                if (!empty($favorites)) {

                    $favorites = array_reverse($favorites, false);

                    $i = 1;
                    array_reverse($favorites);
                    foreach ($favorites as $key => $post_id) {
                        echo '<li class="bs-item-wrap"><a href="' . get_the_permalink($post_id) . '">' . get_the_title($post_id) . '</a></li>';
                        $i++;
                        if ($i == 10) {
                            break;
                        }
                    }
                } else {
                    echo '<li class="bs-item-wrap"><a>Aucun favoris dans votre liste</a></li>';
                }

                ?>

            </ul>
            <?php

            echo '<footer class="notification-footer"><a class="delete-all" href="' . bp_core_get_user_domain(get_current_user_id()) . '/favoris">Tous mes favoris<i class="bb-icon-angle-right"></i></a></footer>';

            ?>
        </section>

    </div>

<?php

}

?>