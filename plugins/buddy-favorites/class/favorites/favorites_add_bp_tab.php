<?php

add_action('bp_setup_nav', 'nw_setup_profile_favorites_nav');
function nw_setup_profile_favorites_nav() {

    global $bp;
    if (bp_is_user() == false) {
        return;
    }

    //Private : the displayed user must be the current user
    if (bp_displayed_user_id() != get_current_user_id()) {
        return;
    }

    bp_core_new_nav_item(array(
        'name'                => __('Favoris', 'jcorp'),
        'slug'                => 'favoris',
        'screen_function'     => 'nw_favorites_bp_profile',
        'position'            => 40,
        'parent_url'          => bp_loggedin_user_domain() . '/favorites/',
        'parent_slug'         => $bp->profile->slug,
        'default_subnav_slug' => 'view',
    ));

    bp_core_new_subnav_item(array(
        'name'            => __('View', 'jcorp'),
        'slug'            => 'view',
        'parent_slug'     => 'favorites',
        'parent_url'      => bp_loggedin_user_domain() . '/favorites/',
        'item_css_id'     => '',
        // 'user_has_access' => null,
        // 'site_admin_only' => null,
        'position'        => 0,
        'screen_function' => 'nw_favorites_bp_profile',
    ));
}

function nw_favorites_bp_profile() {

    // Add title and content here - last is to call the members plugin.php template.
    add_action('bp_template_content', 'nw_favorites_show_profile_favorites');
    bp_core_load_template('buddypress/members/single/plugins');
}

function nw_favorites_profile_show_screen_title() {

    echo '<div class="screen-profile">' . __('Favoris', 'jcorp') . '</div>';
}

function nw_favorites_show_profile_favorites() {
?>

    <div class="bp-profile-wrapper">
        <div class="bp-profile-content">
            <div class="profile public">
                <header class="entry-header profile-loop-header profile-header flex align-items-center">
                    <h1 class="entry-title bb-profile-title">Favoris</h1>
                </header>
                <div class="bp-widget details">
                    <?php nw_show_favorites(bp_displayed_user_id()); ?>
                </div>
            </div>
        </div>
    </div>
<?php
}

add_action('buddyboss_theme_after_bb_profile_menu', 'nw_add_favorites_link');
function nw_add_favorites_link() {
    $profile_link = trailingslashit(bp_loggedin_user_domain());

?>

    <li id="wp-admin-bar-my-account-favorites" class="menupop parent">
        <a class="ab-item" aria-haspopup="true" href="<?php echo $profile_link; ?>favoris">
            <span class="wp-admin-bar-arrow" aria-hidden="true"></span><?php _e('Favoris', 'jcorp'); ?>
        </a>
    </li>
<?php
}
