<?php

function nw_show_favorites($user_id = '') {
    if (empty($user_id)) {
        return;
    }

    $favorites = get_user_meta($user_id, 'favorites', true);
    if (empty($favorites)) {
        echo '<span>Vous n\'avez enregistré aucuns favoris</span>';
        return;
    }

    //Scripts
    wp_enqueue_style('nw-favorites');
    wp_enqueue_script('nw-favorites');

    $nonce = wp_create_nonce("user_delete_favorite_nonce");

    //Transformation du tableau
    $favorites_new = array();
    foreach ($favorites as $key => $post_id) {
        $post_type                                = get_post_type_labels(get_post_type_object(get_post_type($post_id)))->name;
        $favorites_new[$post_type][$key]['ID']    = $post_id;
        $favorites_new[$post_type][$key]['title'] = get_the_title($post_id);
        $favorites_new[$post_type][$key]['link']  = get_the_permalink($post_id);
    }
    ksort($favorites_new);

    echo '<table id="my_favorites">';

    echo '<tbody>';

    foreach ($favorites_new as $post_type => $items) {
        if (!empty($items)) {
            echo '<tr>';
            echo '<th colspan="3">' . $post_type . '</th>';
            echo '</tr>';
            foreach ($items as $key => $post) {

                $deletion_link = admin_url('admin-ajax.php?action=user_delete_favorite&post_id=' . $post['ID'] . '&nonce=' . $nonce);

                echo '<tr>';

                echo '<td>';
                echo '<a href="' . $post['link'] . '">';
                echo $post['title'];
                echo '</a>';
                echo '</td>';

                echo '<td title="Ouvrir la page" class="open_favorite">';
                echo '<a href="' . $post['link'] . '">';
                echo ' <i class="material-icons">link</i></td>';
                echo '</a>';
                echo '</td>';

                echo '<td title="Supprimer ce favori">';
                echo '<a class="delete_favorite" data-nonce="' . $nonce . '" data-post_id="' . $post['ID'] . '" href="' . $deletion_link . '">';
                echo '<i class="material-icons">delete</i>';
                echo '</a>';
                echo '</td>';

                echo '</tr>';
            }
        }
    }

    echo '</tbody>';

    echo '</table>';

}