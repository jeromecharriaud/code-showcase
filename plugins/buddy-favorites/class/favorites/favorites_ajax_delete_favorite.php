<?php

add_action("wp_ajax_user_delete_favorite", "nw_user_delete_favorite");
add_action("wp_ajax_nopriv_user_delete_favorite", "nw_user_delete_favorite_nopriv");

function nw_user_delete_favorite() {

//Let's check the nonce
    if (!wp_verify_nonce($_REQUEST['nonce'], "user_delete_favorite_nonce")) {
        exit('Wrong nonce');
    }

    $post_id   = $_REQUEST['post_id'];
    $post_type = get_post_type($post_id);
    $user_id   = get_current_user_id();
    $favorites = get_user_meta($user_id, 'favorites', true);

    $key = array_search($post_id, $favorites);

    if ($key !== FALSE) {
        unset($favorites[$key]);
    }

    //Let's update the meta
    update_user_meta($user_id, 'favorites', $favorites);

    // If above action fails, result type is set to 'error' and like_count set to old value, if success, updated to new_like_count
    if ($key === false) {
        $result['type'] = "error";
    } else {
        $result['type'] = "success";
    }

    // Check if action was fired via Ajax call. If yes, JS code will be triggered, else the user is redirected to the post page
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }

    die();

}

// define the function to be fired for logged out users
function nw_user_delete_favorite_nopriv() {
    echo "You must log in to like";
    die();
}