<?php

class NW_Favorites {

    public function __construct() {

        $files = glob(plugin_dir_path(__FILE__) . 'favorites/*.php');

        //Include All Files
        foreach ($files as $file):

            require_once $file;

        endforeach;
    }
}