jQuery(document).ready(function ($) {
	//Add Favorite
	jQuery('.add_favorite').click(function (e) {
		e.preventDefault()
		post_id = jQuery(this).attr('data-post_id')
		nonce = jQuery(this).attr('data-nonce')

		jQuery(this).toggleClass('favorited')

		if ($(this).hasClass('favorited')) {
			$(this).find('span').html('Retirer de mes favoris')
		} else {
			$(this).find('span').html('Ajouter à mes favoris')
		}

		jQuery.ajax({
			type: 'post',
			dataType: 'json',
			url: myAjax.ajaxurl,
			data: {
				action: 'user_add_favorite',
				post_id: post_id,
				nonce: nonce,
			},
			success: function (response) {
				if (response.type == 'success') {
				} else {
				}
			},
		})
	})

	//Delete Favorite
	jQuery('.delete_favorite').click(function (e) {
		e.preventDefault()

		$(this)
			.closest('tr')
			.fadeOut(('slow', function () {}))

		post_id = jQuery(this).attr('data-post_id')
		nonce = jQuery(this).attr('data-nonce')

		jQuery.ajax({
			type: 'post',
			dataType: 'json',
			url: myAjax.ajaxurl,
			data: {
				action: 'user_delete_favorite',
				post_id: post_id,
				nonce: nonce,
			},
			success: function (response) {
				if (response.type == 'success') {
				} else {
				}
			},
		})
	})
})
