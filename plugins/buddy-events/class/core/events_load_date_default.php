<?php

add_filter('acf/load_field/name=start', 'load_start');
function load_start ( $field ) {
        
	$field ['default_value'] =  date('Y/m/d 12:00', time());

    return $field;

}

add_filter('acf/load_field/name=end', 'load_end');
function load_end ( $field ) {
        
	$field ['default_value'] =  date('Y/m/d 13:00', time());

    return $field;

}