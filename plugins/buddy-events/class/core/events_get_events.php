<?php

//Events
function get_events($parameters = '') {

    //Groups of the user
    if(!empty($parameters['groups'])) {
        $groups = $parameters['groups'];
    } else{
        $groups       = bp_get_user_groups($parameters['user_id']);
        $groups_admin = bp_get_user_groups($parameters['user_id'], array('is_admin' => true));
        $groups_mod   = bp_get_user_groups($parameters['user_id'], array('is_mod' => true));

        $groups = array_merge($groups, $groups_admin);
        $groups = array_merge($groups, $groups_mod);

        if(!empty($groups)) {
            $result = array();
            foreach($groups as $group) {
                $result[] = $group->group_id;
            }
            $groups = $result;
        }
    }

    $args = array(
        'post_type'   => array('event'),
        'numberposts' => -1,
        'meta_key'    => 'start',
        'orderby'     => 'meta_value_num',
        'order'       => 'DESC',
    );

    //Terms
    if (!empty($parameters['terms'])):
        $args['tax_query']['category-event'] = array(
            'taxonomy'      => 'category-event',
            'terms'         => $parameters['terms'],
            'field'       => 'slug',
        );
    endif;

    //Start
    if (!empty($parameters['start'])):

        $args['meta_query']['start'] = array(
            'key'     => 'start',
            'value'   => $parameters['start'],
            'compare' => '>=',
        );

    endif;

    //End
    if (!empty($parameters['end'])):

        $args['meta_query']['end'] = array(
            'key'     => 'end',
            'value'   => $parameters['end'],
            'compare' => '<=',
        );

    endif;

    //If a specific Group has been selected, we only display the events of this group

    if (!empty($parameters['group'])) {
        $args['meta_query']['group'][] = array(

            'key'     => 'group',
            'value'   => '"' . $parameters['group'] . '"',
            'compare' => 'LIKE',

        );

        //Let's stop the request here
        $query = new WP_Query($args);
        return $query->posts;

    }

    $args['meta_query']['access']['relation'] = 'OR';

    //We query all the public events
    if( !isset($parameters['show_groups_only']) || $parameters['show_groups_only'] == 'false' ) {

        $args['meta_query']['access']['private'] = array(

            'key'     => 'private',
            'value'   => 0,
            'compare' => '==',

        );

    }

    //If the user belongs to one or several groups, we also query the events of these groups   
    if (!empty($groups)):

        $args['meta_query']['access']['groups']['relation'] = 'OR';

        foreach ($groups as $key => $group_id):

            $args['meta_query']['access']['groups'][] = array(

                'key'     => 'group',
                'value'   => '"' . $group_id . '"',
                'compare' => 'LIKE',

            );

            endforeach;

    endif;

    //Let's see among the events if the user is among the guests
    if( !isset($parameters['show_groups_only']) || $parameters['show_groups_only'] == 'false' ) {
        $args['meta_query']['access']['invitations'] = array (
            'key'         => 'invitations',
            'value'     => '"'.$parameters['user_id'].'"',
            'compare'   => 'LIKE',
        );
    }

    $query = new WP_Query($args);

    return $query->posts;

}