<?php

//Exclude People who gave an answer from the invitations
// add_filter('acf/fields/user/query/name=invitations','nw_field_invitations_value2', 10, 3);
function nw_field_invitations_value2($args, $field, $post_id)
{

    $excluded_users = array();

    //Going
    if (!empty(get_field('going', $post_id))) {
        $excluded_users = array_merge($excluded_users, get_field('going', $post_id));
    }

    //Not going
    if (!empty(get_field('not_going', $post_id))) {
        $excluded_users = array_merge($excluded_users, get_field('not_going', $post_id));
    }

    $args['exclude'] = $excluded_users;

    return $args;
}