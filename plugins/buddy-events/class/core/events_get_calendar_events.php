<?php

//Insert Events into the Calendar
add_filter( 'collect_events_calendar_event', 'get_calendar_events', 10, 2);
function get_calendar_events ($calendar_events, $args_calendar) {
    
    $args = array(
        'user_id'   => get_current_user_id(),
    );
    
    if(!empty($args_calendar['start'])) $args['start'] = $args_calendar['start'];
    if(!empty($args_calendar['end'])) $args['end'] = $args_calendar['end'];
    if(!empty($args_calendar['group'])) $args['group'] = $args_calendar['group'];       
    if(!empty($args_calendar['groups'])) $args['groups'] = $args_calendar['groups'];    
    if(!empty($args_calendar['terms'])) $args['terms'] = $args_calendar['terms'];    
    if(!empty($args_calendar['show_groups_only'])) $args['show_groups_only'] = $args_calendar['show_groups_only'];

    $events = get_events ($args);

    if(!empty($events)) {
        
        if(!empty($calendar_events)):
            end($calendar_events);
            $i = key($calendar_events) +1;
        else : 
            $i = 0;
        endif;
        
        foreach ($events as $event) {
            
            $post_id = $event->ID;

            $start = new DateTime( get_field( 'start', $post_id ), wp_timezone() );
            $end = new DateTime( get_field( 'end', $post_id ), wp_timezone() );

            $calendar_events[$i] = array (
                'id'			=>	$post_id,
                'title'			=> 	html_entity_decode(get_the_title($post_id)),
                'class'			=>	'',
                'color'         =>  '#e73825',
                // 'url'			=> 	get_the_permalink($post_id),
                'all_day'       =>  false,
                'start'         => $start->format( 'c' ),
                'end'           => $end->format( 'c' ),
                'extendedProps'   => array(
                    'post_type'		=>	get_post_type($post_id),
                ),
            );
            
            //Add Taxonomies
            
            $taxonomies = get_object_taxonomies('event');
            
            if(!empty($taxonomies)):
            
                foreach($taxonomies as $key => $taxonomy):
            
                    $calendar_events[$i]['extendedProps']['terms'] = wp_get_post_terms($post_id, $taxonomy, array("fields" => "slugs"));
            
                endforeach;

                //Let's define a color which is the first category of the array
                $terms = get_the_terms($post_id, $taxonomy);

                if(!empty($terms)) {
                    $first_term = $terms[0];
                    $calendar_events[$i]['color'] = get_field('color', 'term_'.$first_term->term_id, true);
                }
            
            endif;

            //Groups
            $groups = get_field('group', $post_id);
            if(!empty($groups)) {
                $calendar_events[$i]['extendedProps']['groups'] = $groups;
            }
            
            //All Day Event
            if( 
                get_field('all_day', $post_id) == TRUE
                || ( date('H:i',strtotime(get_field ('start', $post_id))) == '00:00'
                && date('H:i',strtotime(get_field ('end', $post_id))) == '00:00' ) 
                || empty(get_field ('end', $post_id))
            ):
            
                $calendar_events [$i]['all_day'] = true;
                $calendar_events [$i]['start'] = date('Y-m-d',strtotime(get_field ('start', $post_id)));
                $calendar_events [$i]['end'] = date('Y-m-d',strtotime(get_field ('start', $post_id). " +1 day") );
            
            endif;
            
            $i++;

        }

    }

        
    return $calendar_events;
    
}