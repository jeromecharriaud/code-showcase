<?php

// add_filter('the_content', 'nw_register_event_invitation');
function nw_register_event_invitation($content) {

    //Only Single Page Event
    if (!is_singular('event')) {
        return $content;
    }

    //Action must concern the invitation registration
    if (!isset($_GET['action']) || $_GET['action'] != 'register-invitation') {
        return $content;
    }

    //We need a User ID
    if (!isset($_GET['user_id']) || empty($_GET['user_id'])) {
        return $content;
    }

    //We need a Nonce
    if (!isset($_GET['key']) || empty($_GET['key'])) {
        return $content;
    }

    //We need a status
    if (!isset($_GET['status']) || empty($_GET['status'])) {
        return $content;
    }

    //Variables
    $post_id  = get_the_ID();
    $user_id  = $_GET['user_id'];
    $userdata = get_userdata($user_id);
    $key      = $_GET['key'];
    $status   = $_GET['status'];

    //Let's verify the key !
    if ($_GET['key'] != get_user_meta($user_id, 'invitation_key_' . $post_id, true)) {

        $message = __('You have already given a response to the event or you don\'t have the right to give a response', 'jcorp-events');
    } else {

        //Invitation accepted !
        if ($status == 'accepted') {

            if (!empty(get_field('going'))) {
                $going = get_field('going');
            } else {
                $going = array();
            }

            $going[] = $user_id;

            //Let's update the guest list
            update_field('going', $going, $post_id);

            $message = __('You have accepted the invitation to the event', 'jcorp-events');
        }

        //Invitation declined !
        else if ($status == 'declined') {

            if (!empty(get_field('not_going'))) {
                $not_going = get_field('not_going');
            } else {
                $not_going = array();
            }

            $not_going[] = $user_id;

            //Let's update the guest list
            update_field('not_going', $not_going, $post_id);

            $message = __('You have declined the invitation to the event', 'jcorp-events');
        }

        //Let's remove the user from the invitations
        $invitations = get_field('invitations', $post_id);

        if (($key = array_search($user_id, $invitations)) !== false) {
            unset($invitations[$key]);
        }

        update_field('invitations', $invitations, $post_id);

        //Let's destroy the key !
        delete_user_meta($user_id, 'invitation_key_' . $post_id);
    }

    echo '<div class=""><p>' . $message . '</p></div>';

    return $content;
}
