<?php

add_action('init', 'nw_declare_taxonomy_event', 0);
function nw_declare_taxonomy_event() {

    $labels = array(
        'name'                       => _x('Categories', 'jcorp-events'),
        'singular_name'              => _x('Category', 'jcorp-events'),
        'menu_name'                  => __('Categories', 'jcorp-events'),
        'all_items'                  => __('All Categories', 'jcorp-events'),
        'parent_item'                => __('Parent Category', 'jcorp-events'),
        'parent_item_colon'          => __('Parent Category:', 'jcorp-events'),
        'new_item_name'              => __('New Category Name', 'jcorp-events'),
        'add_new_item'               => __('Add New Category', 'jcorp-events'),
        'edit_item'                  => __('Edit Category', 'jcorp-events'),
        'update_item'                => __('Update Category', 'jcorp-events'),
        'view_item'                  => __('View Category', 'jcorp-events'),
        'separate_items_with_commas' => __('Separate items with commas', 'jcorp-events'),
        'add_or_remove_items'        => __('Add or remove items', 'jcorp-events'),
        'choose_from_most_used'      => __('Choose from the most used', 'jcorp-events'),
        'popular_items'              => __('Popular Categories', 'jcorp-events'),
        'search_items'               => __('Search Categories', 'jcorp-events'),
        'not_found'                  => __('Not Found', 'jcorp-events'),
        'no_terms'                   => __('No items', 'jcorp-events'),
        'items_list'                 => __('Categories list', 'jcorp-events'),
        'items_list_navigation'      => __('Categories list navigation', 'jcorp-events'),
    );
    $args = array(
        'labels'             => $labels,
        'hierarchical'       => true,
        'public'             => true,
        'show_ui'            => true,
        'show_admin_column'  => true,
        'show_in_nav_menus'  => true,
        'show_tagcloud'      => true,

        'show_in_quick_edit' => true,
        'meta_box_cb'        => false,
    );

    register_taxonomy('category-event', array('event'), $args);
}
