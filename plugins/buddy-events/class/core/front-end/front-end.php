<?php

class BP_Events_Front_End {

    public function __construct() {
    }

    public function show_form_add_event() {

        acf_form_head();

        if (!current_user_can('edit_events')) {
            return;
        }

        if (!is_page()) :

?>

            <header class="entry-header">

                <h1 class="entry-title"><?php echo __('Add Event', 'jcorp-events'); ?></h1>

            </header>

<?php

        endif;

        $options = array(

            'post_id'         => 'new_post',
            'post_title'      => true,
            'post_content'    => true,
            'return'          => '%post_url%',
            'new_post'        => array(
                'post_type'   => 'event',
                'post_status' => 'publish',
            ),
            'updated_message' => __("New Event created !", 'jcorp-events'),
            'submit_value'    => __("Publish Event", 'jcorp-events'),
        );

        acf_form($options);
    }

    function edit_event($content) {

        global $wp;
        global $post;

        if (get_post_type() != 'event') {
            return $content;
        }

        if (!current_user_can('edit_others_events') && get_current_user_id() != $post->post_author) {
            return $content;
        }

        //Edit Page
        if (!empty($wp->query_vars['action']) && $wp->query_vars['action'] == 'edit') :

            //Let's close the comments
            $post->comment_status = 'closed';

            //Form
            $options = array(

                'post_id'         => get_the_ID(),
                'post_title'      => true,
                'post_content'    => true,
                'return'          => '%post_url%',
                'new_post'        => array(
                    'post_type'   => 'event',
                    'post_status' => 'publish',
                ),
                'updated_message' => __("Event updated !", 'jcorp-events'),
                'submit_value'    => __("Update Event", 'jcorp-events'),
            );

            $content = acf_form($options);

        //Other Event Pages
        else :

            $content .= '<a style="float:right;" class="button" href="action/edit">' . __('Edit Event', 'jcorp-events') . '</a>';

        endif;

        return $content;
    }
}
