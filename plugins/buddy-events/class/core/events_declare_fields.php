<?php

if (function_exists('acf_add_local_field_group')) :

    $fields = array();

    //Thumbnail
    $fields[] = array(
        'key'               => 'field_5e9028819e185',
        'label'             => __('Featured Image', 'jcorp-events'),
        'name'              => 'featured_image',
        'type'              => 'image',
        'instructions'      => '',
        'required'          => 0,
        'conditional_logic' => 0,
        'wrapper'           => array(
            'width' => '',
            'class' => '',
            'id'    => '',
        ),
        'return_format'     => 'array',
        'preview_size'      => 'medium',
        'library'           => 'uploadedTo',
        'min_width'         => '',
        'min_height'        => '',
        'min_size'          => '',
        'max_width'         => '',
        'max_height'        => '',
        'max_size'          => '2',
        'mime_types'        => '',
    );

    //Categories
    $fields[] = array(
        'key'               => 'field_5f76cf19e2d25',
        'label'             => 'Category',
        'name'              => 'category',
        'type'              => 'taxonomy',
        'instructions'      => '',
        'required'          => 0,
        'conditional_logic' => 0,
        'wrapper'           => array(
            'width' => '',
            'class' => '',
            'id'    => '',
        ),
        'taxonomy'          => 'category-event',
        'field_type'        => 'checkbox',
        'add_term'          => 0,
        'save_terms'        => 1,
        'load_terms'        => 0,
        'return_format'     => 'id',
        'multiple'          => 0,
        'allow_null'        => 0,
    );

    $fields[] = array(
        'key'               => 'field_5e8199bd17e09',
        'label'             => __('Private', 'jcorp-events'),
        'name'              => 'private',
        'type'              => 'true_false',
        'instructions'      => '',
        'required'          => 0,
        'conditional_logic' => 0,
        'wrapper'           => array(
            'width' => '',
            'class' => '',
            'id'    => '',
        ),
        'message'           => '',
        'default_value'     => 0,
        'ui'                => 1,
        'ui_on_text'        => 'Privé',
        'ui_off_text'       => 'Public',
    );

    //Group
    if (array_key_exists('groups', get_option('bp-active-components'))) {

        $fields[] = array(
            'key'               => 'field_5afd037084436',
            'label'             => __('Group', 'jcorp-events'),
            'name'              => 'group',
            'type'              => 'select',
            'instructions'      => __('If the event is related to one or several groups, select one here', 'jcorp-events'),
            'required'          => 0,
            'conditional_logic' => 0,
            'wrapper'           => array(
                'width' => '',
                'class' => '',
                'id'    => '',
            ),
            'choices'           => array(),
            'default_value'     => array(),
            'allow_null'        => 0,
            'multiple'          => 1,
            'ui'                => 1,
            'ajax'              => 0,
            'return_format'     => 'value',
            'placeholder'       => '',
            'conditional_logic' => array(
                array(
                    array(
                        'field'    => 'field_5e8199bd17e09',
                        'operator' => '==',
                        'value'    => '1',
                    ),
                ),
            ),
        );
    }

    //Invitations
    $fields[] = array(
        'key'               => 'field_5e82de5cb61e9',
        'label'             => __('Invitations', 'jcorp-events'),
        'name'              => 'invitations',
        'type'              => 'select',
        'instructions'      => '',
        'required'          => 0,
        'conditional_logic' => array(
            array(
                array(
                    'field'    => 'field_5e8199bd17e09',
                    'operator' => '==',
                    'value'    => '1',
                ),
            ),
        ),
        'wrapper'           => array(
            'width' => '',
            'class' => '',
            'id'    => '',
        ),
        'role'              => '',
        'allow_null'        => 0,
        'multiple'          => 1,
        'ui'                => 1,
        'return_format'     => 'id',
    );

    $fields[] = array(
        'key'               => 'field_5ecdfde474257',
        'label'             => __('Send Reminders', 'jcorp-events'),
        'name'              => 'reminder',
        'type'              => 'checkbox',
        'instructions'      => 'Choisissez un ou plusieurs rappels par email aux invités de l\'événement',
        'required'          => 0,
        'conditional_logic' => array(
            array(
                array(
                    'field'    => 'field_5e8199bd17e09',
                    'operator' => '==',
                    'value'    => '1',
                ),
            ),
        ),
        'wrapper'           => array(
            'width' => '',
            'class' => '',
            'id'    => '',
        ),
        'choices'           => array(
            1  => 'Une heure avant',
            6  => 'Six heures avant',
            24 => 'Un jour avant',
            72 => 'Trois jours avant',
        ),
        'default_value'     => false,
        'allow_null'        => 0,
        'multiple'          => 1,
        'ui'                => 1,
        'return_format'     => 'value',
        'ajax'              => 0,
        'placeholder'       => '',
    );

    //Invitations Accepted
    /*
    $fields[] = array (
    'key' => 'field_5e8413daad9ce',
    'label' => __('Going', 'jcorp-events'),
    'name' => 'going',
    'type' => 'user',
    'instructions' => '',
    'required' => 0,
    'conditional_logic' => 0,
    'wrapper' => array(
    'width' => '',
    'class' => '',
    'id' => '',
    ),
    'choices' => array(
    ),
    'default_value' => array(
    ),
    'allow_null' => 0,
    'multiple' => 1,
    'ui' => 0,
    'ajax' => 0,
    'return_format' => 'id',
    'placeholder' => '',
    'conditional_logic' => array(
    array(
    array(
    'field' => 'field_5e8199bd17e09',
    'operator' => '==',
    'value' => '1',
    ),
    ),
    ),
    'disabled'   => 1,

    );

    //Invitations Accepted
    $fields[] = array (
    'key' => 'field_5e8413daad9cf',
    'label' => __('Not going', 'jcorp-events'),
    'name' => 'not_going',
    'type' => 'user',
    'instructions' => '',
    'required' => 0,
    'conditional_logic' => 0,
    'wrapper' => array(
    'width' => '',
    'class' => '',
    'id' => '',
    ),
    'choices' => array(
    ),
    'default_value' => array(
    ),
    'allow_null' => 0,
    'multiple' => 1,
    'ui' => 0,
    'ajax' => 0,
    'return_format' => 'id',
    'placeholder' => '',
    'conditional_logic' => array(
    array(
    array(
    'field' => 'field_5e8199bd17e09',
    'operator' => '==',
    'value' => '1',
    ),
    ),
    ),
    'disabled'   => 1,

    );
     */

    //All Day
    /*
    $fields[] = array(
        'key'               => 'field_579dfczfd6795a',
        'label'             => __('All day', 'jcorp-events'),
        'name'              => 'all_day',
        'type'              => 'true_false',
        'ui'                => 1,
        'ui_on_text'        => '',
        'ui_off_text'       => '',
        'instructions'      => __('Select this if the event takes the whole day', 'jcorp-events'),
        'required'          => 0,
        'conditional_logic' => 0,
        'wrapper'           => array(
            'width' => '',
            'class' => '',
            'id'    => '',
        ),
        'ui_on_text'        => __('Yes', 'jcorp-events'),
        'ui_off_text'       => __('No', 'jcorp-events'),
    );
    */
    //Start
    $fields[] = array(
        'key'               => 'field_579d9efd6795a',
        'label'             => __('Start', 'jcorp-events'),
        'name'              => 'start',
        'type'              => 'date_time_picker',
        'instructions'      => '',
        'required'          => 1,
        'conditional_logic' => 0,
        'wrapper'           => array(
            'width' => 50,
            'class' => '',
            'id'    => '',
        ),
        'display_format'    => 'd/m/Y H:i',
        'return_format'     => 'Y-m-d H:i:s',
        'first_day'         => 1,
    );

    //End
    $fields[] = array(
        'key'               => 'field_579da042f8801',
        'label'             => __('End', 'jcorp-events'),
        'name'              => 'end',
        'type'              => 'date_time_picker',
        'instructions'      => '',
        'required'          => 1,
        'conditional_logic' => 0,
        'wrapper'           => array(
            'width' => 50,
            'class' => '',
            'id'    => '',
        ),
        'display_format'    => 'd/m/Y H:i',
        'return_format'     => 'Y-m-d H:i:s',
    );

    //Address
    $fields[] = array(
        'key'               => 'field_57a1bfa46f517',
        'label'             => __('Address', 'jcorp-events'),
        'name'              => 'location',
        'type'              => 'google_map',
        'instructions'      => '',
        'required'          => 0,
        'conditional_logic' => 0,
        'wrapper'           => array(
            'width' => '',
            'class' => '',
            'id'    => '',
        ),
        'center_lat'        => '',
        'center_lng'        => '',
        'zoom'              => '',
        'height'            => '',
    );

    $args = array(
        'key'                   => 'group_579d9e506c98b',
        'title'                 => __('Details', 'jcorp-events'),
        'fields'                => $fields,
        'location'              => array(
            array(
                array(
                    'param'    => 'post_type',
                    'operator' => '==',
                    'value'    => 'event',
                ),
            ),
        ),
        'menu_order'            => 0,
        'position'              => 'normal',
        'style'                 => 'default',
        'label_placement'       => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen'        => '',
        'active'                => 1,
        'description'           => '',
    );

    acf_add_local_field_group($args);

endif;

if (function_exists('acf_add_local_field_group')) :

    acf_add_local_field_group(array(
        'key'                   => 'group_5eccc6fdd6d95',
        'title'                 => __('Event - Category', 'jcorp-events'),
        'fields'                => array(
            array(
                'key'               => 'field_5eccc705f2608',
                'label'             => 'Couleur',
                'name'              => 'color',
                'type'              => 'color_picker',
                'instructions'      => '',
                'required'          => 0,
                'conditional_logic' => 0,
                'wrapper'           => array(
                    'width' => '',
                    'class' => '',
                    'id'    => '',
                ),
                'default_value'     => '#007CFF',
            ),
        ),
        'location'              => array(
            array(
                array(
                    'param'    => 'taxonomy',
                    'operator' => '==',
                    'value'    => 'category-event',
                ),
            ),
        ),
        'menu_order'            => 0,
        'position'              => 'normal',
        'style'                 => 'default',
        'label_placement'       => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen'        => '',
        'active'                => true,
        'description'           => '',
    ));

endif;
