<?php

//Transform the date and time values into time
add_filter('acf/update_value/type=date_picker', 'acf_update_time_format', 10, 3);
add_filter('acf/update_value/type=date_time_picker', 'acf_update_time_format', 10, 3);
function acf_update_time_format ( $value, $post_id, $field  ) {
    
    if(empty($value)) return;

    $value = strtotime($value);
    
    return $value;
    
}