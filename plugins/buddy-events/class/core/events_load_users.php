<?php 

add_filter('acf/load_field/name=invitations', 'bp_events_load_users');
function bp_events_load_users ( $field ) {

    $users = get_users();

    if(!empty($users)) {
        foreach($users as $user) {
            $userdata = $user->data;
            $field['choices'][$userdata->ID] = nw_mb_ucfirst($userdata->display_name).' ('.$userdata->user_email.')';
        }
    }

    asort($field['choices']);

    return $field;

}