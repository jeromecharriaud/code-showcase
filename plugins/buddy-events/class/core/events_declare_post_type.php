<?php

// Register Custom Post Event
add_action('init', 'nw_declare_post_type_event', 0);
function nw_declare_post_type_event() {

    $labels = array(
        'name'           => __('Events', 'jcorp-events'),
        'singular_name'  => __('Event', 'jcorp-events'),
        'menu_name'      => __('Events', 'jcorp-events'),
        'name_admin_bar' => __('Event', 'jcorp-events'),
    );
    $rewrite = array(
        'slug'       => 'calendar',
        'with_front' => true,
        'pages'      => true,
        'feeds'      => true,
    );
    $args = array(
        'label'               => __('Events', 'jcorp-events'),
        'description'         => __('Description', 'jcorp-events'),
        'labels'              => $labels,
        'supports'            => array('title', 'editor', 'comments', 'author'),
        'taxonomies'          => array(),
        'hierarchical'        => false,
        'public'              => true,
        'menu_icon'           => 'dashicons-calendar-alt',
        'menu_position'       => 8,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite'             => $rewrite,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
        'show_ui'             => true,

    );

    register_post_type('event', $args);
}
