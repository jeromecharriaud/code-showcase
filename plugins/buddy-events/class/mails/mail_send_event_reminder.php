<?php

add_action('cron_nw_send_post_reminder', 'nw_send_post_reminder');
if (!wp_next_scheduled('cron_nw_send_post_reminder')) {
    wp_schedule_event(time(), 'hourly', 'cron_nw_send_post_reminder');
}

function nw_send_post_reminder()
{

    $args = array(
        'post_type' =>     array('post'),
        'numberposts'   => -1,
        'meta_key'   => 'start',
        'orderby'    => 'meta_value_num',
        'order'      => 'DESC',
        'meta_query' => array(
            array(
                'key'         => 'start',
                'value'     => time(),
                'compare'   => '>=',
            ),
            array(
                'key'       =>  'invitations',
                'compare'   =>  'EXISTS',
            )
        )
    );

    $posts = get_posts($args);

    if (!empty($posts)) {

        foreach ($posts as $post) {

            //Only if the event is private 
            if (get_field('private', $post->ID) == FALSE) continue;

            $start = get_field('start', $post->ID, true);
            $reminder = (array) get_field('reminder', 306);

            $difference = strtotime($start) - time();
            $difference = round($difference / 60 / 60);

            if (in_array($difference, $reminder)) {

                $invitations = get_field('invitations', $post->ID, true);

                $args = array(
                    'tokens' => array(
                        'site.name'     => get_bloginfo('name'),
                        'reminder'      => $difference,
                        'post.name'     => get_the_title($post->ID),
                        'post.url'      => get_permalink($post->ID),
                    ),
                );

                if (!empty($invitations)) {

                    foreach ($invitations as $key => $user_id) {

                        bp_send_email('post_reminder', (int) $user_id, $args);
                    }
                }
            }
        }
    }
}