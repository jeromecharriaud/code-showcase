<?php

add_filter('acf/update_value/name=invitations', 'nw_send_event_invitations2', 10, 3);
function nw_send_event_invitations2($value, $post_id, $field) {

    //Only if the event is private
    if (get_field('private', $post_id) == FALSE) {
        return;
    }

    $title = get_the_title($post_id);
    $post  = get_post($post_id);

    $previous_invitations = get_field('invitations', $post_id);
    $new_invitations      = $value;

    $people_invited = $new_invitations;
    // $people_not_more_invited = array();

    if (is_array($previous_invitations) && is_array($new_invitations)) {

        //New people invited
        $people_invited = array_diff($new_invitations, $previous_invitations);

        //People not more invited
        // $people_not_more_invited = array_diff($previous_invitations, $new_invitations);
    }

    //Let's send an email to the people who are newly invited
    if (!empty($people_invited)) {

        //Header
        $headers = array('Content-Type: text/html; charset=UTF-8');

        //Subject
        $subject = __('You have been invited to a new event', 'jcorp-events') . ' | ' . $title;

        //Message
        $message_header = sprintf('You have been invited to a new event “%s”' . "<br/>", $title);
        $message_header .= sprintf('Link: %s', get_permalink($post_id));
        $message_header .= "<br/>";

        $args = array(
            'tokens' => array(
                'site.name'  => get_bloginfo('name'),
                'event.name' => $title,
                'event.url'  => get_permalink($post_id),
            ),
        );

        foreach ($people_invited as $key => $user_id) :

            bp_send_email('new_invitation', (int) $user_id, $args);

        /*

        // $userdata = get_userdata($user_id);

        //Check if the user has an invitation to the event
        if(empty(get_user_meta($user_id, 'invitation_key_'.$post_id, true))) {
        $invitation_key = wp_generate_password(24, false);
        add_user_meta($user_id, 'invitation_key_'.$post_id, $invitation_key, true);
        }
        else {
        $invitation_key = get_user_meta($user_id, 'invitation_key_'.$post_id, true);
        }

        $bare_url = get_permalink($post_id).'?action=register-invitation&user_id='.$user_id.'&key='.$invitation_key;

        $accept = '<a href="'.$bare_url.'&status=accepted'.'">'.__('Accept','jcorp-events').'</a>';

        $decline = '<a href="'.$bare_url.'&status=declined'.'">'.__('Decline','jcorp-events').'</a>';

        $message = $message_header;
        $message .= $accept.' | '.$decline;

        wp_mail ($userdata->user_email, $subject, $message, $headers);

         */

        endforeach;
    }

    //Let's delete the invitation key to the people not more invited
    /*
    if(!empty($people_not_more_invited)) {

    foreach ($people_not_more_invited as $key => $user_id) {

    delete_user_meta($user_id, 'invitation_key_'.$post_id);

    }

    }
     */

    return $value;
}
