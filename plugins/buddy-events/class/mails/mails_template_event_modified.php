<?php

add_action('bp_core_install_emails', 'bp_events_email_template_event_modified');
function bp_events_email_template_event_modified() {

    // Do not create if it already exists and is not in the trash
    $post_exists = post_exists('[{{{site.name}}}] Un événement a été modifié');

    if ($post_exists != 0 && get_post_status($post_exists) == 'publish') {
        return;
    }

    // Create post object
    $my_post = array(
        'post_title'   => __('[{{{site.name}}}] L\'événement {{{event.name}}} a été modifié.', 'jcorp-events'),
        'post_content' => __('L\'événement {{{event.name}}} a été modifié {{{event.url}}}', 'jcorp-events'), // HTML email content.
        'post_excerpt' => __('L\'événement a été modifié', 'jcorp-events'), // Plain text email content.
        'post_status'  => 'publish',
        'post_type'    => bp_get_email_post_type(), // this is the post type for emails
    );

    // Insert the email post into the database
    $post_id = wp_insert_post($my_post);

    if ($post_id) {
        // add our email to the taxonomy term 'post_received_comment'
        // Email is a custom post type, therefore use wp_set_object_terms

        $tt_ids = wp_set_object_terms($post_id, 'event_modified', bp_get_email_tax_type());
        foreach ($tt_ids as $tt_id) {
            $term = get_term_by('term_taxonomy_id', (int) $tt_id, bp_get_email_tax_type());
            wp_update_term((int) $term->term_id, bp_get_email_tax_type(), array(
                'description' => 'Un événement a été modifié',
            ));
        }
    }
}
