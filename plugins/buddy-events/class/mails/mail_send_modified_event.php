<?php

add_action('transition_post_status', 'nw_send_event_changes_modifications', 999, 3);
function nw_send_event_changes_modifications($new_status, $old_status, $post) {

    if (('publish' === $new_status && 'publish' === $old_status)
        && 'event' === $post->post_type
    ) {

        //Only if the event is private
        if (get_field('private', $post->ID) == FALSE) {
            return;
        }

        $args = array(
            'tokens' => array(
                'site.name'  => get_bloginfo('name'),
                'event.name' => get_the_title($post->ID),
                'event.url'  => get_permalink($post->ID),
            ),
        );

        $invitations = get_field('invitations', $post->ID);

        if (!empty($invitations)) {

            foreach ($invitations as $key => $user_id) {

                bp_send_email('event_modified', (int) $user_id, $args);
            }
        }
    }
}