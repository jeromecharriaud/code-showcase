<?php

class BP_Events_Calendar {

    public function __construct() {

        $files = glob(plugin_dir_path(__FILE__) . 'calendar/*.php');

        //Include All Files
        foreach ($files as $file):

            require_once $file;

        endforeach;

    }

}