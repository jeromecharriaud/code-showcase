<?php

class BP_Events_Core {

    public function __construct() {

        $files = glob(plugin_dir_path(__FILE__) . 'core/*.php');

        //Include All Files
        foreach ($files as $file) :

            require_once $file;

        endforeach;

        //Front End
        if (get_field('bp_events_front_end', 'option') == true) :

            include_once plugin_dir_path(__FILE__) . '/core/front-end/front-end.php';
            new BP_Events_Front_End();

        endif;

        //Template Event
        add_filter('template_include', array($this, 'include_templates'));
    }

    public function include_templates($template) {

        if (is_singular('event')) {
        }

        if (is_post_type_archive('event')) {

            $template = WP_PLUGIN_DIR . '/' . plugin_basename('jcorp-events') . '/template/archive-event.php';
        }

        return $template;
    }
}
