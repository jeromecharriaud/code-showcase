<?php

add_action('wp_ajax_get_calendar_json_events', 'get_calendar_json_events');
add_action('wp_ajax_nopriv_get_calendar_json_events', 'get_calendar_json_events');
function get_calendar_json_events() {


    // Short-circuit if the client did not give us a date range.
    if (!isset($_REQUEST['start']) || !isset($_REQUEST['end'])) {
        return;
    }

    $args = array(
        'start' => strtotime($_REQUEST['start']),
        'end'   => strtotime($_REQUEST['end']),
    );

    //Terms
    if (!empty($_REQUEST['terms'])) {
        $args['terms'] = $_REQUEST['terms'];
    }

    //Single Group
    if (!empty($_REQUEST['group'])) {
        $args['group'] = $_REQUEST['group'];
        $args['show_groups_only'] = 'true';
    }

    //Groups
    if (!empty($_REQUEST['groups'])) {
        $args['groups'] = $_REQUEST['groups'];
        $args['show_groups_only'] = 'true';
    }

    $calendar_events = array();

    $events = apply_filters('collect_events_calendar_' . $_REQUEST['post_type'], $calendar_events, $args);

    $events = json_encode($events);

    echo $events;

    exit;

}