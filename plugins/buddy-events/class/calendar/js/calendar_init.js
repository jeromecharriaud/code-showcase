jQuery(document).ready(function ($) {
	//Labels Color
	var styleElem = document.head.appendChild(document.createElement('style'))
	var filter_terms = $('#calendar-filters .filter-term')
	filter_terms.each(function (index) {
		let color = this.getAttribute('data-color')
		let term = this.getAttribute('data-term')
		styleElem.innerHTML +=
			'li[data-term=' +
			term +
			'] input[type=checkbox]:checked+label:before {background: ' +
			color +
			';}'
	})

	//Filters
	let filters_list = []
	filters_list['terms'] = new Array()
	filters_list['groups'] = new Array()

	//Group
	if (typeof group !== 'undefined') {
		var group_id = group
	} else {
		var group_id = null
	}

	//Terms
	if (typeof terms == 'undefined') {
		var terms = Array
	}

	var left = 'today, prev,next'
	var center = 'title'
	var right = 'dayGridMonth,dayGridWeek,dayGridDay'

	//Mobile Settings
	if (
		/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			navigator.userAgent
		)
	) {
		left = 'prev'
		center = 'title'
		right = 'next'
	}

	var calendarEl = document.getElementById('calendar')
	var calendar = new FullCalendar.Calendar(calendarEl, {
		timeZone: 'local',
		initialView: 'dayGridMonth',
		aspectRatio: 1,
		locale: 'fr',
		editable: false,
		headerToolbar: {
			left: left,
			center: center,
			right: right,
		},
		displayEventEnd: true,
		events: function (info, successCallback, failureCallback) {
			$('#loading_container').show()
			$('.fc-button').addClass('fc-state-disabled')
			jQuery.ajax({
				url: my_ajax.ajaxurl,
				type: 'POST',
				data: {
					action: 'get_calendar_json_events',
					start: info.start,
					end: info.end,
					post_type: 'event',
					group: group_id,
					groups: filters_list['groups'],
					terms: filters_list['terms'],
				},
				datatype: 'json',
				success: function (response) {
					var data = jQuery.parseJSON(response)
					successCallback(data)
					$('#loading_container').hide()
					$('.fc-button').removeClass('fc-state-disabled')
				},
			})
		},

		eventDidMount: function (info) {
			var dateStart = info.event.start
			var dateEnd = info.event.end
			
			var start =
				dateStart.getDate() +
				'-' +
				('0' + (dateStart.getMonth() + 1)).slice(-2) +
				'-' +
				dateStart.getFullYear() +
				' à ' +
				dateStart.getHours() +
				'h' +
				('0' + dateStart.getMinutes()).slice(-2)

			var end =
				dateEnd.getDate() +
				'-' +
				('0' + (dateEnd.getMonth() + 1)).slice(-2) +
				'-' +
				dateEnd.getFullYear() +
				' à ' +
				dateEnd.getHours() +
				'h' +
				('0' + dateEnd.getMinutes()).slice(-2)

			var tooltip = new Tooltip(info.el, {
				title: info.event.title,
				placement: 'top',
				trigger: 'hover',
				container: 'body',
				template:
					'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div><div class="start">' +
					dom_labels.start +
					' ' +
					start +
					'</div><div class="end">' +
					dom_labels.end +
					' ' +
					end +
					'</div></div>',
			})
		},
	})
	calendar.render()

	//Taxonomy Filter System
	$('input[type=checkbox][class=filter][data-type=term]').change(function () {
		filters_list['terms'] = new Array()

		var filters = $(
			'#calendar-filters input[type=checkbox][data-type=term]:checked'
		)

		filters.each(function (index) {
			filters_list['terms'].push(this.value)
		})

		calendar.refetchEvents()
	})

	//Group Filter System
	$('input[type=checkbox][class=filter][data-type=group]').change(
		function () {
			filters_list['groups'] = new Array()

			var filters = $(
				'#calendar-filters input[type=checkbox][data-type=group]:checked'
			)

			filters.each(function (index) {
				filters_list['groups'].push(this.value)
			})

			calendar.refetchEvents()
		}
	)

	/*
    	$('input[type=checkbox][class=filter]').change(function () {
			filters_list = []
			var filters = $('#calendar-filters input[type=checkbox]:checked')

			filters.each(function (index) {
				filters_list.push(this.value)
			})

			calendar.rerenderEvents()
        })
    
    */
})

/*
jQuery(document).ready(function ($) {
	//Labels Color
	var styleElem = document.head.appendChild(document.createElement('style'))
	var filter_terms = $('#calendar-filters .filter-term')
	filter_terms.each(function (index) {
		let color = this.getAttribute('data-color')
		let term = this.getAttribute('data-term')
		styleElem.innerHTML +=
			'li[data-term=' +
			term +
			'] input[type=checkbox]:checked+label:before {background: ' +
			color +
			';}'
	})

	//Filters
	var filters_list = ''
	var defaultView = 'agendaWeek'
	var left = 'dayGridMonth,timeGridWeek,timeGridDay'
	var center = 'title'
	var right = 'today, prev,next'

	if (typeof group !== 'undefined') {
		var group_id = group
	} else {
		var group_id = null
	}

	//Mobile Settings
	if (
		/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			navigator.userAgent
		)
	) {
		left = 'prev'
		center = 'title'
		right = 'next'
	}

	var calendarEl = document.getElementById('calendar-events')

	var calendar = new FullCalendar.Calendar(calendarEl, {
		plugins: ['dayGrid', 'timeGrid'],
		events: {
			url:
				ajaxurl +
				'?action=get_calendar_json_events&post_type=event&group=' +
				group_id,
			type: 'GET',
		},
		header: {
			left: left,
			center: center,
			right: right,
		},
		locale: 'fr',
		displayEventEnd: true,
		timeZone: 'Europe/Paris',
		eventLimit: true,
		loading_container: function (isLoading, view) {
			if (isLoading) {
				$('.fc-button').addClass('fc-state-disabled')
			} else {
				$('#loading_container').hide()
				$('.fc-button').removeClass('fc-state-disabled')
			}
		},
		eventRender: function (info) {
			//If no filters, we return true. Else we compare the term slugs of the events to the array of the filter

			if (filters_list.length === 0) return true

			var taxonomies = info.event.extendedProps.terms

			let intersection = filters_list.filter((x) =>
				taxonomies.includes(x)
			)

			//If we find at least one value, we display the event
			if (intersection.length > 0) {
				return true
			} else {
				return false
			}
		},
		eventDidMount: function (info) {
			var dateStart = info.event.start
			var dateEnd = info.event.end

			var start =
				dateStart.getDate() +
				'-' +
				('0' + (dateStart.getMonth() + 1)).slice(-2) +
				'-' +
				dateStart.getFullYear() +
				' à ' +
				dateStart.getHours() +
				'h' +
				('0' + dateStart.getMinutes()).slice(-2)

			var end =
				dateEnd.getDate() +
				'-' +
				('0' + (dateEnd.getMonth() + 1)).slice(-2) +
				'-' +
				dateEnd.getFullYear() +
				' à ' +
				dateEnd.getHours() +
				'h' +
				('0' + dateEnd.getMinutes()).slice(-2)

			var tooltip = new Tooltip(info.el, {
				title: info.event.title,
				placement: 'top',
				trigger: 'hover',
				container: 'body',
				template:
					'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div><div class="booked_by">' +
					dom_labels.reserved_by +
					' ' +
					info.event.extendedProps.author +
					'</div><div class="start">' +
					dom_labels.start +
					' ' +
					start +
					'</div><div class="end">' +
					dom_labels.end +
					' ' +
					end +
					'</div></div>',
			})
		},
	})

	$('input[type=checkbox][class=filter]').change(function () {
		filters_list = []
		var filters = $('#calendar-filters input[type=checkbox]:checked')

		filters.each(function (index) {
			filters_list.push(this.value)
		})

		calendar.rerenderEvents()
	})

	calendar.render()
})
*/
