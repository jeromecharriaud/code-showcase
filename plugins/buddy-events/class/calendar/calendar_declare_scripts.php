<?php

//Full Calendar
add_action('wp_enqueue_scripts', 'nw_events_declare_calendar_scripts');
function nw_events_declare_calendar_scripts() {

    //Config
    wp_register_script('calendar-events', plugin_dir_url(__FILE__) . 'js/calendar_init.js', array('jquery'), '1.0.0', true);

}
?>