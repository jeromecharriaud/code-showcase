<?php

//Full Calendar
add_shortcode('calendar-events', 'nw_events_declare_shortcode_calendar');
function nw_events_declare_shortcode_calendar($args) {
    global $wp;
    $current_page_url = home_url($wp->request);

    //Show the calendar if there are no actions
    if (isset($_GET['action'])) {
        $form_add_event = new BP_Events_Front_End;

        if ($_GET['action'] = 'add-event') {
            $form_add_event->show_form_add_event();
        }
        return;
    }

    //Styles & Scripts
    nw_calendar_enqueue_files();
    // nw_scheduler_enqueue_files();
    nw_events_enqueue_files();

    //Ajax
    wp_localize_script('calendar-events', 'my_ajax', array('ajaxurl' => admin_url('admin-ajax.php')));

    //Send Arguments to the JS Script
    if (!empty($args)) :
        foreach ($args as $key => $value) :
            wp_localize_script('calendar-events', $key, $value);

        endforeach;

    endif;

    //Translations
    wp_localize_script(
        'calendar-events',
        'dom_labels',
        array(
            'reserved_by'  => __('Reserved by:', 'jcorp-bookings'),
            'start'        => __('Start:', 'jcorp-bookings'),
            'end'          => __('End:', 'jcorp-bookings'),
            'resources'    => __('Resources', 'jcorp-bookings'),
            'hide_all'     => __('Hide all', 'jcorp-bookings'),
            'show_all'     => __('Show all', 'jcorp-bookings'),
            'select_all'   => __('Select all', 'jcorp-bookings'),
            'unselect_all' => __('Unselect all', 'jcorp-bookings'),
        )
    );

    //Taxonomies
    $taxonomies = get_object_taxonomies('event');


    echo '<div id="calendar-container">';

    echo '<div id="calendar-left-column">';

    echo '<div id="calendar-filters">';

    echo '<h3>' . __('Filters', 'jcorp-events') . '</h3>';

    if (!empty($taxonomies)) :

        foreach ($taxonomies as $key => $taxonomy) {

            $labels = get_taxonomy_labels(get_taxonomy($taxonomy));

            echo '<h4>' . $labels->name . '</h4>';

            echo '<div data-taxonomy=' . $taxonomy . '>';

            $args = array(
                'taxonomy'   => $taxonomy,
                'hide_empty' => true,
                'orderby'    => 'name',
                'order'      => 'ASC',
                'number'     => '',
                'exclude'    => '',
                'include'    => '',
                'pad_counts' => true,
            );

            $terms = get_terms($args);

            if (!empty($terms)) {

                echo '<ul>';

                foreach ($terms as $term) {

                    $color = get_field('color', 'term_' . $term->term_id, true);

                    echo '<li class="filter-term" data-term="' . $term->slug . '" data-type="term" data-color="' . $color . '">';
                    echo '<input type="checkbox" class="filter" data-type="term" data-taxonomy="' . $taxonomy . '" value="' . $term->slug . '">';
                    echo '<label class="filter-label">' . $term->name . '</label>';
                    echo '</li>';
                }

                echo '</ul>';
            }
        }

    endif;

    if (bp_is_active('groups') && bp_is_group() == false) {

        echo '<h4>' . __('Groups', 'jcorp-events') . '</h4>';

        $groups = groups_get_groups(
            array(
                'user_id'   =>  get_current_user_id(),
            )
        );
        $groups = $groups['groups'];

        if (!empty($groups)) {

            echo '<ul>';

            foreach ($groups as $group) {

                echo '<li class="filter-group" data-type="group" data-group="' . $group->id . '">';
                echo '<input type="checkbox" class="filter" data-type="group" value="' . $group->id . '">';
                echo '<label class="filter-label">' . $group->name . '</label>';
                echo '</li>';
            }

            echo '</ul>';
        }
    }

    echo '</div>';

    echo '</div>';

    //Create Event Button
    if (get_field('bp_events_front_end', 'option') == true && current_user_can('edit_posts')) {

        echo '<div id="calendar-actions">';

        echo '<h3>' . __('Actions', 'jcorp-bookings') . '</h3>';

        echo '<a href="' . $current_page_url . '?action=add-event" class="button">' . __('Create Event', 'jcorp-events') . '</a>';

        echo '</div>';
    }

    //Loading
    echo '<div id="loading_container" style="position:relative; font-size: 14px;">';

    echo '<span class="acf-spinner is-active" style="display: inline-block;"></span> ' . __('Events loading', 'jcorp-bookings');

    echo '</div>';

?>

    <!--Calendar-->
    </div>
    <div id="calendar"></div>


    </div>



<?php

}

?>