<?php

add_action('bp_setup_nav', 'nw_setup_profile_calendar_nav');

function nw_setup_profile_calendar_nav() {

    global $bp;

    bp_core_new_nav_item(array(
        'name'                => __('Calendar', 'jcorp-events'),
        'slug'                => 'calendar',
        'screen_function'     => 'nw_events_bp_profile_calendar',
        'position'            => 40,
        'parent_url'          => bp_loggedin_user_domain() . '',
        'parent_slug'         => $bp->slug,
        'default_subnav_slug' => '',
    ));
}

function nw_events_bp_profile_calendar() {

    add_action('bp_template_content', 'nw_events_show_profile_calendar');
    bp_core_load_template('buddypress/members/single/plugins');
}

function nw_events_show_profile_calendar() {
?>
    <div class="bp-profile-wrapper">
        <div class="bp-profile-content">
            <div class="profile public">
                <header class="entry-header profile-loop-header profile-header flex align-items-center">
                    <h1 class="entry-title bb-profile-title">Calendrier</h1>
                </header>
                <div class="bp-widget details">
                    <?php do_shortcode('[calendar-events]'); ?>
                </div>
            </div>
        </div>
    </div>
<?php

}

add_action('buddyboss_theme_after_bb_profile_menu', 'nw_add_calendar_link');
function nw_add_calendar_link() {
    $profile_link = trailingslashit(bp_loggedin_user_domain());

?>

    <li id="wp-admin-bar-my-account-calendar" class="menupop parent">
        <a class="ab-item" aria-haspopup="true" href="<?php echo $profile_link; ?>calendar">
            <span class="wp-admin-bar-arrow" aria-hidden="true"></span><?php _e('Calendar', 'jcorp-events'); ?>
        </a>
    </li>
<?php
}
