<?php

add_action('bp_init', 'nw_setup_group_calendar_nav');
function nw_setup_group_calendar_nav() {
    global $bp;

    /* Add some group subnav items */
    $user_access = false;

    $group_link = '';

    if (bp_is_active('groups') && !empty($bp->groups->current_group)) {

        $group_link  = $bp->root_domain . '/' . bp_get_groups_root_slug() . '/' . $bp->groups->current_group->slug . '/';
        $user_access = $bp->groups->current_group->user_has_access;
        bp_core_new_subnav_item(array(
            'name'            => __('Calendar', 'jcorp-events'),
            'slug'            => 'calendar',
            'parent_url'      => $group_link,
            'parent_slug'     => $bp->groups->current_group->slug,
            'screen_function' => 'nw_bp_group_calendar',
            'position'        => 50,
            'user_has_access' => $user_access,
            'item_css_id'     => 'calendar_tab',
        ));
    }
}

function nw_bp_group_calendar() {

    add_action('bp_template_title', 'nw_group_show_screen_title');
    add_action('bp_template_content', 'nw_show_group_calendar');

    $templates = array('groups/single/plugins.php', 'plugin-template.php');

    if (strstr(locate_template($templates), 'groups/single/plugins.php')) {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'groups/single/plugins'));
    } else {
        bp_core_load_template(apply_filters('bp_core_template_plugin', 'plugin-template'));
    }
}

function nw_group_show_screen_title() {
    _e('Calendar', 'jcorp-events');
}

function nw_show_group_calendar() {

    do_shortcode('[calendar-events group="' . bp_get_group_id() . '"]');
}
