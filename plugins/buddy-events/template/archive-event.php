<?php global $wp;?>

<?php get_header();?>

<div id="primary" class="content-area">

    <main id="main" class="site-main post-wrap" role="main">

        <article id="post-<?php the_ID();?>" <?php post_class();?>>

            <div class="entry-content">

                <?php

if (get_field('bp_events_front_end', 'option') == true
    && isset($wp->query_vars['action'])
    && $wp->query_vars['action'] == 'add'
):

    do_shortcode('[add-event]');

else:

    do_shortcode('[calendar-events]');

endif;

?>

            </div>

        </article>

    </main><!-- #content -->

</div><!-- #primary -->

<?php get_footer();?>