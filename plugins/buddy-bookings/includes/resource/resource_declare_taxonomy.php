<?php

add_action('init', 'nw_bookings_declare_taxonomy_resource', 0);
function nw_bookings_declare_taxonomy_resource() {

    $labels = array(
        'name'                       => __('Resources', 'jcorp-bookings'),
        'singular_name'              => __('Resource', 'jcorp-bookings'),
        'menu_name'                  => __('Resources', 'jcorp-bookings'),
        'all_items'                  => __('All Resources', 'jcorp-bookings'),
        'parent_item'                => __('Parent Resource', 'jcorp-bookings'),
        'parent_item_colon'          => __('Parent Resource:', 'jcorp-bookings'),
        'new_item_name'              => __('New Resource Name', 'jcorp-bookings'),
        'add_new_item'               => __('Add New Resource', 'jcorp-bookings'),
        'edit_item'                  => __('Edit Resource', 'jcorp-bookings'),
        'update_item'                => __('Update Resource', 'jcorp-bookings'),
        'view_item'                  => __('View Resource', 'jcorp-bookings'),
        'separate_items_with_commas' => __('Separate items with commas', 'jcorp-bookings'),
        'add_or_remove_items'        => __('Add or remove items', 'jcorp-bookings'),
        'choose_from_most_used'      => __('Choose from the most used', 'jcorp-bookings'),
        'popular_items'              => __('Popular Resources', 'jcorp-bookings'),
        'search_items'               => __('Search Resources', 'jcorp-bookings'),
        'not_found'                  => __('Not Found', 'jcorp-bookings'),
        'no_terms'                   => __('No items', 'jcorp-bookings'),
        'items_list'                 => __('Resources list', 'jcorp-bookings'),
        'items_list_navigation'      => __('Resources list navigation', 'jcorp-bookings'),
    );
    $args = array(
        'labels'             => $labels,
        'hierarchical'       => true,
        'public'             => true,
        'show_admin_column'  => false,
        'show_in_nav_menus'  => false,
        'show_tagcloud'      => false,
        'show_ui'            => true,
        'show_in_quick_edit' => false,
        'meta_box_cb'        => false,
    );
    register_taxonomy('resource', array('booking'), $args);
}
