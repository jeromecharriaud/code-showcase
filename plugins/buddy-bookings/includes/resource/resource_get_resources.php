<?php
add_action( 'wp', 'nw_bookings_get_resources' );
function nw_bookings_get_resources( $format = '' ) {

    $resources = array();

    $parent_terms = get_terms(
        'resource',
        array(
            'parent'     => 0,
            'orderby'    => 'slug',
            'hide_empty' => false,
        )
    );

    if ( ! empty( $parent_terms ) ) {

        $i = 0;

        foreach ( $parent_terms as $parent_term ) {

            $terms = get_terms(
                'resource',
                array(
                    'parent'     => $parent_term->term_id,
                    'orderby'    => 'slug',
                    'hide_empty' => false,
                )
            );

            if ( $format == 'scheduler' ) {

                $resources[$i] = array(
                    'id'                   => $parent_term->term_id,
                    'title'                => $parent_term->name,
                    'show'                 => true,
                    'eventBackgroundColor' => get_field( 'color', 'term_' . $parent_term->term_id, true ),
                );

                if ( ! empty( $terms ) ) {

                    $resources_children = array();

                    foreach ( $terms as $term ) {

                        $resources_children[] = array(
                            'id'    => $term->term_id,
                            'title' => $term->name,
                            'show'  => true,
                        );

                    }

                    $resources[ $i ]['children'] = $resources_children;

                }

            } else {

                if ( ! empty( $terms ) ) {

                    $resources[ $parent_term->name ] = array();

                    foreach ( $terms as $term ) {

                        $resources[ $parent_term->name ][ $term->term_id ] = $term->name;

                    }

                } else {

                    $resources[ $parent_term->term_id ] = $parent_term->name;

                }

            }

            $i++;

        }

    }

    return $resources;

}
