<?php

//Delete all the bookings if they have that deleted resource only
add_action( 'delete_resource', 'nw_bookings_delete_resource', 10, 4 );
function nw_bookings_delete_resource( $term, $term_id, $deleted_term, $object_ids ) {

    //Let's find all the bookings with the term we want to delete
    $args = array(
        'post_type'    => array( 'booking' ),
        'numberposts'  => -1,
        'post_status ' => 'publish',
        'orderby'      => 'meta_value_num',
        'order'        => 'DESC',
        'meta_query'   => array(
            array(
                'key'     => 'resources_%_resource',
                'value'   => $term_id,
                'compare' => 'IN',
            ),
        ),
    );

    $query = new WP_Query( $args );

    if ( $query->have_posts() ) {

        while ( $query->have_posts() ): $query->the_post();
            $resources = get_field( 'resources', get_the_ID() );

            if ( empty( $resources ) ) {
                continue;
            }

            //Let's count the number of resources in the booking, if it's one, it means only the resource deleted is used in the booking. Therefore we need to delete it.
            if ( count( $resources ) === 1 ) {
                wp_delete_post( get_the_ID(), true );

                //Else if there are other resources, let's just delete the resource
            } else {

                $key = array_search( $term_id, array_column( $resources, 'resource' ) );
                delete_row( 'resources', ( $key + 1 ), get_the_ID() );

            }

        endwhile;

    }

}
