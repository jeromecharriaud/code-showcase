<?php

if (function_exists('acf_add_local_field_group')) {

    acf_add_local_field_group(array(
        'key'                   => 'group_5ef46a817690b',
        'title'                 => __('Resource', 'jcorp-bookings'),
        'fields'                => array(
            array(
                'key'               => 'field_5ef46aacdac75',
                'label'             => __('Color', 'jcorp-bookings'),
                'name'              => 'color',
                'type'              => 'color_picker',
                'instructions'      => '',
                'required'          => 0,
                'conditional_logic' => 0,
                'wrapper'           => array(
                    'width' => '',
                    'class' => '',
                    'id'    => '',
                ),
                'default_value'     => '',
            ),
        ),
        'location'              => array(
            array(
                array(
                    'param'    => 'taxonomy',
                    'operator' => '==',
                    'value'    => 'resource',
                ),
            ),
        ),
        'menu_order'            => 0,
        'position'              => 'normal',
        'style'                 => 'default',
        'label_placement'       => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen'        => '',
        'active'                => true,
        'description'           => '',
    ));
}
