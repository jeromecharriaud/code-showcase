<?php

add_filter( 'acf/load_field/name=resource', 'nw_bookings_acf_load_resource_field' );
function nw_bookings_acf_load_resource_field( $field ) {

    $resources = nw_bookings_get_resources();

    if ( ! empty( $resources ) ) {

        $field['choices'] = $resources;

    }

    return $field;

}
