<?php

if (function_exists('acf_add_local_field_group')) {

    acf_add_local_field_group(array(
        'key'                   => 'group_5ef2fa68001fd',
        'title'                 => 'Booking',
        'fields'                => array(
            array(
                'key'               => 'field_5ef2fae0e948e',
                'label'             => __('Start', 'jcorp-bookings'),
                'name'              => 'start',
                'type'              => 'date_time_picker',
                'instructions'      => '',
                'required'          => 1,
                'conditional_logic' => 0,
                'wrapper'           => array(
                    'width' => '50',
                    'class' => '',
                    'id'    => '',
                ),
                'display_format'    => 'd/m/Y G:i',
                'return_format'     => 'Y-m-d H:i:s',
                'first_day'         => 1,
                'default_value'     => '',
            ),
            array(
                'key'               => 'field_5ef2fb0be948f',
                'label'             => __('End', 'jcorp-bookings'),
                'name'              => 'end',
                'type'              => 'date_time_picker',
                'instructions'      => '',
                'required'          => 1,
                'conditional_logic' => 0,
                'wrapper'           => array(
                    'width' => '50',
                    'class' => '',
                    'id'    => '',
                ),
                'display_format'    => 'd/m/Y G:i',
                'return_format'     => 'Y-m-d H:i:s',
                'first_day'         => 1,
                'default_value'     => '',
            ),
            array(
                'key'               => 'field_5ef2fc3ae360c',
                'label'             => __('Used resources', 'jcorp-bookings'),
                'name'              => 'resources',
                'type'              => 'repeater',
                'instructions'      => __('Add resources for your booking. If some of those are already reserved, a warning will be displayed.', 'jcorp-bookings'),
                'required'          => 0,
                'conditional_logic' => 0,
                'wrapper'           => array(
                    'width' => '',
                    'class' => '',
                    'id'    => '',
                ),
                'collapsed'         => 'field_5ef59038bc936',
                'min'               => 1,
                'max'               => 0,
                'layout'            => 'table',
                'button_label'      => __('Add a resource', 'jcorp-bookings'),
                'sub_fields'        => array(
                    array(
                        'key'               => 'field_5ef59038bc936',
                        'label'             => __('Resource', 'jcorp-bookings'),
                        'name'              => 'resource',
                        'type'              => 'select',
                        'instructions'      => '',
                        'required'          => 1,
                        'conditional_logic' => 0,
                        'wrapper'           => array(
                            'width' => '',
                            'class' => '',
                            'id'    => '',
                        ),
                        'choices'           => array(),
                        'default_value'     => false,
                        'allow_null'        => 0,
                        'multiple'          => 0,
                        'ui'                => 1,
                        'ajax'              => 0,
                        'return_format'     => 'value',
                        'placeholder'       => '',
                    ),
                ),
            ),
        ),
        'location'              => array(
            array(
                array(
                    'param'    => 'post_type',
                    'operator' => '==',
                    'value'    => 'booking',
                ),
            ),
        ),
        'menu_order'            => 0,
        'position'              => 'normal',
        'style'                 => 'default',
        'label_placement'       => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen'        => '',
        'active'                => true,
        'description'           => '',
    ));
}
