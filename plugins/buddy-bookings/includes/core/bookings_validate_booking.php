<?php

add_action('acf/validate_save_post', 'nw_bookings_check_booking_availibility');
function nw_bookings_check_booking_availibility() {

    //Only the booking form
    if (!isset($_POST['acf']['field_5ef2fc3ae360c'])) {
        return;
    }

    $start = strtotime($_POST['acf']['field_5ef2fae0e948e']);
    $end   = strtotime($_POST['acf']['field_5ef2fb0be948f']);

    $resources = $_POST['acf']['field_5ef2fc3ae360c'];

    //Title
    if (!is_admin() && empty($_POST['acf']['_post_title'])) {
        acf_add_validation_error('acf[_post_title]', __('Please enter a name that represents your booking', 'jcorp-bookings'));
    }

    //Start is required
    if (empty($start)) {
        acf_add_validation_error('acf[field_5ef2fae0e948e]', __('Providing a date and time is mrequired', 'jcorp-bookings'));
    }

    //End is required
    if (empty($end)) {
        acf_add_validation_error('acf[field_5ef2fb0be948f]', __('Providing a date and time is mrequired', 'jcorp-bookings'));
    }

    //Check if the start if before the end
    if (!empty($start) && !empty($end) && $start >= $end) {
        acf_add_validation_error('acf[field_5ef2fae0e948e]', __('The booking start date must be before its end', 'jcorp-bookings'));
        acf_add_validation_error('acf[field_5ef2fb0be948f]', __('The booking end date must be after its start', 'jcorp-bookings'));
    }

    //Check if there are multiple resources
    if (!empty($resources)) {

        $array_resources = array();

        foreach ($resources as $rows) {
            $array_resources[] = $rows['field_5ef59038bc936'];
        }

        if (count($array_resources) != count(array_unique($array_resources))) {
            acf_add_validation_error('acf[field_5ef2fc3ae360c]', __('You have selected duplicate resources. Please only select one of each.', 'jcorp-bookings'));
            return;
        }
    }

    //Check if the resources have not been used already
    $args = array(
        'post_type'    => array('booking'),
        'numberposts'  => -1,
        'post_status ' => 'publish',
        'orderby'      => 'meta_value_num',
        'order'        => 'DESC',
        'meta_query'   => array(
            'time' => array(
                'relation'     => 'OR',
                'start'        => array(
                    'key'     => 'start',
                    'value'   => array($start, ($end - 1)),
                    'compare' => 'BETWEEN',
                ),
                'end'          => array(
                    'key'     => 'end',
                    'value'   => array(($start + 1), $end),
                    'compare' => 'BETWEEN',
                ),
                'special_case' => array(
                    'relation' => 'AND',
                    'start'    => array(
                        'key'     => 'start',
                        'value'   => $start,
                        'compare' => '<',
                    ),
                    'end'      => array(
                        'key'     => 'end',
                        'value'   => $end,
                        'compare' => '>',
                    ),
                ),
            ),
        ),
    );

    //If we're editing the post, we can exclude the current post for control
    if (isset($_POST['post_ID']) && !empty($_POST['post_ID'])) {
        $args['post__not_in'] = array($_POST['post_ID']);
    }

    if (!empty($resources)) {

        $array_resources = array();
        foreach ($resources as $rows) {
            $array_resources[$rows['field_5ef59038bc936']] = $rows['field_5ef59038bc936'];
        }

        $args['meta_query']['resources'] = array(
            'relation' => 'AND',
            array(
                'key'     => 'resources_%_resource',
                'value'   => $array_resources,
                'compare' => 'IN',
            ),
        );
    }

    $query = new WP_Query($args);

    if ($query->have_posts()) {

        $resouces_other_bookings = array();

        while ($query->have_posts()) : $query->the_post();

            if (have_rows('resources', get_the_ID())) {

                while (have_rows('resources', get_the_ID())) : the_row();

                    $resouces_other_bookings[get_sub_field('resource')]['start'] = get_field('start');
                    $resouces_other_bookings[get_sub_field('resource')]['end']   = get_field('end');

                endwhile;
            }

        endwhile;

        $common_resources = array_intersect_key($resouces_other_bookings, $array_resources);

        $html = '';
        if (!empty($common_resources)) {
            $html .= '<ul>';
            foreach ($common_resources as $term_id => $common_resource) {

                $term = get_term_by('id', $term_id, 'resource');

                $html .=
                    '<li>' . $term->name . ' ' .
                    sprintf(
                        __('Used resource from %s to %s', 'jcorp-bookings'),
                        date('d/m/Y à H:i', strtotime($common_resource['start'])),
                        date('d/m/Y à H:i', strtotime($common_resource['end']))
                    ) .
                    '</li>';
            }
            $html .= '</ul>';
        }

        //Display the error message
        acf_add_validation_error('acf[field_5ef2fc3ae360c]', _n('The following resource is not available on this period:', 'The following resources are not available on this period:', count($common_resources), 'jcorp-bookings') . $html);
    }
}
