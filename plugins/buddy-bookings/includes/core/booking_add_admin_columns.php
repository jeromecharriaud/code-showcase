<?php

add_filter('manage_booking_posts_columns', 'nw_bookings_add_admin_columns');
function nw_bookings_add_admin_columns($columns) {

    $new = array();
    foreach ($columns as $key => $title) {
        if ($key == 'author') // Put the Thumbnail column before the Author column
        {
            $new['start']     = __('Start', 'jcorp-bookings');
            $new['end']       = __('Fin', 'jcorp-bookings');
            $new['resources'] = __('Resource', 'jcorp-bookings');
        }

        $new[$key] = $title;
    }
    return $new;
}

add_action('manage_booking_posts_custom_column', 'nw_bookings_add_admin_columns_info', 10, 2);
function nw_bookings_add_admin_columns_info($column, $post_id) {
    switch ($column) {

        case 'start':
            echo date('d/m/Y H:i', get_field('start', $post_id, false));

            break;
        case 'end':
            echo date('d/m/Y H:i', get_field('end', $post_id, false));

            break;
        case 'resources':
            $resources = get_field('resources', $post_id);
            if (!empty($resources)) {
                $array_resources = array();
                foreach ($resources as $resource_content) {
                    $resource = get_term_by('id', $resource_content['resource'], 'resource', 'ARRAY');
                    if ($resource !== false) {
                        $array_resources[] = $resource->name;
                    }
                }
                $array_resources = implode($array_resources, ', ');
            }
            echo $array_resources;
            break;
    }
}
