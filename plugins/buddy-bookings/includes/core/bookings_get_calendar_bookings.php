<?php

add_filter( 'nw_bookings_collect_bookings_calendar_booking', 'nw_bookings_get_calendar_bookings', 10, 2 );
function nw_bookings_get_calendar_bookings( $calendar_bookings, $args_calendar ) {

    $args = array();

    if ( ! empty( $args_calendar['start'] ) ) {
        $args['start'] = $args_calendar['start'];
    }

    if ( ! empty( $args_calendar['end'] ) ) {
        $args['end'] = $args_calendar['end'];
    }

    $bookings = nw_bookings_get_bookings($args);

    if ( ! empty( $bookings ) ) {

        if ( ! empty( $calendar_bookings ) ) {
            end( $calendar_bookings );
            $i = key( $calendar_bookings ) + 1;
        } else {
            $i = 0;
        }

        foreach ( $bookings as $booking ) {

            $post_id = $booking->ID;

            $resources = get_field( 'resources', $post_id );
            if ( ! empty( $resources ) ) {
                $resources = array_column( $resources, 'resource' );
            }

            //Color
            $color = '';
            if ( ! empty( $resources ) ) {
                $first_resource = get_term_by( 'term_id', $resources[0], 'resource' );
                $color          = get_field( 'color', 'term_' . $first_resource->term_id, true );
            }

            $start = new DateTime( get_field( 'start', $post_id ), wp_timezone() );
            $end = new DateTime( get_field( 'end', $post_id ), wp_timezone() );

            $calendar_bookings[$i] = array(
                'id'          => $post_id,
                'title'       => html_entity_decode( get_the_title( $post_id ) ),
                'author'      => get_the_author_meta( 'display_name', get_post_field( 'post_author', $post_id ) ),
                'color'       => $color,
                'all_day'     => false,
                'start'       => $start->format( 'c' ),
                'end'         => $end->format( 'c' ),
                'resourceIds' => $resources,
            );

            //All Day Event
            if (
                get_field( 'all_day', $post_id ) == TRUE
                || ( date( 'H:i', strtotime( get_field( 'start', $post_id ) ) ) == '00:00'
                    && date( 'H:i', strtotime( get_field( 'end', $post_id ) ) ) == '00:00')
                || empty( get_field( 'end', $post_id ) )
            ) {

                $calendar_bookings[$i]['all_day'] = true;
                $calendar_bookings[$i]['start']   = date( 'Y-m-d', strtotime( get_field( 'start', $post_id ) ) );
                $calendar_bookings[$i]['end']     = date( 'Y-m-d', strtotime( get_field( 'start', $post_id ) . " +1 day" ) );

            }

            $i++;

        }

    }

    return $calendar_bookings;

}