<?php

function nw_bookings_get_bookings( $parameters = '' ) {

    $args = array(
        'post_type'   => array( 'booking' ),
        'numberposts' => -1,
        'meta_key'    => 'start',
        'orderby'     => 'meta_value_num',
        'order'       => 'DESC',
    );

    //Start
    if ( ! empty( $parameters['start'] ) ):

        $args['meta_query']['start'] = array(
            'key'     => 'start',
            'value'   => $parameters['end'],
            'compare' => '<=',
        );

    endif;

    //End
    if ( ! empty( $parameters['end'] ) ):

        $args['meta_query']['end'] = array(
            'key'     => 'end',
            'value'   => $parameters['start'],
            'compare' => '>=',
        );

    endif;

    $query = new WP_Query( $args );

    return $query->posts;

}
