<?php

// Register Custom Post Booking
add_action('init', 'nw_bookings_declare_post_type_booking', 0);
function nw_bookings_declare_post_type_booking() {

    $labels = array(
        'name'                     => __('Bookings', 'jcorp-bookings'),
        'singular_name'            => __('Booking', 'jcorp-bookings'),
        'add_new_item'             => __('Add New Booking', 'jcorp-bookings'),
        'edit_item'                => __('Edit Booking', 'jcorp-bookings'),
        'new_item'                 => __('New Booking', 'jcorp-bookings'),
        'view_item'                => __('View Booking', 'jcorp-bookings'),
        'view_items'               => __('View Bookings', 'jcorp-bookings'),
        'search_items'             => __('Search Bookings', 'jcorp-bookings'),
        'not_found'                => __('No bookings found.', 'jcorp-bookings'),
        'not_found_in_trash'       => __('No bookings found in Trash.', 'jcorp-bookings'),
        'all_items'                => __('All Bookings', 'jcorp-bookings'),
        'archives'                 => __('Booking Archives', 'jcorp-bookings'),
        'attributes'               => __('Booking Attributes', 'jcorp-bookings'),
        'insert_into_item'         => __('Insert into booking', 'jcorp-bookings'),
        'uploaded_to_this_item'    => __('Uploaded to this booking', 'jcorp-bookings'),
        'filter_items_list'        => __('Filter bookings list', 'jcorp-bookings'),
        'items_list_navigation'    => __('Bookings list navigation', 'jcorp-bookings'),
        'items_list'               => __('Bookings list', 'jcorp-bookings'),
        'item_published'           => __('Booking published.', 'jcorp-bookings'),
        'item_published_privately' => __('Booking published privately.', 'jcorp-bookings'),
        'item_reverted_to_draft'   => __('Booking reverted to draft.', 'jcorp-bookings'),
        'item_scheduled'           => __('Booking scheduled.', 'jcorp-bookings'),
        'item_updated'             => __('Booking updated.', 'jcorp-bookings'),
    );
    $rewrite = array(
        'slug'       => JCORP_BOOKINGS_SLUG,
        'with_front' => true,
        'pages'      => true,
        'feeds'      => true,
    );
    $args = array(
        'label'               => __('Bookings', 'jcorp-bookings'),
        'description'         => __('Description', 'jcorp-bookings'),
        'labels'              => $labels,
        'supports'            => array('title', 'author'),
        'taxonomies'          => array(),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'menu_icon'           => 'dashicons-calendar',
        'menu_position'       => 8,
        'show_in_admin_bar'   => true,
        'show_in_nav_menus'   => true,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'rewrite'             => $rewrite,
        'capability_type'     => 'post',
        'map_meta_cap'        => true,
    );

    register_post_type('booking', $args);
}
