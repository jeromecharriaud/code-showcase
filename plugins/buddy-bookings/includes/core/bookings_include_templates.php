<?php

add_filter('template_include', 'nw_bookings_include_templates');
function nw_bookings_include_templates($template) {

    if (is_singular('booking')) {
    }

    if (is_post_type_archive('booking')) {

        // TODO voir ici l'utilisation du plugin basename
        $template = JCORP_BOOKINGS_PATH . 'templates/archive-booking.php';
    }

    return $template;
}
