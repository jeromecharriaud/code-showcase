<?php

//Full Calendar
add_action('wp_enqueue_scripts', 'nw_bookings_declare_calendar_scripts');
function nw_bookings_declare_calendar_scripts() {

    //Config
    wp_register_script(
        'nw-bookings-js',
        JCORP_BOOKINGS_URL . 'assets/js/nw-bookings.js',
        array('jquery'),
        '1.0.0',
        true
    );
}
