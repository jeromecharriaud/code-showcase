<?php

function nw_bookings_show_booking_form() {

    acf_form_head();

    echo '<h1 class="entry-title settings-title">' . __('Book', 'jcorp-bookings') . '</h1>';

    $options = array(
        'post_id'            => 'new_post',
        'new_post'           => array(
            'post_type'   => 'booking',
            'post_status' => 'publish',
        ),
        'form'               => true,
        'post_title'         => true,
        'post_content'       => false,
        'label_placement'    => '',
        'html_before_fields' => '<input type="text" id="submitBooking" name="submitBooking" value="true" style="display:none;">',
        'html_after_fields'  => '',
        'id'                 => '',
        'method'             => 'post',
        'updated_message'    => '',
        'return'             => '?booked=true',
        'submit_value'       => __('Book', 'jcorp-bookings'),
        'field_groups'       => array('group_5ef2fa68001fd'),
    );

    acf_form($options);

    echo '<a href="/' . JCORP_BOOKINGS_SLUG . '" class="button">' . __('Return', 'jcorp-bookings') . '</a>';
}
