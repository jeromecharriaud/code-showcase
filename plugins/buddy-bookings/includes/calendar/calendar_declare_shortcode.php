<?php

//Full Calendar
add_shortcode('nw-bookings-calendar', 'nw_bookings_declare_shortcode_calendar');
function nw_bookings_declare_shortcode_calendar($args) {

    //Show the booking form
    if (isset($_GET['action']) && $_GET['action'] == 'add-booking') {
        if (is_user_logged_in()) {
            nw_bookings_show_booking_form();
        } else {
            wp_redirect('/');
        }
    } else {
        //Else show the calendar

        // Load ACF styles for ACF loader
        acf_form_head();

        if (isset($_GET['booked']) && $_GET['booked'] == 'true') {
            echo '<p class="acf-notice -success acf-success-message">' . __('Your booking has been registered', 'jcorp-bookings') . '</p>';
        }

        //Ajax
        wp_localize_script('nw-bookings-js', 'my_ajax', array('ajaxurl' => admin_url('admin-ajax.php')));

        //Translations
        wp_localize_script(
            'nw-bookings-js',
            'dom_labels',
            array(
                'reserved_by'  => __('Reserved by:', 'jcorp-bookings'),
                'start'        => __('Start:', 'jcorp-bookings'),
                'end'          => __('End:', 'jcorp-bookings'),
                'resources'    => __('Resources', 'jcorp-bookings'),
                'hide_all'     => __('Hide all', 'jcorp-bookings'),
                'show_all'     => __('Show all', 'jcorp-bookings'),
                'select_all'   => __('Select all', 'jcorp-bookings'),
                'unselect_all' => __('Unselect all', 'jcorp-bookings'),
            )
        );

        //Styles & Scripts
        // TODO : dépendant de JCorp, voir si on rapatrie
        nw_calendar_enqueue_files();
        nw_bookings_enqueue_files();

        //Send Arguments to the JS Script
        if (!empty($args)) {
            foreach ($args as $key => $value) {
                wp_localize_script('nw-bookings-js', $key, $value);
            }
        }

        $resources = nw_bookings_get_resources($type = 'scheduler');

        wp_localize_script('nw-bookings-js', 'all_resources', $resources);
        wp_localize_script('nw-bookings-js', 'resources', $resources);

        if (!empty($resources)) {

            //Taxonomies
            $taxonomies = get_object_taxonomies('booking');

            if (!empty($taxonomies)) {

                echo '<div id="calendar-container">';

                //Datepicker
                echo '<input style="margin-bottom: 15px;" type="text" id="nw-bookings-datepicker">';

                echo '<div id="calendar-left-column">';

                echo '<div id="calendar-filters">';

                echo '<h3>' . __('Filters', 'jcorp-bookings') . '</h3>';

                foreach ($taxonomies as $key => $taxonomy) {

                    $labels = get_taxonomy_labels(get_taxonomy($taxonomy));

                    echo '<div class="filter-taxonomy" data-taxonomy=' . $taxonomy . '>';

                    $parent_terms = get_terms(
                        $taxonomy,
                        array(
                            'parent'     => 0,
                            'orderby'    => 'slug',
                            'hide_empty' => false,
                        )
                    );

                    if (!empty($parent_terms)) {

                        $i = 0;
                        echo '<ul class="parent-filters">';

                        foreach ($parent_terms as $parent_term) {

                            $color = get_field('color', 'term_' . $parent_term->term_id, true);

                            echo '<li class="filter-term"  data-term="' . $parent_term->slug . '" data-color="' . $color . '">';

                            echo '<input checked type="checkbox" class="filter" data-level="parent" data-parent_id="' . $i . '" data-taxonomy="' . $taxonomy . '" data-title="' . $parent_term->name . '" value="' . $parent_term->slug . '">';
                            echo '<label class="filter-label">' . $parent_term->name . '</label>';

                            $terms = get_terms(
                                $taxonomy,
                                array(
                                    'parent'     => $parent_term->term_id,
                                    'orderby'    => 'slug',
                                    'hide_empty' => false,
                                )
                            );

                            echo '</li>';

                            $i++;
                        }

                        echo '</ul>';
                    }
                }

                echo '</div>';

                echo '</div>';

                //Actions
                if (is_user_logged_in()) {

                    echo '<div id="calendar-actions">';

                    echo '<h3>' . __('Actions', 'jcorp-bookings') . '</h3>';

                    echo '<a href="?action=add-booking" class="button">' . __('Book', 'jcorp-bookings') . '</a>';

                    echo '</div>';
                }

                //Loading
                echo '<div id="loading_container" style="position:relative; font-size: 14px;">';

                echo '<span class="acf-spinner is-active" style="display: inline-block;"></span> ' . __('Bookings loading', 'jcorp-bookings');

                echo '</div>';

                echo '</div>';
            }
        }

        echo '<div id="calendar"></div>';
    }
}
