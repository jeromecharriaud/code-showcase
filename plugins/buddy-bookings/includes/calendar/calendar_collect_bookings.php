<?php

add_action( 'wp_ajax_nw_bookings_get_calendar_json_bookings', 'nw_bookings_get_calendar_json_bookings' );
add_action( 'wp_ajax_nopriv_nw_bookings_get_calendar_json_bookings', 'nw_bookings_get_calendar_json_bookings' );
function nw_bookings_get_calendar_json_bookings() {

    // Short-circuit if the client did not give us a date range.
    if ( ! isset( $_REQUEST['start'] ) || ! isset( $_REQUEST['end'] ) ) {
        return;
    }

    $args = array(
        'start' => strtotime( $_REQUEST['start'] ),
        'end'   => strtotime( $_REQUEST['end'] ),
    );

    $calendar_bookings = array();

    $bookings = apply_filters( 'nw_bookings_collect_bookings_calendar_booking', $calendar_bookings, $args );

    echo json_encode( $bookings );

    exit;

}