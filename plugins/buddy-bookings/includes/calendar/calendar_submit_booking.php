<?php

add_action('acf/save_post', 'nw_bookings_submit_booking', 20);

function nw_bookings_submit_booking($post_id) {

    if (get_post_type($post_id) !== 'booking') {
        return;
    }

    $start = date('Y-m-d', get_field('start', $post_id, false));

    if (isset($_POST['submitBooking']) && $_POST['submitBooking'] === "true") {
        wp_redirect('/' . JCORP_BOOKINGS_SLUG . '?booked=true&start=' . $start);
        exit;
    }
}
