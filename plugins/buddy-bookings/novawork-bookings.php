<?php

/**
 * Plugin Name: JCorp - Bookings
 * Plugin URI:  https://www.inovagora.net/
 * Description: Plugin for booking resources
 * Author:      Inovagora
 * Author URI:  https://www.inovagora.net/
 * Version:     1.0.0
 * Text Domain: jcorp-bookings
 * Domain Path: /languages/
 * License:     GPLv3 or later (license.txt)
 */

/**
 * This file should always remain compatible with the minimum version of
 * PHP supported by WordPress.
 */

// Exit if accessed directly
defined('ABSPATH') || exit;

if (!class_exists('JCorp_Bookings_BB_Platform_Addon')) {

    /**
     * Main JCorp Bookings Class
     *
     * @class JCorp_Bookings_BB_Platform_Addon
     * @version    1.0.0
     */
    final class JCorp_Bookings_BB_Platform_Addon {

        /**
         * @var JCorp_Bookings_BB_Platform_Addon The single instance of the class
         * @since 1.0.0
         */
        protected static $_instance = null;

        /**
         * Main JCorp_Bookings_BB_Platform_Addon Instance
         *
         * Ensures only one instance of JCorp_Bookings_BB_Platform_Addon is loaded or can be loaded.
         *
         * @since 1.0.0
         * @static
         * @see JCorp_Bookings_BB_Platform_Addon()
         * @return JCorp_Bookings_BB_Platform_Addon - Main instance
         */
        public static function instance() {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * Cloning is forbidden.
         * @since 1.0.0
         */
        public function __clone() {
            _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'jcorp-bookings'), '1.0.0');
        }
        /**
         * Unserializing instances of this class is forbidden.
         * @since 1.0.0
         */
        public function __wakeup() {
            _doing_it_wrong(__FUNCTION__, __('Cheatin&#8217; huh?', 'jcorp-bookings'), '1.0.0');
        }

        /**
         * JCorp_Bookings_BB_Platform_Addon Constructor.
         */
        public function __construct() {
            $this->define_constants();
            $this->includes();
            // Set up localisation.
            $this->load_plugin_textdomain();
        }

        /**
         * Define Constants
         */
        private function define_constants() {
            $this->define('JCORP_BOOKINGS_URL', plugin_dir_url(__FILE__));
            $this->define('JCORP_BOOKINGS_PATH', plugin_dir_path(__FILE__));
            $this->define('JCORP_BOOKINGS_SLUG', __('bookings', 'jcorp-bookings'));
        }

        /**
         * Define constant if not already set
         * @param  string $name
         * @param  string|bool $value
         */
        private function define($name, $value) {
            if (!defined($name)) {
                define($name, $value);
            }
        }

        /**
         * Include required core files used in admin and on the frontend.
         */
        public function includes() {
            $includes = glob(JCORP_BOOKINGS_PATH . 'includes/*/*.php');

            //Include All Files
            foreach ($includes as $include) {

                require_once $include;
            }
        }

        /**
         * Load Localisation files.
         *
         * Note: the first-loaded translation file overrides any following ones if the same translation is present.
         */
        public function load_plugin_textdomain() {
            $locale = is_admin() && function_exists('get_user_locale') ? get_user_locale() : get_locale();
            $locale = apply_filters('plugin_locale', $locale, 'jcorp-bookings');

            unload_textdomain('jcorp-bookings');
            load_textdomain('jcorp-bookings', WP_LANG_DIR . '/' . plugin_basename(dirname(__FILE__)) . '/' . plugin_basename(dirname(__FILE__)) . '-' . $locale . '.mo');
            load_plugin_textdomain('jcorp-bookings', false, plugin_basename(dirname(__FILE__)) . '/languages');
        }
    }

    /**
     * Returns the main instance of JCorp_Bookings_BB_Platform_Addon to prevent the need to use globals.
     *
     * @since  1.0.0
     * @return JCorp_Bookings_BB_Platform_Addon
     */
    function JCorp_Bookings_BB_Platform_Addon() {
        return JCorp_Bookings_BB_Platform_Addon::instance();
    }

    function JCorp_Bookings_BB_Platform_install_bb_platform_notice() {
        echo '<div class="error fade"><p>';
        _e('<strong>JCorp Bookings</strong></a> requires the BuddyBoss Platform plugin to work. Please <a href="https://buddyboss.com/platform/" target="_blank">install BuddyBoss Platform</a> first.', 'jcorp-bookings');
        echo '</p></div>';
    }

    function JCorp_Bookings_BB_Platform_update_bb_platform_notice() {
        echo '<div class="error fade"><p>';
        _e('<strong>JCorp Bookings</strong></a> requires BuddyBoss Platform plugin version 1.2.6 or higher to work. Please update BuddyBoss Platform.', 'jcorp-bookings');
        echo '</p></div>';
    }

    function JCorp_Bookings_BB_Platform_is_active() {
        if (defined('BP_PLATFORM_VERSION') && version_compare(BP_PLATFORM_VERSION, '1.2.6', '>=')) {
            return true;
        }
        return false;
    }

    function JCorp_Bookings_BB_Platform_init() {
        if (!defined('BP_PLATFORM_VERSION')) {
            add_action('admin_notices', 'JCorp_Bookings_BB_Platform_install_bb_platform_notice');
            add_action('network_admin_notices', 'JCorp_Bookings_BB_Platform_install_bb_platform_notice');
            return;
        }

        if (version_compare(BP_PLATFORM_VERSION, '1.2.6', '<')) {
            add_action('admin_notices', 'JCorp_Bookings_BB_Platform_update_bb_platform_notice');
            add_action('network_admin_notices', 'JCorp_Bookings_BB_Platform_update_bb_platform_notice');
            return;
        }

        JCorp_Bookings_BB_Platform_Addon();
    }

    add_action('plugins_loaded', 'JCorp_Bookings_BB_Platform_init', 9);
}
