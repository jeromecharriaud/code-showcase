<?php get_header(); ?>

<div id="primary" class="content-area">

    <main id="main" class="site-main post-wrap" role="main">

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="entry-content">

                <?php do_shortcode( '[nw-bookings-calendar]' ); ?>

            </div>

        </article>

    </main><!-- #content -->

</div><!-- #primary -->

<?php get_footer(); ?>
