jQuery(document).ready(function ($) {
	function nw_booking_get_query_string_value(param) {
		var vars = {}
		window.location.href.replace(location.hash, '').replace(
			/[?&]+([^=&]+)=?([^&]*)?/gi, // regexp
			function (m, key, value) {
				// callback
				vars[key] = value !== undefined ? value : ''
			}
		)

		if (param) {
			return vars[param] ? vars[param] : null
		}
		return vars
	}

	//Datepicker
	$('#nw-bookings-datepicker')
		.datepicker(
			{
				inline: true,
				autoSize: true,
				changeMonth: false,
				showButtonPanel: true,
				showWeek: true,
				firstDay: 1,
				setDate: new Date(),
				onSelect: function (dateText, inst) {
					splitDate = dateText.split('/')
					var date = new Date(
						splitDate[1] + '/' + splitDate[0] + '/' + splitDate[2]
					)
					calendar.gotoDate(date)
				},
			},
			$.datepicker.regional['fr']
		)
		.datepicker('setDate', '0')

	//Labels Color
	var styleElem = document.head.appendChild(document.createElement('style'))
	var filter_terms = $('#calendar-filters .filter-term')
	filter_terms.each(function (index) {
		let color = this.getAttribute('data-color')
		let term = this.getAttribute('data-term')
		styleElem.innerHTML +=
			'li[data-term=' +
			term +
			'] input[type=checkbox]:checked+label:before {background: ' +
			color +
			';}'
	})

	var left = 'today, prev,next'
	var center = 'title'
	var right = 'resourceTimelineDay,resourceTimelineWeek'

	//Mobile Settings
	if (
		/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
			navigator.userAgent
		)
	) {
		left = 'prev'
		center = 'title'
		right = 'next'
	}

	var calendarEl = document.getElementById('calendar')
	var calendar = new FullCalendar.Calendar(calendarEl, {
		timeZone: 'local',
		initialView: 'resourceTimelineDay',
		locale: 'fr',
		editable: false,
		aspectRatio: 1,
		headerToolbar: {
			left: left,
			center: center,
			right: right,
		},
		eventDidMount: function (info) {
			var dateStart = info.event.start
			var dateEnd = info.event.end

			var start =
				dateStart.getDate() +
				'-' +
				('0' + (dateStart.getMonth() + 1)).slice(-2) +
				'-' +
				dateStart.getFullYear() +
				' à ' +
				dateStart.getHours() +
				'h' +
				('0' + dateStart.getMinutes()).slice(-2)

			var end =
				dateEnd.getDate() +
				'-' +
				('0' + (dateEnd.getMonth() + 1)).slice(-2) +
				'-' +
				dateEnd.getFullYear() +
				' à ' +
				dateEnd.getHours() +
				'h' +
				('0' + dateEnd.getMinutes()).slice(-2)

			var tooltip = new Tooltip(info.el, {
				title: info.event.title,
				placement: 'top',
				trigger: 'hover',
				container: 'body',
				template:
					'<div class="tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div><div class="booked_by">' +
					dom_labels.reserved_by +
					' ' +
					info.event.extendedProps.author +
					'</div><div class="start">' +
					dom_labels.start +
					' ' +
					start +
					'</div><div class="end">' +
					dom_labels.end +
					' ' +
					end +
					'</div></div>',
			})
		},

		resourceAreaWidth: '20%',
		resourceAreaHeaderContent: dom_labels.resources,
		resources: function (fetchInfo, successCallback, failureCallback) {
			filtered = []

			$(resources).each(function (index_parent) {
				let resources_parent = resources[index_parent]
				let resources_children = []
				//Parent
				if (this.show === true) {
					filtered.push(resources_parent)
				}
			})

			successCallback(filtered)
		},
		resourceOrder: 'title',
		events: function (info, successCallback, failureCallback) {
			jQuery.ajax({
				url: my_ajax.ajaxurl,
				type: 'POST',
				datatype: 'json',
				data: {
					action: 'nw_bookings_get_calendar_json_bookings',
					start: info.start,
					end: info.end,
				},
				success: function (response) {
					var data = jQuery.parseJSON(response)
					successCallback(data)
				},
			})
		},
		loading: function (isLoading) {
			if (isLoading) {
				$('#loading_container').show()
				// TODO : see if useful, doesn't work
				$('.fc-button').addClass('fc-state-disabled')
			} else {
				$('#loading_container').hide()
				// TODO : see if useful, doesn't work
				$('.fc-button').removeClass('fc-state-disabled')
			}
		},
	})

	calendar.render()

	//Taxonomy Filter System : Parent
	$('input[type=checkbox][class=filter][data-level=parent]').change(
		function () {
			parent_id = this.getAttribute('data-parent_id')

			resources[parent_id].show = !resources[parent_id].show

			calendar.refetchResources()

			var is_checked = $(this).prop('checked')
			var child_filters_li = $('.child-filters')
			$(child_filters_li[this.getAttribute('data-parent_id')])
				.find('input')
				.prop('checked', is_checked)
		}
	)

	//Taxonomy Filter System : Child
	$('input[type=checkbox][class=filter][data-level=child]').change(
		function () {
			parent_id = this.getAttribute('data-parent_id')
			child_id = this.getAttribute('data-child_id')
			var child = resources[parent_id]['children'][child_id]

			if (child.show !== undefined) {
				child.show = !child.show
			}

			calendar.refetchResources()
		}
	)

	//Show Child Resources
	$('#calendar-filters .show-more').click(function () {
		$(this).parent().find('ul.child-filters').toggle()
		if ($(this).hasClass('bb-icon-plus-square')) {
			$(this).removeClass('bb-icon-plus-square')
			$(this).addClass('bb-icon-minus-square')
		} else {
			$(this).removeClass('bb-icon-minus-square')
			$(this).addClass('bb-icon-plus-square')
		}
	})

	//Show all resources
	var all_hidden = true
	$('#show-all-resources').click(function () {
		if (all_hidden === true) {
			$('ul.child-filters').show()
			all_hidden = false
			$(this).html(dom_labels.hide_all)

			$('#calendar-filters .show-more').addClass('bb-icon-minus-square')
			$('#calendar-filters .show-more').removeClass('bb-icon-plus-square')
		} else {
			$('ul.child-filters').hide()
			all_hidden = true
			$(this).html(dom_labels.show_all)
			$('#calendar-filters .show-more').removeClass(
				'bb-icon-minus-square'
			)
			$('#calendar-filters .show-more').addClass('bb-icon-plus-square')
		}
	})

	//Select all resources
	var all_selected = true
	$('#select-all-resources').click(function () {
		if (all_selected === true) {
			$('input[type=checkbox][class=filter]').attr('checked', false)
			all_selected = false
			$(this).html(dom_labels.select_all)
			resources = []
		} else {
			$('input[type=checkbox][class=filter]').attr('checked', true)
			all_selected = true
			$(this).html(dom_labels.unselect_all)
			resources = all_resources
		}
		calendar.refetchResources()
	})

	//If there is a start in the url, let's go to that day
	var start = nw_booking_get_query_string_value('start')
	if (start) {
		calendar.gotoDate(start)
		splitDate = start.split('-')
		$('#nw-bookings-datepicker').datepicker(
			'setDate',
			splitDate[2] + '/' + splitDate[1] + '/' + splitDate[0]
		)
	}
})
